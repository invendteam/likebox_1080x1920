﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMonitorEnums
{
    //TODO: Merge enums! Important

    public enum ReportState 
    {
        /// <summary>
        /// Успешно переданные данные
        /// </summary>
        Successfull,
        /// <summary>
        /// Терминал не зарегистрирован
        /// </summary>
        NotRegisteredTerminal,
        /// <summary>
        /// Терминал зарегистрирован, но ожидает владельца
        /// </summary>
        AwaitOwner,   
        /// <summary>
        /// Ожидает первичной конфигурации
        /// </summary>
        AwaitSetup,    
        /// <summary>
        /// Ошибка базы данных
        /// </summary>
        DatabaseError,
        /// <summary>
        /// Что-то пошло не так. Скорее всего это сервер ответил с ошибкой
        /// </summary>
        UnknownError,
        /// <summary>
        /// Сервер не доступен
        /// </summary>
        ServerDown,
        /// <summary>
        /// Некорректный хеш-код
        /// </summary>
        NoHash,
        /// <summary>
        /// Терминал заблокирован, любая дальнейшая работа должна быть прекращена
        /// </summary>
        TerminalLocked
    }
    public enum RegisterState
    {
        /// <summary>
        /// Успешная регистрация
        /// </summary>
        Successfull,
        /// <summary>
        /// Терминал зарегистрирован, но ожидает владельца
        /// </summary>
        AwaitOwner,
        /// <summary>
        /// Ожидает первичной конфигурации
        /// </summary>
        AwaitSetup,
        /// <summary>
        /// Что-то пошло не так. Скорее всего это сервер ответил с ошибкой
        /// </summary>
        UnknownError,  
        /// <summary>
        /// Терминал уже зарегистрирован, есть владелец  и проведена первичная конфигурация. Можно работать.
        /// </summary>
        AlreadyRegister,
        /// <summary>
        /// Что-то пошло не так. Скорее всего это сервер ответил с ошибкой
        /// </summary>
        UnknownClientError,
        /// <summary>
        /// Сервер не доступен
        /// </summary>
        ServerDown,
        /// <summary>
        /// Доступна новая конфигурация
        /// </summary>
        AvaibableConfig,
        /// <summary>
        /// Сервер ожидает трансляцию видео
        /// </summary>
        AwaitBoostConnection,
        /// <summary>
        /// Доступна новая картинка
        /// </summary>
        ImageAvailable,
        /// <summary>
        /// Доступно новое видео
        /// </summary>
        VideoAvailable,
        /// <summary>
        /// Терминал заблокирован, любая дальнейшая работа должна быть прекращена
        /// </summary>
        TerminalLocked

    }
    public enum TerminalType
    {
        Instamat
    }
    public enum InstamatLogType
    {
        /// <summary>
        /// Отправка состояния
        /// </summary>
        SendReport,
        /// <summary>
        /// Получение конфигурации
        /// </summary>
        GetConfig,
        /// <summary>
        /// Собщение об ошибке сервера
        /// </summary>
        ServerError,
        /// <summary>
        /// Сообщение об ошибке терминала
        /// </summary>
        TerminalError,
        /// <summary>
        /// Потеря соединения с сервером
        /// </summary>
        LostConnection,
        /// <summary>
        /// Прочее
        /// </summary>
        Other,
        /// <summary>
        /// СОобщение о регистрации
        /// </summary>
        Register,
        /// <summary>
        /// Отчет о покупке
        /// </summary>
        SendPurchase,
        /// <summary>
        /// Отправка отчета об инкасации
        /// </summary>
        SendCollection
    }


}
