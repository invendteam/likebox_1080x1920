﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lkbox
{

    public partial class Login : Form
    {
        MessageForm mes;
        bool KeyboardShown = false, cleanPass = true;
        bool shiftPressed = false;
        bool sent = false;
        int timer = 1800;
        bool loggedOut = true, pageLoaded = false;
        public WebBrowser wb;
        DoubleBufferedPanel p;
        Button[] u, m, l, spc;
        Button[][] allKeys;
        TextBox log, pass;
        HtmlElement un, ps, el; 

        public Login(string bttnText, string privateText, string loginText, string notSaving, string username)
        {
            InitializeComponent();
            timer1.Start();
            Size = new Size(1080, 1920);
            BackgroundImage = new Bitmap(Image.FromFile("Private.jpg"), Size);
            u = new Button[12];
            m = new Button[11];
            l = new Button[11];
            spc = new Button[9];
            allKeys = new Button[][] { u, m, l, spc };
            log = new TextBox();
            log.Text = username;
            log.Size = new Size(440, 100);
            log.Font = new Font("PF Centro Slab Pro", 28f);
            pass = new TextBox();
            pass.PasswordChar = '*';
            pass.Text = "password";
            pass.Size = new Size(440, 100);
            pass.Font = new Font("PF Centro Slab Pro", 24f);
            pass.GotFocus += pass_GotFocus;
            log.BorderStyle = BorderStyle.None;
            pass.BorderStyle = BorderStyle.None;
            Controls.AddRange(new Control[] { log, pass });
            log.Location = new Point(120, 775);
            pass.Location = new Point(120, 905);
            pass.UseSystemPasswordChar = true;
            loggedOut = Form1.AccessToken != "Logout" && String.IsNullOrWhiteSpace(Form1.AccessToken);
            wb = new WebBrowser();
            wb.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wb_DocumentCompleted);
            wb.ScriptErrorsSuppressed = true;
            Button Back = new Button();
            Back.Name = "Back";
            Back.Text = bttnText;
            Back.BackColor = Color.Transparent;
            Back.Click += new EventHandler(Back_Click);
            Back.Size = new Size(350, 100);
            Back.FlatStyle = FlatStyle.Flat;
            Back.FlatAppearance.BorderSize = 0;
            Back.FlatAppearance.MouseOverBackColor = Color.Transparent;
            Back.FlatAppearance.MouseDownBackColor = Color.Transparent;
            Back.FlatAppearance.CheckedBackColor = Color.Transparent;
            Back.ForeColor = Color.White;
            Back.Font = new Font("PF Centro Slab Pro", 42f);
            Back.BackgroundImage = new Bitmap(Image.FromFile("bttnEn.jpg"), Back.Size);
            Controls.Add(Back);
            Back.Location = new Point(80,1320);
            Back = new Button();
            Back.Name = "Login";
            Back.Text = Form1.loc["Log_in"];
            Back.BackColor = Color.Transparent;
            Back.Click += new EventHandler(Login_Click);
            Back.Size = new Size(350, 100);
            Back.FlatStyle = FlatStyle.Flat;
            Back.FlatAppearance.BorderSize = 0;
            Back.FlatAppearance.MouseOverBackColor = Color.Transparent;
            Back.FlatAppearance.MouseDownBackColor = Color.Transparent;
            Back.FlatAppearance.CheckedBackColor = Color.Transparent;
            Back.ForeColor = Color.White;
            Back.Font = new Font("PF Centro Slab Pro", 42f);
            Back.BackgroundImage = new Bitmap(Image.FromFile("bttnEn.jpg"), Back.Size);
            Controls.Add(Back);
            Back.Location = new Point(80, 1030); 
            Label lbl = new Label();
            lbl.Name = "Private";
            lbl.Text = privateText;
            lbl.BackColor = Color.Transparent;
            lbl.Font = new Font("PF Centro Slab Pro", 32f);
            lbl.AutoSize = true;
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            Controls.Add(lbl);
            lbl.Location = new Point(560 - lbl.Width / 2, 440);
            lbl = new Label();
            lbl.Name = "Login";
            lbl.Text = loginText;
            lbl.BackColor = Color.Transparent;
            lbl.Font = new Font("PF Centro Slab Pro", 28f);
            lbl.ForeColor = Color.FromArgb(60, 60, 60);
            lbl.AutoSize = true;
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            Controls.Add(lbl);
            lbl.Location = new Point(80, 600);
            lbl = new Label();
            lbl.Name = "NotSaving";
            lbl.Text = notSaving;
            lbl.BackColor = Color.Transparent;
            lbl.Font = new Font("PF Centro Slab Pro", 20f);
            lbl.ForeColor = Color.FromArgb(60, 60, 60); 
            lbl.AutoSize = true;
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            Controls.Add(lbl);
            lbl.Location = new Point(180, 1160);
            if (!pageLoaded)
            {
                wb.Navigate("https://api.instagram.com/oauth/authorize/?client_id=bc1c1266e3964b1986b6df6512a6561d&redirect_uri=http://localhost:5678&response_type=token");
            }
            Back.Focus();
        }

        void pass_GotFocus(object sender, EventArgs e)
        {
            if (cleanPass)
            {
                pass.Text = "";
                cleanPass = false;
            }

        }

        private void Login_Click(object sender, EventArgs e)
        {
            LoadInProgress.Create("");
            timer = 1800;
            if (log.Text == "" || pass.Text == "") return;
            un = wb.Document.All["id_username"];
            ps = wb.Document.All["id_password"];            
            if (un != null && ps != null)
            {
                un.InnerText = log.Text;
                ps.InnerText = pass.Text;
                HtmlElementCollection elc = wb.Document.GetElementsByTagName("input");
                foreach (HtmlElement el in elc)
                {
                    if (el.GetAttribute("type").Equals("submit"))
                    {
                        el.InvokeMember("Click");
                        sent = true;
                    }
                }
            }
            if(!sent)
            {
                LoadInProgress.Destroy();
                if (mes != null) mes.Close();
                mes = new MessageForm("Error sending data to Instagram", "Ok");
                mes.ShowDialog();
                pageLoaded = false;
                wb.Navigate("https://api.instagram.com/oauth/authorize/?client_id=bc1c1266e3964b1986b6df6512a6561d&redirect_uri=http://localhost:5678&response_type=token");
            }
        }

        void CreateKeys()
        {
            p = new DoubleBufferedPanel();
            p.Size = new Size(1080, 370);
            p.BackgroundImage = new Bitmap(Form1.KeyboardBG, p.Size);
            Controls.Add(p);
            p.Location = new Point(0, 1550);
            for (int i = 0; i < allKeys.Length; i++)
            {
                for (int j = 0; j < allKeys[i].Length; j++)
                {
                    allKeys[i][j] = new NonFocusableButton();
                    allKeys[i][j].Name = i + "_" + j;
                    allKeys[i][j].Font = new Font("PF Centro Slab Pro", 18f);
                    allKeys[i][j].FlatStyle = FlatStyle.Flat;
                    allKeys[i][j].FlatAppearance.BorderSize = 0;
                    allKeys[i][j].FlatAppearance.CheckedBackColor = Color.Transparent;
                    allKeys[i][j].FlatAppearance.MouseDownBackColor = Color.Transparent;
                    allKeys[i][j].FlatAppearance.MouseOverBackColor = Color.Transparent;
                    allKeys[i][j].ForeColor = Color.White;
                    allKeys[i][j].BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), allKeys[i][j].Size);
                    allKeys[i][j].Click += new EventHandler(b_Click);
                    allKeys[i][j].MouseDown += new MouseEventHandler(Key_MouseDown);
                    allKeys[i][j].MouseUp += new MouseEventHandler(Key_MouseUp);
                    p.Controls.Add(allKeys[i][j]);
                }
            }
            ApplyAlphabet("Eng");
            allKeys[3][1].Text = Form1.loc["Log_in"];
            allKeys[3][5].Text = "Рус";
            allKeys[3][4].Text = "Eng";
            allKeys[3][7].Text = ".?123";
        }

        private void Key_MouseDown(object sender, MouseEventArgs e)
        {
            timer = 1800;
            Button s = (Button)sender;
            if (!s.Name.StartsWith("3_"))
            {
                s.BackgroundImage = new Bitmap(Form1.KeyHightBg, s.Size);
            }
            else switch (s.Name.Substring(2))
                {
                    case "0":
                        {
                            s.BackgroundImage = new Bitmap(Form1.BcspHightBG, s.Size);
                            break;
                        }
                    case "2":
                        {
                            s.BackgroundImage = new Bitmap(Form1.lShiftHightBG, s.Size);
                            break;
                        }
                    case "3":
                        {
                            s.BackgroundImage = new Bitmap(Form1.rShiftHightBG, s.Size);
                            break;
                        }
                    case "6":
                        {
                            s.BackgroundImage = new Bitmap(Form1.SpaceHightBG, s.Size);
                            break;
                        }
                    case "8":
                        {
                            s.BackgroundImage = new Bitmap(Form1.HideKeyHightBG, s.Size);
                            break;
                        }
                    default:
                        {
                            s.BackgroundImage = new Bitmap(Form1.KeyHightBg, s.Size);
                            break;
                        }
                }
        }

        private void Key_MouseUp(object sender, MouseEventArgs e)
        {
            Button s = (Button)sender;
            if (!s.Name.StartsWith("3_"))
            {
                s.BackgroundImage = new Bitmap(Form1.KeyBg, s.Size);
            }
            else switch (s.Name.Substring(2))
                {
                    case "0":
                        {
                            s.BackgroundImage = new Bitmap(Form1.BcspBG, s.Size);
                            break;
                        }
                    case "2":
                        {
                            s.BackgroundImage = new Bitmap(Form1.lShiftBG, s.Size);
                            break;
                        }
                    case "3":
                        {
                            s.BackgroundImage = new Bitmap(Form1.rShiftBG, s.Size);
                            break;
                        }
                    case "6":
                        {
                            s.BackgroundImage = new Bitmap(Form1.SpaceBG, s.Size);
                            break;
                        }
                    case "8":
                        {
                            s.BackgroundImage = new Bitmap(Form1.HideKeyBG, s.Size);
                            break;
                        }
                    default:
                        {
                            s.BackgroundImage = new Bitmap(Form1.KeyBg, s.Size);
                            break;
                        }
                }
        }

        public void LogOut()
        {
            wb.Navigate("https://instagram.com/accounts/logout/");
            Form1.AccessToken = "";
        }

        private void b_Click(object sender, EventArgs e)
        {
            timer = 1800;
            Button b = (Button)sender;
            int i;
            if (log.Focused) i = log.SelectionStart;
            else i = pass.SelectionStart;
            if (!b.Name.StartsWith("3_"))
            {
                if (log.Focused)
                {
                    if (log.SelectionLength > 0) log.Text = log.Text.Remove(i, log.SelectionLength);
                    log.Text = log.Text.Insert(i, b.Text);
                    log.SelectionStart = i + 1;
                    log.SelectionLength = 0;
                }
                else
                {
                    if (pass.SelectionLength > 0) pass.Text = pass.Text.Remove(i, log.SelectionLength);
                    pass.Text = pass.Text == Form1.loc["password"] ? b.Text : pass.Text.Insert(i, b.Text);
                    pass.SelectionStart = i + 1;
                    pass.SelectionLength = 0;
                }
            }
            else
                switch (b.Name)
                {
                    case "3_0":
                        {
                            if (log.Text != "" && log.Focused && i != 0)
                            {
                                if (log.SelectionLength > 0)
                                {
                                    log.Text = log.Text.Remove(i, log.SelectionLength);
                                    log.SelectionStart = i;
                                }
                                else
                                {
                                    log.Text = log.Text.Remove(i - 1, 1);
                                    log.SelectionStart = (i - 1) <= 0 ? 0 : (i - 1);
                                }
                            }
                            else if (pass.Focused && pass.Text != "" && i >= 0)
                            {
                                if (pass.SelectionLength > 0)
                                {
                                    pass.Text = pass.Text.Remove(i, pass.SelectionLength);
                                    pass.SelectionStart = i;
                                }
                                else
                                {
                                    pass.Text = pass.Text.Remove(i - 1, 1);
                                    pass.SelectionStart = (i - 1) <= 0 ? 0 : (i - 1);
                                }
                            }
                            break;
                        }
                    case "3_1":
                        {
                            if (log.Text == "" || pass.Text == "") return;
                            while (!pageLoaded) { }
                            un = wb.Document.All["id_username"];
                            ps = wb.Document.All["id_password"];
                            un.InnerText = log.Text;
                            ps.InnerText = pass.Text;
                            HtmlElementCollection elc = wb.Document.GetElementsByTagName("input");
                            foreach (HtmlElement el in elc)
                            {
                                if (el.GetAttribute("type").Equals("submit"))
                                {
                                    el.InvokeMember("Click");
                                }
                            }
                            break;
                        }
                    case "3_2":
                    case "3_3":
                        {
                            shiftPressed = !shiftPressed;
                            byte j = 0;
                            foreach (Button[] bts in allKeys)
                            {
                                j++;
                                if (j != 4)
                                    foreach (Button bt in bts)
                                    {
                                        if (bt.Text != "#" && bt.Text != "." && bt.Text != "," && bt.Text != "_")
                                        {
                                            if (shiftPressed) bt.Text = bt.Text.ToUpper();
                                            else bt.Text = bt.Text.ToLower();
                                        }
                                        else
                                        {
                                            if (bt.Text == "#") bt.Text = "_";
                                            else if (bt.Text == ".") bt.Text = ",";
                                            else if (bt.Text == ",") bt.Text = ".";
                                            else if (bt.Text == "_") bt.Text = "#";
                                        }
                                    }
                                else break;
                            }
                            break;
                        }
                    case "3_6":
                        {
                            if (log.Visible)
                            {
                                if (log.SelectionLength >= 0) log.Text = log.Text.Remove(i, log.SelectionLength);
                                log.Text = log.Text == Form1.loc["username"] ? " " : log.Text.Insert(i, " ");
                                log.SelectionStart = i + 1;
                                log.SelectionLength = 0;
                            }
                            else
                            {
                                pass.Text = pass.Text.Insert(i, " ");
                                pass.SelectionStart = i + 1;
                                pass.SelectionLength = 0;
                            }
                            break;
                        }
                    case "3_8":
                        {
                            p.Visible = false;
                            Focus();
                            return;
                        }
                    case "3_4":
                        {
                            ApplyAlphabet(b.Text);
                            break;
                        }
                    case "3_7":
                        {
                            ApplyAlphabet("Symb");
                            break;
                        }
                    case "3_5":
                        {
                            ApplyAlphabet(b.Text);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            timer = 1800;
        }

        void Back_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        void ApplyAlphabet(string lang)
        {
            byte j = 0, k = 0;
            allKeys[3][2].Size = new Size(80, 80);
            allKeys[3][2].Location = new Point(20, 2 * (allKeys[3][2].Height + 10) + 10);
            allKeys[3][2].BackgroundImage = new Bitmap(Form1.lShiftBG, allKeys[3][2].Size);
            switch (lang)
            {
                case "Eng":
                    {
                        foreach (Button[] bts in allKeys)
                        {
                            if (j == 3) break;
                            foreach (Button bt in bts)
                            {
                                if (j == 0 && k == 10) continue;
                                if (j == 1 && k == 9) continue;
                                if (j == 2 && k == 9) continue;
                                if (j == 0 && k < 10) bt.Text = Form1.alphabetEng[k].ToString();
                                if (j == 1 && k < 9) bt.Text = Form1.alphabetEng[k + 10].ToString();
                                if (j == 2 && k < 9) bt.Text = Form1.alphabetEng[k + 19].ToString();
                                bt.Size = new Size(80, 80);
                                bt.BackgroundImage = new Bitmap(Form1.KeyBg, bt.Size);
                                if (j % 3 != 2) bt.Location = new Point((j % 2 * (bt.Width + 20) / 2) + (k * (bt.Width + 10)) + 20, j * (bt.Height + 10) + 10);
                                else bt.Location = new Point(allKeys[3][2].Width + (k * (bt.Width + 10)) + 30, j * (bt.Height + 10) + 10);
                                k++;
                            }
                            j++;
                            k = 0;
                        }
                        allKeys[0][10].Size = allKeys[0][11].Size = allKeys[1][9].Size = allKeys[1][10].Size = allKeys[2][9].Size = allKeys[2][10].Size = new Size();
                        allKeys[3][0].Location = new Point(allKeys[0][9].Location.X + allKeys[0][9].Width + 10, allKeys[0][9].Location.Y);
                        allKeys[3][0].Size = allKeys[0][9].Size;
                        allKeys[3][0].BackgroundImage = new Bitmap(Form1.BcspBG, allKeys[3][0].Size);
                        allKeys[3][1].Location = new Point(allKeys[1][8].Location.X + allKeys[1][8].Width + 10, allKeys[1][8].Location.Y);
                        allKeys[3][1].Size = new Size((allKeys[1][8].Width * 3) / 2, allKeys[1][8].Height);
                        allKeys[3][1].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][1].Size);
                        allKeys[3][3].Location = new Point(allKeys[2][8].Location.X + allKeys[2][8].Width + 10, allKeys[2][8].Location.Y);
                        allKeys[3][3].Size = new Size(6 * allKeys[3][2].Width / 5, allKeys[3][2].Height);
                        allKeys[3][3].BackgroundImage = new Bitmap(Form1.rShiftBG, allKeys[3][3].Size);
                        allKeys[3][4].Location = new Point(allKeys[0][0].Location.X, allKeys[2][0].Location.Y + allKeys[2][0].Height + 10);
                        allKeys[3][4].Size = allKeys[2][0].Size;
                        allKeys[3][4].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][4].Size);
                        allKeys[3][5].Location = new Point(allKeys[3][4].Location.X + allKeys[3][4].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][5].Size = allKeys[2][0].Size;
                        allKeys[3][5].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][5].Size);
                        allKeys[3][6].Location = new Point(allKeys[3][5].Location.X + 10 + allKeys[3][5].Width, allKeys[3][4].Location.Y);
                        allKeys[3][6].Size = new Size(7 * allKeys[2][0].Width + 60, allKeys[2][0].Height);
                        allKeys[3][6].BackgroundImage = new Bitmap(Form1.SpaceBG, allKeys[3][6].Size);
                        allKeys[3][7].Location = new Point(allKeys[3][6].Location.X + allKeys[3][6].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][7].Size = allKeys[3][3].Size;
                        allKeys[3][7].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][7].Size);
                        allKeys[3][8].Location = new Point(allKeys[3][7].Location.X + allKeys[3][7].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][8].Size = allKeys[0][0].Size;
                        allKeys[3][8].BackgroundImage = new Bitmap(Form1.HideKeyBG, allKeys[3][8].Size);
                        break;
                    }
                case "Symb":
                    {
                        allKeys[3][2].Size = new Size(70, 80);
                        allKeys[3][2].Location = new Point(20, 2 * (allKeys[3][2].Height + 10) + 10);
                        allKeys[3][2].BackgroundImage = new Bitmap(Form1.lShiftBG, allKeys[3][2].Size);
                        foreach (Button[] bts in allKeys)
                        {
                            if (j == 3) break;
                            foreach (Button bt in bts)
                            {
                                if (j == 0) bt.Text = Form1.alphabetSymb[k].ToString();
                                if (j == 1) bt.Text = Form1.alphabetSymb[k + 12].ToString();
                                if (j == 2) bt.Text = Form1.alphabetSymb[k + 23].ToString();
                                bt.Size = new Size(70, 80);
                                bt.BackgroundImage = new Bitmap(Form1.KeyBg, bt.Size);
                                if (j % 3 != 2) bt.Location = new Point((j % 2 * (bt.Width + 10) / 2) + (k * (bt.Width + 10)) + 20, j * (bt.Height + 10) + 10);
                                else bt.Location = new Point(allKeys[3][2].Width + (k * (bt.Width + 10)) + 30, j * (bt.Height + 10) + 10);
                                k++;
                            }
                            j++;
                            k = 0;
                        }
                        allKeys[3][0].Location = new Point(allKeys[0][11].Location.X + allKeys[0][11].Width + 10, allKeys[0][11].Location.Y);
                        allKeys[3][0].Size = allKeys[0][9].Size;
                        allKeys[3][0].BackgroundImage = new Bitmap(Form1.BcspBG, allKeys[3][0].Size);
                        allKeys[3][1].Location = new Point(allKeys[1][10].Location.X + allKeys[1][10].Width + 10, allKeys[1][10].Location.Y);
                        allKeys[3][1].Size = new Size((allKeys[1][8].Width * 3) / 2, allKeys[1][8].Height);
                        allKeys[3][1].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][1].Size);
                        allKeys[3][3].Location = new Point(allKeys[2][10].Location.X + allKeys[2][10].Width + 10, allKeys[2][10].Location.Y);
                        allKeys[3][3].Size = allKeys[3][2].Size;
                        allKeys[3][3].BackgroundImage = new Bitmap(Form1.lShiftBG, allKeys[3][3].Size);
                        allKeys[3][4].Location = new Point(allKeys[0][0].Location.X, allKeys[2][0].Location.Y + allKeys[2][0].Height + 10);
                        allKeys[3][4].Size = allKeys[2][0].Size;
                        allKeys[3][4].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][4].Size);
                        allKeys[3][5].Location = new Point(allKeys[3][4].Location.X + allKeys[3][4].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][5].Size = allKeys[3][4].Size;
                        allKeys[3][5].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][5].Size);
                        allKeys[3][6].Location = new Point(allKeys[3][5].Location.X + 10 + allKeys[3][5].Width, allKeys[3][4].Location.Y);
                        allKeys[3][6].Size = new Size(8 * allKeys[2][0].Width + 70, allKeys[2][0].Height);
                        allKeys[3][6].BackgroundImage = new Bitmap(Form1.SpaceBG, allKeys[3][6].Size);
                        allKeys[3][7].Location = new Point(allKeys[3][6].Location.X + allKeys[3][6].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][7].Size = new Size(allKeys[2][0].Width * 3 / 2 + 5, allKeys[2][0].Height);
                        allKeys[3][7].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][7].Size);
                        allKeys[3][8].Location = new Point(allKeys[3][7].Location.X + allKeys[3][7].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][8].Size = allKeys[3][7].Size;
                        allKeys[3][8].BackgroundImage = new Bitmap(Form1.HideKeyBG, allKeys[3][8].Size);
                        break;
                    }
                case "Рус":
                    {
                        allKeys[3][2].Size = new Size(70, 80);
                        allKeys[3][2].Location = new Point(20, 2 * (allKeys[3][2].Height + 10) + 10);
                        allKeys[3][2].BackgroundImage = new Bitmap(Form1.lShiftBG, allKeys[3][2].Size);
                        foreach (Button[] bts in allKeys)
                        {
                            if (j == 3) break;
                            foreach (Button bt in bts)
                            {
                                if (j == 0) bt.Text = Form1.alphabetRus[k].ToString();
                                if (j == 1) bt.Text = Form1.alphabetRus[k + 12].ToString();
                                if (j == 2) bt.Text = Form1.alphabetRus[k + 23].ToString();
                                bt.Size = new Size(70, 80);
                                bt.BackgroundImage = new Bitmap(Form1.KeyBg, bt.Size);
                                if (j % 3 != 2) bt.Location = new Point((j % 2 * (bt.Width + 10) / 2) + (k * (bt.Width + 10)) + 20, j * (bt.Height + 10) + 10);
                                else bt.Location = new Point(allKeys[3][2].Width + (k * (bt.Width + 10)) + 30, j * (bt.Height + 10) + 10);
                                k++;
                            }
                            j++;
                            k = 0;
                        }
                        allKeys[3][0].Location = new Point(allKeys[0][11].Location.X + allKeys[0][11].Width + 10, allKeys[0][11].Location.Y);
                        allKeys[3][0].Size = allKeys[0][9].Size;
                        allKeys[3][0].BackgroundImage = new Bitmap(Form1.BcspBG, allKeys[3][0].Size);
                        allKeys[3][1].Location = new Point(allKeys[1][10].Location.X + allKeys[1][10].Width + 10, allKeys[1][10].Location.Y);
                        allKeys[3][1].Size = new Size((allKeys[1][8].Width * 3) / 2, allKeys[1][8].Height);
                        allKeys[3][1].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][1].Size);
                        allKeys[3][3].Location = new Point(allKeys[2][10].Location.X + allKeys[2][10].Width + 10, allKeys[2][10].Location.Y);
                        allKeys[3][3].Size = allKeys[3][2].Size;
                        allKeys[3][3].BackgroundImage = new Bitmap(Form1.lShiftBG, allKeys[3][3].Size);
                        allKeys[3][4].Location = new Point(allKeys[0][0].Location.X, allKeys[2][0].Location.Y + allKeys[2][0].Height + 10);
                        allKeys[3][4].Size = allKeys[2][0].Size;
                        allKeys[3][4].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][4].Size);
                        allKeys[3][5].Location = new Point(allKeys[3][4].Location.X + allKeys[3][4].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][5].Size = allKeys[3][4].Size;
                        allKeys[3][5].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][5].Size);
                        allKeys[3][6].Location = new Point(allKeys[3][5].Location.X + 10 + allKeys[3][5].Width, allKeys[3][4].Location.Y);
                        allKeys[3][6].Size = new Size(8 * allKeys[2][0].Width + 70, allKeys[2][0].Height);
                        allKeys[3][6].BackgroundImage = new Bitmap(Form1.SpaceBG, allKeys[3][6].Size);
                        allKeys[3][7].Location = new Point(allKeys[3][6].Location.X + allKeys[3][6].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][7].Size = new Size(allKeys[2][0].Width * 3 / 2 + 5, allKeys[2][0].Height);
                        allKeys[3][7].BackgroundImage = new Bitmap(Form1.LangChangeBG, allKeys[3][7].Size);
                        allKeys[3][8].Location = new Point(allKeys[3][7].Location.X + allKeys[3][7].Width + 10, allKeys[3][4].Location.Y);
                        allKeys[3][8].Size = allKeys[3][7].Size;
                        allKeys[3][8].BackgroundImage = new Bitmap(Form1.HideKeyBG, allKeys[3][8].Size);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        void wb_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.AbsoluteUri == "https://www.instagram.com/accounts/login/?force_classic_login=&next=/oauth/authorize/%3Fclient_id%3Dbc1c1266e3964b1986b6df6512a6561d%26redirect_uri%3Dhttp%3A//localhost%3A5678%26response_type%3Dtoken")
            {
                if(!pageLoaded) pageLoaded = true;
                else
                {
                    if (mes != null) mes.Close();
                    mes = new MessageForm(Form1.loc["WrongPass"], "OK");
                    mes.ShowDialog();
                    LoadInProgress.Destroy();
                }
            }
            if (e.Url.AbsoluteUri == "https://www.instagram.com/oauth/authorize/?client_id=bc1c1266e3964b1986b6df6512a6561d&redirect_uri=http://localhost:5678&response_type=token")
            {
                HtmlElementCollection elc = wb.Document.GetElementsByTagName("input");
                foreach (HtmlElement el in elc)
                {
                    if (el.GetAttribute("type").Equals("submit") && el.GetAttribute("value").Equals("Authorize"))
                    {
                        el.InvokeMember("Click");
                    }
                }
            }
            if (e.Url.AbsoluteUri.Contains("access_token"))
            {
                Form1.AccessToken = e.Url.AbsoluteUri.Split('#')[1].Split('=')[1];
                loggedOut = false;
                sent = false;
                this.DialogResult = DialogResult.OK;
                LoadInProgress.Destroy();
                Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer--;
            if(!KeyboardShown)
            {
                CreateKeys();
                KeyboardShown = !KeyboardShown;
            }
            if(timer == 300)
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(Form1.loc["AddTime"], Form1.loc["Yes"], Form1.loc["No"], 30);
                DialogResult r = mes.ShowDialog();
                if (r == DialogResult.OK) timer = 1800;
                if (r == DialogResult.Cancel) timer = 1;
            }
            if(timer == 0)
            {
                DialogResult = DialogResult.Cancel;
                Form1.WaitTimeLeft = 1;
                Close();
            }
        }
    }
} 

/* Keyboard
        INPUT[] inp;
        bool shifted = false;
        string[] Alphabet = new string[]
        {
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
            "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
            "a", "s", "d", "f", "g", "h", "j", "k", "l", 
            "z", "x", "c", "v", "b", "n", "m",
            "`", "-", "=", "[", "]", "\\", ";", "'", ",", ".", "/"
        };

        [DllImport("user32.dll")]
        internal static extern uint SendInput(uint nInputs,
            [MarshalAs(UnmanagedType.LPArray), In] INPUT[] pInputs,
            int cbSize);
 DoubleBufferedPanel panel1;

 void CreateKeyboard()
 {
     panel1.Controls.Clear();
     int k = 0;
     for (int i = 0; i < 36; i++)
     {
         if (i == 10 || i == 20 || i == 29) k++;
         NonFocusableButton b = new NonFocusableButton();
         b.Text = Alphabet[i];
         b.Name = "KEY_" + Alphabet[i].ToUpper();
         b.Size = new Size(70, 70);
         b.Font = new Font("PF Centro Slab Pro", 18f);
         b.FlatStyle = FlatStyle.Flat;
         b.FlatAppearance.BorderSize = 0;
         b.FlatAppearance.CheckedBackColor = Color.Transparent;
         b.FlatAppearance.MouseDownBackColor = Color.Transparent;
         b.FlatAppearance.MouseOverBackColor = Color.Transparent;
         b.ForeColor = Color.White;
         b.BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), b.Size);
         b.Click += new EventHandler(b_Click);
         b.MouseDown += new MouseEventHandler(Key_MouseDown);
         b.MouseUp += new MouseEventHandler(Key_MouseUp);
         panel1.Controls.Add(b);
         b.Location = new Point(20 + ((k == 0) ? i * 75 + 70 : (k == 1) ? (i - 10) * 75 + 15 : (k == 2)? (i - 20) * 75 + 40 : (i-29) * 75 + 125), 10 + k * 75);
     }
     for (int i = 36; i < Alphabet.Length; i++)
     {
         NonFocusableButton b = new NonFocusableButton();
         b.Text = Alphabet[i];
         b.Size = new Size(70, 70);
         b.Font = new Font("PF Centro Slab Pro", 18f);
         b.FlatStyle = FlatStyle.Flat;
         b.FlatAppearance.BorderSize = 0;
         b.FlatAppearance.CheckedBackColor = Color.Transparent;
         b.FlatAppearance.MouseDownBackColor = Color.Transparent;
         b.FlatAppearance.MouseOverBackColor = Color.Transparent;
         b.ForeColor = Color.White;
         b.BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), b.Size);
         b.Click += new EventHandler(b_Click);
         b.MouseDown += new MouseEventHandler(Key_MouseDown);
         b.MouseUp += new MouseEventHandler(Key_MouseUp);
         panel1.Controls.Add(b);
         switch (i)
         {
             case 36: // ` ~
                 {
                     b.Name = "`";
                     b.Location = new Point(10, 10);
                     break;
                 }
             case 37: // - _
                 {
                     b.Name = "-";
                     b.Location = new Point(840, 10);
                     break;
                 }
             case 38: // = +
                 {
                     b.Name = "=";
                     b.Location = new Point(915, 10);
                     break;
                 }
             case 39: // [ {
                 {
                     b.Name = "[";
                     b.Location = new Point(785, 85);
                     break;
                 }
             case 40: // ] }
                 {
                     b.Name = "]";
                     b.Location = new Point(860, 85);
                     break;
                 }
             case 41: // \ |
                 {
                     b.Name = "\\";
                     b.Location = new Point(935, 85);
                     b.Size = new Size(115, 70);
                     b.BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), b.Size);
                     break;
                 }
             case 42: // ; :
                 {
                     b.Name = ";";
                     b.Location = new Point(735, 160);
                     break;
                 }
             case 43: // ' "
                 {
                     b.Name = "'";
                     b.Location = new Point(810, 160);
                     break;
                 }
             case 44: // , <
                 {
                     b.Name = ",";
                     b.Location = new Point(670, 235);
                     break;
                 }
             case 45: // . >
                 {
                     b.Name = ".";
                     b.Location = new Point(745, 235);
                     break;
                 }
             case 46: // / ?
                 {
                     b.Name = "/";
                     b.Location = new Point(820, 235);
                     break;
                 }
             default:
                 {
                     break;
                 }
         }
     }
     NonFocusableButton bttn = new NonFocusableButton();
     bttn.Name = "LShift";
     bttn.Size = new Size(120, 70);
     bttn.FlatStyle = FlatStyle.Flat;
     bttn.FlatAppearance.BorderSize = 0;
     bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseDownBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseOverBackColor = Color.Transparent; 
     bttn.BackgroundImage = new Bitmap(Image.FromFile("LShift.jpg"), bttn.Size);
     bttn.ForeColor = Color.White; 
     bttn.Click += new EventHandler(b_Click);
     bttn.MouseDown += new MouseEventHandler(Key_MouseDown);
     bttn.MouseUp += new MouseEventHandler(Key_MouseUp); panel1.Controls.Add(bttn);
     bttn.Location = new Point(20, 235);
     bttn = new NonFocusableButton();
     bttn.Name = "RShift";
     bttn.Size = new Size(160, 70);
     bttn.FlatStyle = FlatStyle.Flat;
     bttn.FlatAppearance.BorderSize = 0;
     bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseDownBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseOverBackColor = Color.Transparent;
     bttn.BackgroundImage = new Bitmap(Image.FromFile("RShift.jpg"), bttn.Size);
     bttn.ForeColor = Color.White;
     bttn.Click += new EventHandler(b_Click);
     bttn.MouseDown += new MouseEventHandler(Key_MouseDown);
     bttn.MouseUp += new MouseEventHandler(Key_MouseUp); 
     panel1.Controls.Add(bttn);
     bttn.Location = new Point(900, 235);
     bttn = new NonFocusableButton();
     bttn.Name = "Space";
     bttn.Size = new Size(620, 70);
     bttn.FlatStyle = FlatStyle.Flat;
     bttn.FlatAppearance.BorderSize = 0;
     bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseDownBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseOverBackColor = Color.Transparent;
     bttn.BackgroundImage = new Bitmap(Image.FromFile("Space.jpg"), bttn.Size);
     bttn.ForeColor = Color.White;
     bttn.Click += new EventHandler(b_Click);
     bttn.MouseDown += new MouseEventHandler(Key_MouseDown);
     bttn.MouseUp += new MouseEventHandler(Key_MouseUp); panel1.Controls.Add(bttn);
     panel1.Controls.Add(bttn);
     bttn.Location = new Point(200, 310);
     bttn = new NonFocusableButton();
     bttn.Name = "Enter";
     bttn.Size = new Size(120, 70);
     bttn.Font = new Font("PF Centro Slab Pro", 18f);
     bttn.Text = "Enter";
     bttn.FlatStyle = FlatStyle.Flat;
     bttn.FlatAppearance.BorderSize = 0;
     bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseDownBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseOverBackColor = Color.Transparent;
     bttn.BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), bttn.Size);
     bttn.ForeColor = Color.White;
     bttn.Click += new EventHandler(b_Click);
     bttn.MouseDown += new MouseEventHandler(Key_MouseDown);
     bttn.MouseUp += new MouseEventHandler(Key_MouseUp); panel1.Controls.Add(bttn);
     panel1.Controls.Add(bttn);
     bttn.Location = new Point(885, 160);
     bttn = new NonFocusableButton();
     bttn.Name = "Backspace";
     bttn.Size = new Size(70, 70);
     bttn.FlatStyle = FlatStyle.Flat;
     bttn.FlatAppearance.BorderSize = 0;
     bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseDownBackColor = Color.Transparent;
     bttn.FlatAppearance.MouseOverBackColor = Color.Transparent;
     bttn.BackgroundImage = new Bitmap(Image.FromFile("Bcsp.jpg"), bttn.Size);
     bttn.ForeColor = Color.White;
     bttn.Click += new EventHandler(b_Click);
     bttn.MouseDown += new MouseEventHandler(Key_MouseDown);
     bttn.MouseUp += new MouseEventHandler(Key_MouseUp); panel1.Controls.Add(bttn);
     panel1.Controls.Add(bttn);
     bttn.Location = new Point(990, 10);
 }

 void b_Click(object sender, EventArgs e)
 {
     timer = 180;
     Button s = (Button)sender;
     switch (s.Name)
     {
         case "KEY_1":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_1,
                                 wVk = VirtualKeyShort.KEY_1
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_1,
                             wVk = VirtualKeyShort.KEY_1,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_2":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_2,
                                 wVk = VirtualKeyShort.KEY_2
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_2,
                             wVk = VirtualKeyShort.KEY_2,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_3":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_3,
                                 wVk = VirtualKeyShort.KEY_3
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_3,
                             wVk = VirtualKeyShort.KEY_3,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_4":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_4,
                                 wVk = VirtualKeyShort.KEY_4
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_4,
                             wVk = VirtualKeyShort.KEY_4,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_5":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_5,
                                 wVk = VirtualKeyShort.KEY_5
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_5,
                             wVk = VirtualKeyShort.KEY_5,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_6":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_6,
                                 wVk = VirtualKeyShort.KEY_6
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_6,
                             wVk = VirtualKeyShort.KEY_6,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_7":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_7,
                                 wVk = VirtualKeyShort.KEY_7
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_7,
                             wVk = VirtualKeyShort.KEY_7,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_8":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_8,
                                 wVk = VirtualKeyShort.KEY_8
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_8,
                             wVk = VirtualKeyShort.KEY_8,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_9":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_9,
                                 wVk = VirtualKeyShort.KEY_9
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_9,
                             wVk = VirtualKeyShort.KEY_9,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_0":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_0,
                                 wVk = VirtualKeyShort.KEY_0
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_0,
                             wVk = VirtualKeyShort.KEY_0,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_A":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_A,
                                 wVk = VirtualKeyShort.KEY_A
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_A,
                             wVk = VirtualKeyShort.KEY_A,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_B":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_B,
                                 wVk = VirtualKeyShort.KEY_B
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_B,
                             wVk = VirtualKeyShort.KEY_B,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_C":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_C,
                                 wVk = VirtualKeyShort.KEY_C
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_C,
                             wVk = VirtualKeyShort.KEY_C,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_D":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_D,
                                 wVk = VirtualKeyShort.KEY_D
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_D,
                             wVk = VirtualKeyShort.KEY_D,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_E":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_E,
                                 wVk = VirtualKeyShort.KEY_E
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_E,
                             wVk = VirtualKeyShort.KEY_E,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_F":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_F,
                                 wVk = VirtualKeyShort.KEY_F
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_F,
                             wVk = VirtualKeyShort.KEY_F,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_G":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_G,
                                 wVk = VirtualKeyShort.KEY_G
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_G,
                             wVk = VirtualKeyShort.KEY_G,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_H":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_H,
                                 wVk = VirtualKeyShort.KEY_H
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_H,
                             wVk = VirtualKeyShort.KEY_H,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_I":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_I,
                                 wVk = VirtualKeyShort.KEY_I
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_I,
                             wVk = VirtualKeyShort.KEY_I,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_J":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_J,
                                 wVk = VirtualKeyShort.KEY_J
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_J,
                             wVk = VirtualKeyShort.KEY_J,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_K":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_K,
                                 wVk = VirtualKeyShort.KEY_K
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_K,
                             wVk = VirtualKeyShort.KEY_K,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_L":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_L,
                                 wVk = VirtualKeyShort.KEY_L
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_L,
                             wVk = VirtualKeyShort.KEY_L,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_M":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_M,
                                 wVk = VirtualKeyShort.KEY_M
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_M,
                             wVk = VirtualKeyShort.KEY_M,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_N":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_N,
                                 wVk = VirtualKeyShort.KEY_N
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_N,
                             wVk = VirtualKeyShort.KEY_N,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_O":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_O,
                                 wVk = VirtualKeyShort.KEY_O
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_O,
                             wVk = VirtualKeyShort.KEY_O,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_P":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_P,
                                 wVk = VirtualKeyShort.KEY_P
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_P,
                             wVk = VirtualKeyShort.KEY_P,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_Q":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_Q,
                                 wVk = VirtualKeyShort.KEY_Q
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_Q,
                             wVk = VirtualKeyShort.KEY_Q,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_R":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_R,
                                 wVk = VirtualKeyShort.KEY_R
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_R,
                             wVk = VirtualKeyShort.KEY_R,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_S":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_S,
                                 wVk = VirtualKeyShort.KEY_S
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_S,
                             wVk = VirtualKeyShort.KEY_S,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_T":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_T,
                                 wVk = VirtualKeyShort.KEY_T
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_T,
                             wVk = VirtualKeyShort.KEY_T,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_U":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_U,
                                 wVk = VirtualKeyShort.KEY_U
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_U,
                             wVk = VirtualKeyShort.KEY_U,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_V":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_V,
                                 wVk = VirtualKeyShort.KEY_V
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_V,
                             wVk = VirtualKeyShort.KEY_V,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_W":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_W,
                                 wVk = VirtualKeyShort.KEY_W
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_W,
                             wVk = VirtualKeyShort.KEY_W,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_X":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_X,
                                 wVk = VirtualKeyShort.KEY_X
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_X,
                             wVk = VirtualKeyShort.KEY_X,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_Y":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_Y,
                                 wVk = VirtualKeyShort.KEY_Y
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_Y,
                             wVk = VirtualKeyShort.KEY_Y,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "KEY_Z":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.KEY_Z,
                                 wVk = VirtualKeyShort.KEY_Z
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.KEY_Z,
                             wVk = VirtualKeyShort.KEY_Z,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "-":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_MINUS,
                                 wVk = VirtualKeyShort.OEM_MINUS
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_MINUS,
                             wVk = VirtualKeyShort.OEM_MINUS,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "=":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_PLUS,
                                 wVk = VirtualKeyShort.OEM_PLUS
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_PLUS,
                             wVk = VirtualKeyShort.OEM_PLUS,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "`":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_3,
                                 wVk = VirtualKeyShort.OEM_3
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_3,
                             wVk = VirtualKeyShort.OEM_3,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "[": // [ {
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_4,
                                 wVk = VirtualKeyShort.OEM_4
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_4,
                             wVk = VirtualKeyShort.OEM_4,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "]": // ] }
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_6,
                                 wVk = VirtualKeyShort.OEM_6
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_6,
                             wVk = VirtualKeyShort.OEM_6,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "\\": // \ |
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_5,
                                 wVk = VirtualKeyShort.OEM_5
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_5,
                             wVk = VirtualKeyShort.OEM_5,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case ";": // ; :
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_1,
                                 wVk = VirtualKeyShort.OEM_1
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_1,
                             wVk = VirtualKeyShort.OEM_1,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "'": // ' "
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_7,
                                 wVk = VirtualKeyShort.OEM_7
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_7,
                             wVk = VirtualKeyShort.OEM_7,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case ",": // , <
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_COMMA,
                                 wVk = VirtualKeyShort.OEM_COMMA
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_COMMA,
                             wVk = VirtualKeyShort.OEM_COMMA,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case ".": // . >
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_PERIOD,
                                 wVk = VirtualKeyShort.OEM_PERIOD
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_PERIOD,
                             wVk = VirtualKeyShort.OEM_PERIOD,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "/": // / ?
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.OEM_2,
                                 wVk = VirtualKeyShort.OEM_2
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.OEM_2,
                             wVk = VirtualKeyShort.OEM_2,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "RShift":
         case "LShift":
             {
                 Control L = panel1.Controls.Find("LShift", false)[0];
                 Control R = panel1.Controls.Find("RShift", false)[0];
                 if (!shifted)
                 {
                     inp = new INPUT[] 
                     { 
                         new INPUT()
                         {
                             type = InputType.KEYBOARD,
                             U = new InputUnion
                             {
                                 ki = new KEYBDINPUT()
                                 {
                                     wScan = ScanCodeShort.SHIFT,
                                     wVk = VirtualKeyShort.SHIFT
                                 }
                             }
                         }
                     };
                     L.BackgroundImage = new Bitmap(Image.FromFile("LShift_Hight.jpg"), L.Size);
                     R.BackgroundImage = new Bitmap(Image.FromFile("RShift_Hight.jpg"), R.Size);
                 }
                 else
                 {
                     inp = new INPUT[]
                     {
                         new INPUT()
                         {
                             type = InputType.KEYBOARD,
                             U = new InputUnion
                             {
                                 ki = new KEYBDINPUT()
                                 {
                                     wScan = ScanCodeShort.SHIFT,
                                     wVk = VirtualKeyShort.SHIFT,
                                     dwFlags = KEYEVENTF.KEYUP
                                 }
                             }
                         }
                     };
                     L.BackgroundImage = new Bitmap(Image.FromFile("LShift.jpg"), L.Size);
                     R.BackgroundImage = new Bitmap(Image.FromFile("RShift.jpg"), R.Size);
                 }
                 foreach (Button b in panel1.Controls)
                 {
                     if
                         (b.Name.StartsWith("KEY_") && 
                         !(b.Name.EndsWith("1")||b.Name.EndsWith("2")||b.Name.EndsWith("3")||b.Name.EndsWith("4")||b.Name.EndsWith("5")
                         ||b.Name.EndsWith("6")|| b.Name.EndsWith("7")||b.Name.EndsWith("8")||b.Name.EndsWith("9")||b.Name.EndsWith("0")))
                         b.Text = shifted ? b.Text.ToLower() : b.Text.ToUpper();
                     else switch (b.Name)
                         {
                         case "KEY_1":
                                 {
                                     b.Text = shifted ? "1" : "!";
                                     break;
                                 }
                         case "KEY_2":
                                 {
                                     b.Text = shifted ? "2" : "@";
                                     break;
                                 }
                         case "KEY_3":
                                 {
                                     b.Text = shifted ? "3" : "#";
                                     break;
                                 }
                         case "KEY_4":
                                 {
                                     b.Text = shifted ? "4" : "$";
                                     break;
                                 }
                         case "KEY_5":
                                 {
                                     b.Text = shifted ? "5" : "%";
                                     break;
                                 }
                         case "KEY_6":
                                 {
                                     b.Text = shifted ? "6" : "^";
                                     break;
                                 }
                         case "KEY_7":
                                 {
                                     b.Text = shifted ? "7" : "&";
                                     break;
                                 }
                         case "KEY_8":
                                 {
                                     b.Text = shifted ? "8" : "*";
                                     break;
                                 }
                         case "KEY_9":
                                 {
                                     b.Text = shifted ? "9" : "(";
                                     break;
                                 }
                         case "KEY_0":
                                 {
                                     b.Text = shifted ? "0" : ")";
                                     break;
                                 }
                         case "`": // ` ~
                                 {
                                     b.Text = shifted ? "`" : "~";
                                     break;
                                 }
                         case "-": // - _
                                 {
                                     b.Text = shifted ? "-" : "_";
                                     break;
                                 }
                         case "=": // = +
                                 {
                                     b.Text = shifted ? "=" : "+";
                                     break;
                                 }
                         case "[": // [ {
                                 {
                                     b.Text = shifted ? "[" : "{";
                                     break;
                                 }
                         case "]": // ] }
                                 {
                                     b.Text = shifted ? "]" : "}";
                                     break;
                                 }
                         case "\\": // \ |
                                 {
                                     b.Text = shifted ? "\\" : "|";
                                     break;
                                 }
                         case ";": // ; :
                                 {
                                     b.Text = shifted ? ";" : ":";
                                     break;
                                 }
                         case "'": // ' "
                                 {
                                     b.Text = shifted ? "'" : "\"";
                                     break;
                                 }
                         case ",": // , <
                                 {
                                     b.Text = shifted ? "," : "<";
                                     break;
                                 }
                         case ".": // . >
                                 {
                                     b.Text = shifted ? "." : ">";
                                     break;
                                 }
                         case "/": // / ?
                                 {
                                     b.Text = shifted ? "/" : "?";
                                     break;
                                 }
                         default:
                                 {
                                     break;
                                 }
                         }
                 }
                 shifted = !shifted;
                 break;
             }
         case "Space":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.SPACE,
                                 wVk = VirtualKeyShort.SPACE
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.SPACE,
                             wVk = VirtualKeyShort.SPACE,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "Enter":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.RETURN,
                                 wVk = VirtualKeyShort.RETURN
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.RETURN,
                             wVk = VirtualKeyShort.RETURN,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         case "Backspace":
             {
                 inp = new INPUT[] 
                 { 
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                                 wScan = ScanCodeShort.BACK,
                                 wVk = VirtualKeyShort.BACK
                             }
                         }
                     },
                     new INPUT()
                     {
                         type = InputType.KEYBOARD,
                         U = new InputUnion
                         {
                             ki = new KEYBDINPUT()
                             {
                             wScan = ScanCodeShort.BACK,
                             wVk = VirtualKeyShort.BACK,
                             dwFlags = KEYEVENTF.KEYUP
                             }
                         }
                     }
                 };
                 break;
             }
         default:
             {
                 return;
             }
     }
     if(inp != null) SendInput((uint)inp.Length, inp, INPUT.Size);
     inp = null;
 }

 private void Key_MouseUp(object sender, MouseEventArgs e)
 {
     Button s = (Button)sender;
     if (s.Name.StartsWith("KEY_"))
     {
         s.BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), s.Size);
     }
     else switch (s.Name)
         {
             case "Backspace":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Bcsp.jpg"), s.Size);
                     break;
                 }
             case "Enter":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), s.Size);
                     break;
                 }
             case "RShift":
             case "LShift":
                 {
                     break;
                 }
             case "Space":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Space.jpg"), s.Size);
                     break;
                 }
             case "HdKey":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Kbrd.jpg"), s.Size);
                     return;
                 }
             default:
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Key.jpg"), s.Size);
                     break;
                 }
         }
 }

 private void Key_MouseDown(object sender, MouseEventArgs e)
 {
     Button s = (Button)sender;
     if (s.Name.StartsWith("KEY_"))
     {
         s.BackgroundImage = new Bitmap(Image.FromFile("Key_Hight.jpg"), s.Size);
     }
     else switch (s.Name)
         {
             case "Backspace":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Bcsp_Hight.jpg"), s.Size);
                     break;
                 }
             case "Enter":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Key_Hight.jpg"), s.Size);
                     break;
                 }
             case "RShift":
             case "LShift":
                 {
                     break;
                 }
             case "Space":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Space_Hight.jpg"), s.Size);
                     break;
                 }
             case "HdKey":
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Kbrd_Hight.jpg"), s.Size);
                     return;
                 }
             default:
                 {
                     s.BackgroundImage = new Bitmap(Image.FromFile("Key_Hight.jpg"), s.Size);
                     break;
                 }
         }
 }
 */
