﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Net;
using System.Drawing.Printing;
using Newtonsoft.Json.Linq;
using netfx;
using CCNet;
using TermDevises;
using System.Threading;
using System.Reflection;
using Genesis.Localization;
using System.ComponentModel;
using System.Text.RegularExpressions;
using InVendApi;
using InVendApi.InVendMonitorApi;
using OnlineMonitorEnums;
using Microsoft.Win32;
using System.IO.Ports;

namespace Lkbox
{
    public partial class Form1 : Form
    {
        #region Variables
        const UInt32 WM_CLOSE = 0x0010;
        MessageForm mes;
        Login log;
        int selfTimerSecs;
        bool justCameToPage = true;
        string inpTemp;
        const int VIDEODEVICE = 0;
        const int VIDEOWIDTH = 640;
        const int VIDEOHEIGHT = 480;
        const short VIDEOBITSPERPIXEL = 24;
        State m_State = State.Uninit;
        DxPlay m_play = null;
        InVendMonitorApi api;
        bool saveChanges = false;
        RegisterState rs = RegisterState.AlreadyRegister;
        HttpListener hl;
        Random rnd;
        bool isShown = false;
        Label picsSelected;
        int paperAmm, timerSecs = 5;
        string siteName = "online.invend.ru", terminalHash, phoneNum = "none";
        double photoPrice;
        PictureBox[] cart, filters, userPics;
        private Capture cam;
        string strClientID = "bc1c1266e3964b1986b6df6512a6561d", strAccID;
        string nextPageUrl = null;
        string imageUrl = null;
        static Image[] nxtImages = new Image[30];
        string[] fullImgUrls = new string[30];
        List<string[]> imgsUrls = new List<string[]>();
        List<ImageWithText[]> imgs = new List<ImageWithText[]>();
        List<ImageWithText> chosenImages = new List<ImageWithText>();
        List<int[]> imageNumbers = new List<int[]>();
        List<PictureBox[]> pics = new List<PictureBox[]>();
        Image imToProcess;
        static string token = "";
        ImageWithText Original;
        BitmapW imForFilters;
        SerialPort mySerialPort;
        byte timePast;
        string phoneSupport;
        int slfTimerCnt;
        int cashInside = 0;
        byte swtch = 0;
        int chngNum = -1, cntr;
        effects ef = new effects();
        IntPtr m_ip = IntPtr.Zero;
        Casher cashr = new Casher();
        InstagramUser[] usersFromSearch = new InstagramUser[30];
        string[] tagsFromSearch = new string[5];
        Label[][] labelsForResults = new Label[4][];
        Panel[] Usernames, Tags;
        Button[][] KeyboardButtons;
        Button[] u, m, l, spc;
        string portname = "";
        string textPrevUser = "", textPrevTag = "";
        public static int cash = 0;
        int toPay = 0;
        int bttnRptTmr = 0;
        string bttnName = "";
        bool shiftPressed = false, tmrChecked = true;
        Thread thrU, thrH, Pr;
        object userLock = new object(), tagLock = new object();
        ManualResetEvent[] doneEvents;
        float wdth, hght;
        bool fromCart = true;
        public static LocalizationManager lm;
        string curLocale;
        List<string> whitelist = new List<string>(), blacklist = new List<string>();
        List<string> locales = new List<string>();
        public static LocalizationPackage loc;
        int searchPause = 0;
        RegisterState s;
        byte timeToPrint;
        bool scrolling;
        int MousePos, CursorPos;
        public static int WaitTimeLeft = 10;
        bool fromMain = true;
        bool timeout = false;
        int printTime = 0;
        #region Alphabets
        public static char[] alphabetEng = new char[] 
            { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 
                'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
                'z', 'x', 'c', 'v', 'b', 'n', 'm', '#', '.' },
            alphabetSymb = new char[] 
            { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '!', '*', 
                '-', '=', '_', '[', ']', '{', '}', '\\', '^', '%', '@', 
                ':', ',', '$', '<', '>', '/', '?', '`', '~', '(', ')'},
            alphabetRus = new char[]
            { 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ',
                'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э',
                'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', '#', '.'};
        #endregion
        #region Images
        Image BG;
        Image loadingGif = Image.FromFile("loading.gif");
        public static Image KeyboardBG = Image.FromFile("Board.jpg");
        public static Image KeyBg = Image.FromFile("Key.jpg"), KeyHightBg = Image.FromFile("Key_Hight.jpg");
        public static Image HideKeyBG = Image.FromFile("Kbrd.jpg"), HideKeyHightBG = Image.FromFile("Kbrd_Hight.jpg");
        public static Image BcspBG = Image.FromFile("Bcsp.jpg"), BcspHightBG = Image.FromFile("Bcsp_Hight.jpg");
        public static Image LangChangeBG = Image.FromFile("LngCh.jpg"), LangChangeHightBG = Image.FromFile("LngCh_Hight.jpg");
        public static Image lShiftBG = Image.FromFile("LShift.jpg"), lShiftHightBG = Image.FromFile("LShift_Hight.jpg");
        public static Image rShiftBG = Image.FromFile("RShift.jpg"), rShiftHightBG = Image.FromFile("RShift_Hight.jpg");
        public static Image SpaceBG = Image.FromFile("Space.jpg"), SpaceHightBG = Image.FromFile("Space_Hight.jpg");
        Image PhotoAdd = Image.FromFile("PhotoAdd.jpg");
        Image ArrLeft = Image.FromFile("ArrUpEn.png"), ArrRight;
        Image bttnEnabledBG = Image.FromFile("bttnEn.jpg"), bttnDisabledBG = Image.FromFile("bttnDis.jpg");
        Image TimerCh = Image.FromFile("TimerChecked.png"), TimerUn = Image.FromFile("TimerUnchecked.png");
        Image FiltLeft = Image.FromFile("ArrLeftFilt.png"), FiltRight;
        #endregion
        bool SearchScroll = false;
        bool filtersScroll = false;
        int startMousePos;
        bool cartScroll = false;
        #endregion

        enum State
        {
            Uninit,
            Stopped,
            Paused,
            Playing
        }

        #region Properties
        public static Image[] NextImages
        {
            get { return nxtImages; }
        }

        public static string AccessToken
        {
            get { return token; }
            set { token = value; }
        }
        #endregion

        #region Methods

        void LogOutFromInstagram()
        {
            if (log != null) log.Close();
            log = new Login("", "", "", "", "");
            log.LogOut();
            log.Close();
        }

        void Down()
        {
            WebClient wr = new WebClient();
            try
            {
                File.Move("info.jpg", "info.jpg.bckp");
                wr.DownloadFile(new Uri("http://online.invend.ru/api/GetImage/?hash=" + terminalHash), "info.jpg");
            }
            catch (Exception ex)
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(ex.Message, "Ok");
                mes.ShowDialog();
            }
        }

        void ShowAdminPanel()
        {
            foreach (Control c in Controls) c.Visible = false;
            WaitModeTmr.Stop();
            BackgroundImage = null;
            BackColor = Color.White;
            Label lbl = new Label();
            lbl.Name = "AdmPassLbl";
            lbl.AutoSize = true;
            lbl.Font = new Font("PF Centro Slab Pro", 16);
            lbl.Text = loc["Pass"];
            TextBox txt = new TextBox();
            txt.Name = "AdmPass";
            txt.Font = new Font("PF Centro Slab Pro", 16);
            txt.Size = new Size(500, 80);
            txt.PasswordChar = '@';
            txt.UseSystemPasswordChar = true;
            Button bttn = new Button();
            bttn.Name = "AdmChkPass";
            bttn.Text = loc["Enter"];
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.MouseClick += CheckPassword;
            bttn.Size = new Size(200,50);
            Controls.Add(bttn);
            Controls.Add(lbl);
            Controls.Add(txt);
            lbl.Location = new Point(200,100);
            txt.Location = new Point(220 + lbl.Width, 100);
            bttn.Location = new Point(200, 220 + lbl.Height);
            bttn = new Button();
            bttn.Name = "GoMain";
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.Text = loc["ToMainPage"];
            bttn.Size = new Size(200, 50);
            bttn.Click += ReturnToMain;
            Controls.Add(bttn);
            bttn.Location = new Point(850, 350);
        }

        void EnterAdminPanel()
        {
            Controls.RemoveByKey("AdmPass");
            Controls.RemoveByKey("AdmPassLbl");
            Controls.RemoveByKey("AdmChkPass");
            Controls.RemoveByKey("GoMain");
            Label lbl = new Label();
            Button bttn = new Button();
            TextBox txt = new TextBox();
            lbl.Text = loc["PhotoPri"];
            lbl.AutoSize = true;
            lbl.Name = "PhoPri";
            lbl.Font = new Font("PF Centro Slab Pro", 16);
            txt.Font = new Font("PF Centro Slab Pro", 16);
            txt.Size = new Size(200, 50);
            txt.Name = "PhoPriVal";
            txt.Text = photoPrice.ToString();
            txt.TextChanged += TextIsChanged;
            Controls.Add(lbl);
            Controls.Add(txt);
            lbl.Location = new Point(50, 50);
            txt.Location = new Point(300, 50);
            lbl = new Label();
            txt = new TextBox();
            lbl.Text = loc["SupportNum"];
            lbl.AutoSize = true;
            lbl.Name = "SupNum";
            lbl.Font = new Font("PF Centro Slab Pro", 16);
            txt.Font = new Font("PF Centro Slab Pro", 16);
            txt.Size = new Size(200, 50);
            txt.Name = "SupNumVal";
            txt.Text = phoneSupport;
            txt.TextChanged += TextIsChanged;
            Controls.Add(lbl);
            Controls.Add(txt);
            lbl.Location = new Point(50, 200);
            txt.Location = new Point(300, 200);
            lbl = new Label();
            txt = new TextBox();
            lbl.Text = loc["SlfTm"];
            lbl.AutoSize = true;
            lbl.Name = "SelfTmr";
            lbl.Font = new Font("PF Centro Slab Pro", 16);
            txt.Font = new Font("PF Centro Slab Pro", 16);
            txt.Size = new Size(200, 50);
            txt.Name = "SelfTmrVal";
            txt.Text = selfTimerSecs.ToString();
            txt.TextChanged += TextIsChanged;
            Controls.Add(lbl);
            Controls.Add(txt);
            lbl.Location = new Point(50, 350);
            txt.Location = new Point(300, 350);
            lbl = new Label();
            txt = new TextBox();
            lbl.Text = loc["PrntTmr"];
            lbl.AutoSize = true;
            lbl.Name = "PrntTm";
            lbl.Font = new Font("PF Centro Slab Pro", 16);
            txt.Font = new Font("PF Centro Slab Pro", 16);
            txt.Size = new Size(200, 50);
            txt.Name = "PrntTmVal";
            txt.Text = printTime.ToString();
            txt.TextChanged += TextIsChanged;
            Controls.Add(lbl);
            Controls.Add(txt);
            lbl.Location = new Point(50, 500);
            txt.Location = new Point(300, 500);
            bttn.Name = "SaveSet";
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.Text = loc["Save"];
            bttn.Size = new Size(200, 50);
            bttn.Click += SaveChanges;
            Controls.Add(bttn);
            bttn.Location = new Point(850, 50);
            bttn = new Button();
            bttn.Name = "Exit";
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.Text = loc["CloseProg"];
            bttn.Size = new Size(200, 50);
            bttn.Click += CloseProgram;
            Controls.Add(bttn);
            bttn.Location = new Point(850, 200);
            bttn = new Button();
            bttn.Name = "GoMain";
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.Text = loc["ToMainPage"];
            bttn.Size = new Size(200, 50);
            bttn.Click += ReturnToMain;
            Controls.Add(bttn);
            bttn.Location = new Point(850, 350);
            ComboBox cbx = new ComboBox();
            cbx.Name = "cbx";
            cbx.DropDownStyle = ComboBoxStyle.DropDownList;
            cbx.Size = new Size(200, 50);
            cbx.Font = new Font("PF Centro Slab Pro", 16);
            cbx.Items.AddRange(new string[] { loc["Blacklist"], loc["Whitelist"] });
            cbx.SelectedIndex = 0; 
            cbx.SelectedIndexChanged += cbxIndexChange;
            ListBox lbx = new ListBox();
            lbx.Name = "lbx";
            lbx.Font = new Font("PF Centro Slab Pro", 16);
            lbx.Size = new Size(540, 600);
            lbx.Items.Clear();
            lbx.Items.AddRange(blacklist.ToArray());
            Controls.Add(cbx);
            Controls.Add(lbx);
            cbx.Location = new Point(50, 650);
            lbx.Location = new Point(50, 750);
            bttn = new Button();
            bttn.Name = "AddList";
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.Text = loc["AddToList"];
            bttn.Size = new Size(300, 50);
            bttn.Click += AddToList;
            Controls.Add(bttn);
            bttn.Location = new Point(650, 900);
            bttn = new Button();
            bttn.Name = "RemoveList";
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.Text = loc["RemoveFromList"];
            bttn.Size = new Size(300, 50);
            bttn.Click += RemoveFromList;
            Controls.Add(bttn);
            bttn.Location = new Point(650, 1000);
            bttn = new Button();
            bttn.Name = "NewPaper";
            bttn.Font = new Font("PF Centro Slab Pro", 16);
            bttn.Text = loc["PaperInPrinter"];
            bttn.Size = new Size(400, 50);
            bttn.Click += PrinterRefresh;
            Controls.Add(bttn);
            bttn.Location = new Point(340, 1400);
            txt = new TextBox();
            txt.Font = new Font("PF Centro Slab Pro", 16);
            txt.Size = new Size(300, 50);
            txt.Name = "AddTxt";
            Controls.Add(txt);
            txt.Location = new Point(650, 800);
        }

        void PrinterRefresh(object sender, EventArgs e)
        {
            paperAmm = 695;
            saveChanges = true;
        }

        void TextIsChanged(object sender, EventArgs e)
        {
            saveChanges = true;
        }

        void ReturnToMain(object sender, EventArgs e)
        {
            if (saveChanges)
            {
                DialogResult d = MessageBox.Show("Save changes?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (d == DialogResult.Yes)
                {
                    if (!double.TryParse(Controls["PhoPriVal"].Text, out photoPrice))
                    {
                        MessageBox.Show("Error: photo price");
                        Controls["PhoPriVal"].Focus();
                        return;
                    }
                    phoneSupport = Controls["SupNumVal"].Text;
                    if (!int.TryParse(Controls["SelfTmrVal"].Text, out selfTimerSecs))
                    {
                        MessageBox.Show("Error: Selfie timer");
                        Controls["SelfTmrVal"].Focus();
                        return;
                    }
                    if (!int.TryParse(Controls["PrntTmVal"].Text, out printTime))
                    {
                        MessageBox.Show("Error: Print time");
                        Controls["PrntTmVal"].Focus();
                        return;
                    }
                    SaveToIni();
                    WriteToBlacklist();
                    WriteToWhitelist();
                    MessageBox.Show("Saved");
                }
                if (d == DialogResult.Cancel) return;
            }
            Controls.RemoveByKey("PhoPri");
            Controls.RemoveByKey("PhoPriVal");
            Controls.RemoveByKey("SupNum");
            Controls.RemoveByKey("SupNumVal");
            Controls.RemoveByKey("SelfTmr");
            Controls.RemoveByKey("SelfTmrVal");
            Controls.RemoveByKey("PrntTm");
            Controls.RemoveByKey("PrntTmVal");
            Controls.RemoveByKey("SaveSet");
            Controls.RemoveByKey("AddTxt");
            Controls.RemoveByKey("RemoveList");
            Controls.RemoveByKey("AddList");
            Controls.RemoveByKey("Exit");
            Controls.RemoveByKey("GoMain");
            Controls.RemoveByKey("cbx");
            Controls.RemoveByKey("lbx");
            reset();
            WaitModeTmr.Start();
        }

        void AddToList(object sender, EventArgs e)
        {
            if (Controls["AddTxt"].Text == "") return;
            ((ListBox)Controls["lbx"]).Items.Add(Controls["AddTxt"].Text.ToLower());
            if((string)((ComboBox)Controls["cbx"]).SelectedItem == "Blacklist")
            {
                blacklist.Add(Controls["AddTxt"].Text.ToLower());
            }
            else
            {
                whitelist.Add(Controls["AddTxt"].Text.ToLower());
            }
            Controls["AddTxt"].Text = "";
        }

        void RemoveFromList(object sender, EventArgs e)
        {
            if(((ListBox)Controls["lbx"]).SelectedIndex != -1)
            {
                if ((string)((ComboBox)Controls["cbx"]).SelectedItem == "Blacklist")
                {
                    blacklist.Remove((((ListBox)Controls["lbx"]).SelectedItem.ToString()));
                }
                else
                {
                    whitelist.Remove(((string)((ListBox)Controls["lbx"]).SelectedItem));
                }
                ((ListBox)Controls["lbx"]).Items.Remove(((ListBox)Controls["lbx"]).SelectedItem);
            }
            else
            {
                if (((ListBox)Controls["lbx"]).FindStringExact(Controls["AddTxt"].Text, 0) != -1)
                {
                    if ((string)((ComboBox)Controls["cbx"]).SelectedItem == "Blacklist")
                    {
                        blacklist.Remove(Controls["AddTxt"].Text);
                    }
                    else
                    {
                        whitelist.Remove(Controls["AddTxt"].Text);
                    }
                    ((ListBox)Controls["lbx"]).Items.Remove(Controls["AddTxt"].Text);
                }
            }
        }

        void WriteToBlacklist()
        {
            using (StreamWriter sw = new StreamWriter("blacklist", false, Encoding.Unicode))
            {
                sw.WriteLine("//Каждая строка, начинающаяся с \"//\" считается комментарием. Строки, начинающиеся с любого другого символа, будут внесены в чёрный список.");
                sw.WriteLine("//Every string starting with \"//\" is considered as comment. String starting with any other symbol are blacklisted.");
                foreach (string s in blacklist) sw.WriteLine(s);
            }
        }

        void WriteToWhitelist()
        {
            using (StreamWriter sw = new StreamWriter("whitelist", false, Encoding.Unicode))
            {
                sw.WriteLine("//Каждая строка, начинающаяся с \"//\" считается комментарием. Строки, начинающиеся с любого другого символа, будут внесены в белый список.");
                sw.WriteLine("//Every string starting with \"//\" is considered as comment. String starting with any other symbol are whitelisted.");
                foreach (string s in whitelist) sw.WriteLine(s);
            }
        }

        void cbxIndexChange(object sender, EventArgs e)
        {
            if ((string)((ComboBox)Controls["cbx"]).SelectedItem == loc["Blacklist"])
            {
                ((ListBox)Controls["lbx"]).Items.Clear();
                ((ListBox)Controls["lbx"]).Items.AddRange(blacklist.ToArray());
            }
            else
            {
                ((ListBox)Controls["lbx"]).Items.Clear();
                ((ListBox)Controls["lbx"]).Items.AddRange(whitelist.ToArray());
            }
        }

        void SaveChanges(object sender, EventArgs e)
        {
            if(!double.TryParse(Controls["PhoPriVal"].Text, out photoPrice))
            {
                MessageBox.Show("Error: photo price");
                Controls["PhoPriVal"].Focus();
                return;
            }
            phoneSupport = Controls["SupNumVal"].Text;
            if(!int.TryParse(Controls["SelfTmrVal"].Text, out selfTimerSecs))
            {
                MessageBox.Show("Error: Selfie timer");
                Controls["SelfTmrVal"].Focus();
                return;
            }
            if (!int.TryParse(Controls["PrntTmVal"].Text, out printTime))
            {
                MessageBox.Show("Error: Print time");
                Controls["PrntTmVal"].Focus();
                return;
            }
            SaveToIni();
            WriteToBlacklist();
            WriteToWhitelist();
            saveChanges = false;
            MessageBox.Show("Saved");
        }

        void CloseProgram(object sender, EventArgs e)
        {
            if (saveChanges)
            {
                DialogResult d = MessageBox.Show("Save changes?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (d == DialogResult.Yes)
                {
                    if (!double.TryParse(Controls["PhoPriVal"].Text, out photoPrice))
                    {
                        MessageBox.Show("Error: photo price");
                        Controls["PhoPriVal"].Focus();
                        return;
                    }
                    phoneSupport = Controls["SupNumVal"].Text;
                    if (!int.TryParse(Controls["SelfTmrVal"].Text, out selfTimerSecs))
                    {
                        MessageBox.Show("Error: Selfie timer");
                        Controls["SelfTmrVal"].Focus();
                        return;
                    }
                    if (!int.TryParse(Controls["PrntTmVal"].Text, out printTime))
                    {
                        MessageBox.Show("Error: Print time");
                        Controls["PrntTmVal"].Focus();
                        return;
                    }
                    SaveToIni();
                    WriteToBlacklist();
                    WriteToWhitelist();
                    MessageBox.Show("Saved");
                }
                if (d == DialogResult.Cancel) return;
            }
            Close();
        }

        void CheckPassword(object sender, MouseEventArgs e)
        {
            if (Controls["AdmPass"].Text == "password")
            {
                EnterAdminPanel();
            }
            else return;
        }

        void Received(IAsyncResult result)
        {
            HttpListener listener = (HttpListener)result.AsyncState;
            HttpListenerContext context = listener.EndGetContext(result);
            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;
            string responseString = "<html><body>" + loc["LoginSuccess"] + "</body></html>";
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();
            hl.BeginGetContext(new AsyncCallback(Received), hl);
        }

        private bool CheckBlacklist(string query)
        {
            bool blacklisted = false;
            query = query.ToLower();
            foreach (string s in blacklist)
            {
                if (query.Contains(s))
                {
                    blacklisted = true;
                    break;
                }
            }
            if (blacklisted) blacklisted = !CheckWhitelist(query);
            return blacklisted;
        }

        private bool CheckWhitelist(string query)
        {
            bool whitelisted = false;
            query = query.ToLower();
            foreach (string s in whitelist)
            {
                if (query.Contains(s))
                {
                    whitelisted = true;
                    break;
                }
            }
            return whitelisted;
        }

        private void LoadBlacklists()
        {
            string temp;
            using (StreamReader sr = new StreamReader("blacklist", Encoding.Unicode))
            {
                while (sr.Peek() != -1)
                {
                    temp = sr.ReadLine();
                    if (!temp.StartsWith("//")) blacklist.Add(temp.ToLower());
                }
            }
            using (StreamReader sr = new StreamReader("whitelist", Encoding.Unicode))
            {
                while (sr.Peek() != -1)
                {
                    temp = sr.ReadLine();
                    if (!temp.StartsWith("//")) whitelist.Add(temp.ToLower());
                }
            }
        }

        void LoadFromIni()
        {
            string[] strings = new string[6];
            using (StreamReader sr = new StreamReader("config.ini"))
            {
                for (int i = 0; i < 6; i++) strings[i] = sr.ReadLine();
                phoneSupport = strings[0].Split(' ')[1];
                photoPrice = Double.Parse(strings[1].Split(' ')[1]);
                selfTimerSecs = int.Parse(strings[2].Split(' ')[1]);
                terminalHash = strings[3].Split(' ')[1];
                paperAmm = int.Parse(strings[4].Split(' ')[1]);
                printTime = int.Parse(strings[5].Split(' ')[1]);
            }
            RegistryKey r = null;
            try
            {
                r = Registry.CurrentUser.OpenSubKey("Software\\Invend");
                if ((string)r.GetValue("hash") != terminalHash)
                {
                    if (mes != null) mes.Close();
                    mes = new MessageForm("Хэш терминала в файле config.ini был изменён. \nПрограмма будет закрыта", "Ok");
                    mes.ShowDialog();
                    Close();
                }
            }
            catch
            {
                if (mes != null) mes.Close();
                mes = new MessageForm("Программа не была установлена.\n Установите программу с помощью установщика", "Ok");
                mes.ShowDialog();
                Close();
            }
            finally
            {
                r.Close();
            }
        }

        void SaveToIni()
        {
            using (StreamWriter sw = new StreamWriter("config.ini"))
            {
                sw.WriteLine("Support: " + phoneSupport);
                sw.WriteLine("Price: " + photoPrice);
                sw.WriteLine("Timer: " + selfTimerSecs);
                sw.WriteLine("Hash: " + terminalHash);
                sw.WriteLine("Paper: " + paperAmm);
                sw.WriteLine("PrintTime: " + printTime);
            }
        }

        void SetLang(string locale)
        {
            loc = lm.Load(locale);
            curLocale = loc.Culture;
            MainScreenLbl.Text = loc["ProgName"];
            PrintPhoneLbl.Text = loc["PrintPhone"];
            MkSelfieLbl.Text = loc["MakeSelfi"];
            VideoFeedbackLbl.Text = loc["Respond"];
            RstBttn.Text = loc["ToMainPage"];
            RtnMain.Text = loc["ToMainPage"];
            RetakeSelf.Text = loc["RePhoto"];
            PhotoOk.Text = loc["NextPage"];
            CartOk.Text = loc["Paying"];
            BttnSearchBack.Text = loc["Back"];
            Next.Text = loc["NextPage"];
            Prev.Text = loc["Last"];
            FiltersBack.Text = loc["Back"];
            FiltersOk.Text = loc["NextPage"];
            BalLbl.Text = loc["Balance"];
            CashPaid.Text = cash + " " + loc["Rub"];
            InfoBttn.Text = loc["Info"];
            InfoClose.Text = loc["Close"];
            Print.Text = loc["Print"];
            PrintBack.Text = loc["Back"];
            PrintEven.Text = loc["OnlyEven"];
            SearchBack.Text = loc["SearchMore"];
            InstaSearch.Text = loc["Search"];
            InactivityTest.Text = loc["TuchHere"];
            OrderDel.Text = loc["DelOrder"];
            CartLbl.Text = loc["Basket"];
            PaidCashLbl.Text = loc["CashPaid"];
            NeedCashLbl.Text = loc["CashNeeded"];
            OrderedLbl.Text = loc["YourOrder"];
            PrintingLbl.Text = loc["PrintPhoto"];
            AcceptedCashLbl.Text = loc["CashAccepted"];
            SupportLbl.Text = loc["Help", phoneSupport];
            NoCoinsLbl.Text = loc["WithoutChange"];
            DoNotForgetLbl.Text = loc["Forget"];
            AutoMainLbl.Text = loc["AutoToMain", 15];
            ThanksLbl.Text = loc["Thanks"];
            EditCancel.Text = loc["Back"];
            EditOk.Text = loc["Save"];
            ToEditLabel.Text = loc["ToEdit"];
            Search.Text = rtnMnPnl.Controls["Srch"].Text = loc["Search"];
        }

        void PlayVideo()
        {
            rnd = new Random();
            if (m_State == State.Playing)
            {
                m_play.Stop();
            }
            // If necessary, close the old file
            if (m_State == State.Stopped)
            {
                m_play.Dispose();
                m_play = null;
                m_State = State.Uninit;
            }

            try
            {
                string[] s = Directory.GetFiles("videos");
                // Open the file, provide a handle to play it in
                this.Invoke((MethodInvoker)(() => VideoPlay.Visible = true));
                this.Invoke((MethodInvoker)(() => m_play = new DxPlay(VideoPlay, s[rnd.Next(0, s.Length)])));
                // Let us know when the file is finished playing
                m_play.StopPlay += new DxPlay.DxPlayEvent(m_play_StopPlay);
            }
            catch (COMException ce)
            {
                this.Invoke((MethodInvoker)(() => VideoPlay.Visible = false));
                if (mes != null) mes.Close();
                mes = new MessageForm("Failed to open file: " + ce.Message, "OK");
                mes.ShowDialog();
            }
            m_play.Start();
            m_State = State.Playing;
        }

        private void m_play_StopPlay(Object sender)
        {
            m_play.Stop();
            m_State = State.Stopped;
            this.Invoke((MethodInvoker)(() => VideoPlay.Visible = false));
            WaitTimeLeft = 10;
        }

        void StopVideo()
        {
            if (m_State == State.Playing || m_State == State.Paused)
            {
                m_play.Stop();
                this.Invoke((MethodInvoker)(() => VideoPlay.Visible = false));
                m_play.Dispose();
                m_play = null;
                m_State = State.Uninit;
            }
        }

        void ApplyAlphabet(string lang)
        {
            byte j = 0, k = 0;
            lShift.Size = new Size((int)(80 * wdth), (int)(80 * wdth));
            lShift.Location = new Point(20, 2 * (lShift.Height + 10) + 10);
            lShift.BackgroundImage = new Bitmap(lShiftBG, lShift.Size);
            switch (lang)
            {
                case "Eng":
                    {
                        foreach (Button[] bts in KeyboardButtons)
                        {
                            if (j == 3) break;
                            foreach (Button bt in bts)
                            {
                                if (j == 0 && k == 10) continue;
                                if (j == 1 && k == 9) continue;
                                if (j == 2 && k == 9) continue;
                                if (j == 0 && k < 10) bt.Text = alphabetEng[k].ToString();
                                if (j == 1 && k < 9) bt.Text = alphabetEng[k + 10].ToString();
                                if (j == 2 && k < 9) bt.Text = alphabetEng[k + 19].ToString();
                                bt.Size = new Size((int)(80 * wdth), (int)(80 * wdth));
                                bt.BackgroundImage = new Bitmap(KeyBg, bt.Size);
                                if (j % 3 != 2) bt.Location = new Point((j % 2 * (bt.Width + 20) / 2) + (k * (bt.Width + 10)) + 20, j * (bt.Height + 10) + 10);
                                else bt.Location = new Point(lShift.Width + (k * (bt.Width + 10)) + 30, j * (bt.Height + 10) + 10);
                                k++;
                            }
                            j++;
                            k = 0;
                        }
                        u11.Size = u12.Size = m10.Size = m11.Size = l10.Size = l11.Size = new Size();
                        bcsp.Location = new Point(u10.Location.X + u10.Width + 10, u10.Location.Y);
                        bcsp.Size = u10.Size;
                        bcsp.BackgroundImage = new Bitmap(BcspBG, bcsp.Size);
                        Search.Location = new Point(m9.Location.X + m9.Width + 10, m9.Location.Y);
                        Search.Size = new Size((m9.Width * 3) / 2, m9.Height);
                        Search.BackgroundImage = new Bitmap(LangChangeBG, Search.Size);
                        rShift.Location = new Point(l9.Location.X + l9.Width + 10, l9.Location.Y);
                        rShift.Size = new Size(6 * lShift.Width / 5, lShift.Height);
                        rShift.BackgroundImage = new Bitmap(rShiftBG, rShift.Size);
                        lSymb.Location = new Point(u1.Location.X, l1.Location.Y + l1.Height + 10);
                        lSymb.Size = l1.Size;
                        lSymb.BackgroundImage = new Bitmap(LangChangeBG, lSymb.Size);
                        langChg.Location = new Point(lSymb.Location.X + lSymb.Width + 10, lSymb.Location.Y);
                        langChg.Size = l1.Size;
                        langChg.BackgroundImage = new Bitmap(LangChangeBG, langChg.Size);
                        Space.Location = new Point(langChg.Location.X + 10 + langChg.Width, lSymb.Location.Y);
                        Space.Size = new Size(7 * l1.Width + 60, l1.Height);
                        Space.BackgroundImage = new Bitmap(SpaceBG, Space.Size);
                        rSymb.Location = new Point(Space.Location.X + Space.Width + 10, lSymb.Location.Y);
                        rSymb.Size = rShift.Size;
                        rSymb.BackgroundImage = new Bitmap(LangChangeBG, rSymb.Size);
                        HdKey.Location = new Point(rSymb.Location.X + rSymb.Width + 10, lSymb.Location.Y);
                        HdKey.Size = u1.Size;
                        HdKey.BackgroundImage = new Bitmap(HideKeyBG, HdKey.Size);
                        break;
                    }
                case "Symb":
                    {
                        lShift.Size = new Size((int)(70 * wdth), (int)(80 * wdth));
                        lShift.Location = new Point(20, 2 * (lShift.Height + 10) + 10);
                        lShift.BackgroundImage = new Bitmap(lShiftBG, lShift.Size);
                        foreach (Button[] bts in KeyboardButtons)
                        {
                            if (j == 3) break;
                            foreach (Button bt in bts)
                            {
                                if (j == 0) bt.Text = alphabetSymb[k].ToString();
                                if (j == 1) bt.Text = alphabetSymb[k + 12].ToString();
                                if (j == 2) bt.Text = alphabetSymb[k + 23].ToString();
                                bt.Size = new Size((int)(70 * wdth), (int)(80 * wdth));
                                bt.BackgroundImage = new Bitmap(KeyBg, bt.Size);
                                if (j % 3 != 2) bt.Location = new Point((j % 2 * (bt.Width + 10) / 2) + (k * (bt.Width + 10)) + 20, j * (bt.Height + 10) + 10);
                                else bt.Location = new Point(lShift.Width + (k * (bt.Width + 10)) + 30, j * (bt.Height + 10) + 10);
                                k++;
                            }
                            j++;
                            k = 0;
                        }
                        bcsp.Location = new Point(u12.Location.X + u12.Width + 10, u12.Location.Y);
                        bcsp.Size = u10.Size;
                        bcsp.BackgroundImage = new Bitmap(BcspBG, bcsp.Size);
                        Search.Location = new Point(m11.Location.X + m11.Width + 10, m11.Location.Y);
                        Search.Size = new Size((m9.Width * 3) / 2, m9.Height);
                        Search.BackgroundImage = new Bitmap(LangChangeBG, Search.Size);
                        rShift.Location = new Point(l11.Location.X + l11.Width + 10, l11.Location.Y);
                        rShift.Size = lShift.Size;
                        rShift.BackgroundImage = new Bitmap(lShiftBG, rShift.Size);
                        lSymb.Location = new Point(u1.Location.X, l1.Location.Y + l1.Height + 10);
                        lSymb.Size = l1.Size;
                        lSymb.BackgroundImage = new Bitmap(LangChangeBG, lSymb.Size);
                        langChg.Location = new Point(lSymb.Location.X + lSymb.Width + 10, lSymb.Location.Y);
                        langChg.Size = lSymb.Size;
                        langChg.BackgroundImage = new Bitmap(LangChangeBG, langChg.Size);
                        Space.Location = new Point(langChg.Location.X + 10 + langChg.Width, lSymb.Location.Y);
                        Space.Size = new Size(8 * l1.Width + 70, l1.Height);
                        Space.BackgroundImage = new Bitmap(SpaceBG, Space.Size);
                        rSymb.Location = new Point(Space.Location.X + Space.Width + 10, lSymb.Location.Y);
                        rSymb.Size = new Size(l1.Width * 3 / 2 + 5, l1.Height);
                        rSymb.BackgroundImage = new Bitmap(LangChangeBG, rSymb.Size);
                        HdKey.Location = new Point(rSymb.Location.X + rSymb.Width + 10, lSymb.Location.Y);
                        HdKey.Size = rSymb.Size;
                        HdKey.BackgroundImage = new Bitmap(HideKeyBG, HdKey.Size);
                        break;
                    }
                case "Рус":
                    {
                        lShift.Size = new Size((int)(70 * wdth), (int)(80 * wdth));
                        lShift.Location = new Point(20, 2 * (lShift.Height + 10) + 10);
                        lShift.BackgroundImage = new Bitmap(lShiftBG, lShift.Size);
                        foreach (Button[] bts in KeyboardButtons)
                        {
                            if (j == 3) break;
                            foreach (Button bt in bts)
                            {
                                if (j == 0) bt.Text = alphabetRus[k].ToString();
                                if (j == 1) bt.Text = alphabetRus[k + 12].ToString();
                                if (j == 2) bt.Text = alphabetRus[k + 23].ToString();
                                bt.Size = new Size((int)(70 * wdth), (int)(80 * wdth));
                                bt.BackgroundImage = new Bitmap(KeyBg, bt.Size);
                                if (j % 3 != 2) bt.Location = new Point((j % 2 * (bt.Width + 10) / 2) + (k * (bt.Width + 10)) + 20, j * (bt.Height + 10) + 10);
                                else bt.Location = new Point(lShift.Width + (k * (bt.Width + 10)) + 30, j * (bt.Height + 10) + 10);
                                k++;
                            }
                            j++;
                            k = 0;
                        }
                        bcsp.Location = new Point(u12.Location.X + u12.Width + 10, u12.Location.Y);
                        bcsp.Size = u10.Size;
                        bcsp.BackgroundImage = new Bitmap(BcspBG, bcsp.Size);
                        Search.Location = new Point(m11.Location.X + m11.Width + 10, m11.Location.Y);
                        Search.Size = new Size((m9.Width * 3) / 2, m9.Height);
                        Search.BackgroundImage = new Bitmap(LangChangeBG, Search.Size);
                        rShift.Location = new Point(l11.Location.X + l11.Width + 10, l11.Location.Y);
                        rShift.Size = lShift.Size;
                        rShift.BackgroundImage = new Bitmap(lShiftBG, rShift.Size);
                        lSymb.Location = new Point(u1.Location.X, l1.Location.Y + l1.Height + 10);
                        lSymb.Size = l1.Size;
                        lSymb.BackgroundImage = new Bitmap(LangChangeBG, lSymb.Size);
                        langChg.Location = new Point(lSymb.Location.X + lSymb.Width + 10, lSymb.Location.Y);
                        langChg.Size = lSymb.Size;
                        langChg.BackgroundImage = new Bitmap(LangChangeBG, langChg.Size);
                        Space.Location = new Point(langChg.Location.X + 10 + langChg.Width, lSymb.Location.Y);
                        Space.Size = new Size(8 * l1.Width + 70, l1.Height);
                        Space.BackgroundImage = new Bitmap(SpaceBG, Space.Size);
                        rSymb.Location = new Point(Space.Location.X + Space.Width + 10, lSymb.Location.Y);
                        rSymb.Size = new Size(l1.Width * 3 / 2 + 5, l1.Height);
                        rSymb.BackgroundImage = new Bitmap(LangChangeBG, rSymb.Size);
                        HdKey.Location = new Point(rSymb.Location.X + rSymb.Width + 10, lSymb.Location.Y);
                        HdKey.Size = rSymb.Size;
                        HdKey.BackgroundImage = new Bitmap(HideKeyBG, HdKey.Size);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        private void TakeSelfie()
        {
            // Release any previous buffer
            if (m_ip != IntPtr.Zero)
            {
                Marshal.FreeCoTaskMem(m_ip);
                m_ip = IntPtr.Zero;
            }
            // capture image
            m_ip = cam.Click();
            Bitmap b = new Bitmap(cam.Width, cam.Height, cam.Stride, PixelFormat.Format24bppRgb, m_ip);
            // If the image is upsidedown
            b.RotateFlip(RotateFlipType.RotateNoneFlipY);
            b = new Bitmap(b.Clone(new Rectangle(80, 0, 480, 480), b.PixelFormat), new Size(640, 640));
            imToProcess = (Image)b.Clone();
            SelfieTake.Visible = SelfPreview.Visible = TimerChecker.Visible = TimerTime.Visible = false;
            SelfView.Location = new Point(0, SelfPreview.Location.Y);
            SelfView.Size = new Size(this.Width, this.Width);
            SelfView.Image = new Bitmap((Image)imToProcess.Clone(), SelfView.Size);
            BG = Image.FromFile("SelfieView.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            SelfView.Visible = PhotoOk.Visible = RetakeSelf.Visible = true;
        }
  
        public void nextImages(string query)
        {
            WebRequest webRequest = null;
            string[] temp;
            Encoding encode = Encoding.Default;
            if (string.IsNullOrEmpty(nextPageUrl))
                if (query.StartsWith("#")) webRequest = WebRequest.Create(String.Format
                    ("https://api.instagram.com/v1/tags/{0}/media/recent?count=30&" + ((!String.IsNullOrWhiteSpace(token) && token != "logout") ? "access_token" : "client_id") + "={1}", query.Substring(1), (!String.IsNullOrWhiteSpace(token) && token != "logout")? token : strClientID));
                else
                {
                    webRequest = WebRequest.Create(String.Format("https://instagram.com/{0}/", query));
                    try
                    {
                        webRequest.Proxy = null;
                        var resp = webRequest.GetResponse();
                        using (StreamReader reader = new StreamReader(resp.GetResponseStream(), encode))
                        {
                            strAccID = reader.ReadToEnd();
                            strAccID = strAccID.Substring(strAccID.IndexOf("window._sharedData"));
                            strAccID = strAccID.Substring(strAccID.IndexOf("\"id\""));
                            temp = strAccID.Split('"');
                            strAccID = temp[3];
                            string q = String.Format
                                ("https://api.instagram.com/v1/users/{0}/media/recent?count=30&" + ((!String.IsNullOrWhiteSpace(token) && token != "logout") ? "access_token" : "client_id") + "={1}", strAccID, (!String.IsNullOrWhiteSpace(token) && token != "logout") ? token : strClientID);
                            if (strAccID != "") webRequest = WebRequest.Create(q);
                        }
                        resp.Close();
                    }
                    catch
                    {
                        throw;
                    }
                }
            else
                webRequest = WebRequest.Create(nextPageUrl);
            webRequest.Proxy = null;
            var response = webRequest.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), encode))
            {
                ImageWithText[] im = new ImageWithText[30];
                string[] caption = new string[30];
                pics.Add(new PictureBox[30]);
                JToken tok = JObject.Parse(reader.ReadToEnd());
                var meta = tok.SelectToken("meta");
                var code = int.Parse(meta.SelectToken("code").ToString());
                if (code != 200)
                {
                    if (code == 400 && meta.SelectToken("error_type").ToString() == "APINotAllowedError") throw new Exception("Private account");
                }
                var pagination = tok.SelectToken("pagination");
                if (pagination != null && pagination.SelectToken("next_url") != null)
                {
                    nextPageUrl = pagination.SelectToken("next_url").ToString();
                }
                else
                {
                    nextPageUrl = null;
                }
                var images = (JArray)tok.SelectToken("data");
                if (images.Count == 0) throw new ArgumentOutOfRangeException();
                byte i = 0;
                doneEvents = new ManualResetEvent[images.Count];
                foreach (var image in images)
                {
                    InstaSearch f;
                    imageUrl = image.SelectToken("images").SelectToken("low_resolution").SelectToken("url").ToString();
                    pics[pics.Count - 1][i] = new PictureBox();
                    pics[pics.Count - 1][i].MouseClick += new MouseEventHandler(picClick);
                    pics[pics.Count - 1][i].MouseDown += new MouseEventHandler(picsPanel_MouseDown);
                    pics[pics.Count - 1][i].MouseMove += new MouseEventHandler(picsPanel_MouseMove);
                    pics[pics.Count - 1][i].MouseUp += new MouseEventHandler(picsPanel_MouseUp);
                    pics[pics.Count - 1][i].Name = "pic_" + pics.Count + "_" + (i + 1);
                    pics[pics.Count - 1][i].Size = new Size(200, 200);
                    pics[pics.Count - 1][i].Location = new Point(13 + (i % 5) * 212, 12 + (i / 5) * 212 + (pics.Count - 1) * 1290 + picsPanel.DisplayRectangle.Y);
                    pics[pics.Count - 1][i].Visible = true;
                    if (picsPanel.InvokeRequired) this.Invoke((MethodInvoker)(() => picsPanel.Controls.Add(pics[pics.Count - 1][i])));
                    else picsPanel.Controls.Add(pics[pics.Count - 1][i]);
                    fullImgUrls[i] = image.SelectToken("images").SelectToken("standard_resolution").SelectToken("url").ToString();
                    try
                    {
                        caption[i] = "@" + image.SelectToken("user").SelectToken("username").ToString() + Environment.NewLine +
                        image.SelectToken("caption").SelectToken("text").ToString();
                    }
                    catch
                    {
                        caption[i] = "@" + image.SelectToken("user").SelectToken("username").ToString() + Environment.NewLine;
                        continue;
                    }
                    finally
                    {
                        caption[i] = Regex.Replace(caption[i], @"[\p{Cs}\p{So}\p{Mn}]", "");
                        doneEvents[i] = new ManualResetEvent(false);
                        f = new InstaSearch(doneEvents[i], i, imageUrl);
                        ThreadPool.QueueUserWorkItem(f.ThreadPoolCallback, i);
                        i++;
                    }
                }
                for (; i < 30; i++)
                {
                    nxtImages[i] = null;
                    pics[pics.Count - 1][i] = null;
                    fullImgUrls[i] = null;
                }
                foreach (ManualResetEvent e in doneEvents)
                    if (e != null) e.WaitOne();
                i = 0;
                foreach (string s in caption)
                {
                    if (String.IsNullOrEmpty(s)) break;
                    im[i] = new ImageWithText(nxtImages[i], s);
                    pics[pics.Count - 1][i].BackgroundImage = new Bitmap (im[i].Image, 200,200);
                    i++;
                }
                imgsUrls.Add((string[])fullImgUrls.Clone());
                imgs.Add((ImageWithText[])im.Clone());
            }
            response.Close();
        }

        private void SearchForUsers(string query)
        {
            if (textPrevUser == query) return;
            lock (userLock)
            {
                textPrevUser = query;
                if (String.IsNullOrWhiteSpace(query) || (query == loc["EnterQuery"] && !fromMain))
                {
                    for (byte i = 0; i < 30; i++)
                    {
                        this.Invoke((MethodInvoker)(() => userPics[i].Image = null));
                        this.Invoke((MethodInvoker)(() => labelsForResults[0][i].Text = ""));
                        this.Invoke((MethodInvoker)(() => labelsForResults[2][i].Text = ""));
                    }
                    return;
                }
                WebRequest wr = null;
                Encoding encode = Encoding.Default;
                try
                {
                    wr = WebRequest.Create(String.Format("https://api.instagram.com/v1/users/search?q={0}&client_id={1}&count=29", query, strClientID));
                    wr.Proxy = null;
                    var resp = wr.GetResponse();
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream(), encode))
                    {
                        JToken token = JObject.Parse(reader.ReadToEnd());
                        var users = (JArray)token.SelectToken("data");
                        byte i = 0;
                        foreach (var user in users)
                        {
                            string un = user.SelectToken("username").ToString();
                            string fn = user.SelectToken("full_name").ToString();
                            long id = Int64.Parse(user.SelectToken("id").ToString());
                            Image pp;
                            WebRequest wrs = WebRequest.Create(user.SelectToken("profile_picture").ToString());
                            wrs.Proxy = null;
                            using (var respn = wrs.GetResponse())
                            {
                                pp = (Image)Image.FromStream(respn.GetResponseStream()).Clone();
                            }
                            usersFromSearch[i] = new InstagramUser(un, fn, id, pp);
                            i++;
                        }
                        while (i < 30)
                        {
                            usersFromSearch[i] = null;
                            i++;
                        }
                    }
                    if ((AccSearch.Text == loc["EnterQuery"] || AccSearch.Visible == false) && !fromMain) return;
                    for (int i = 0; i < 30; i++)
                    {
                        if (usersFromSearch[i] != null)
                        {
                            this.Invoke((MethodInvoker)(() => userPics[i].Image = new Bitmap(usersFromSearch[i].ProfilePic, userPics[i].Size)));
                            this.Invoke((MethodInvoker)(() => labelsForResults[0][i].Text = usersFromSearch[i].Username));
                            this.Invoke((MethodInvoker)(() => labelsForResults[2][i].Text = usersFromSearch[i].FullName));
                            this.Invoke((MethodInvoker)(() => Usernames[i].Visible = true));
                        }
                        else
                        {
                            this.Invoke((MethodInvoker)(() => userPics[i].Image = null));
                            this.Invoke((MethodInvoker)(() => labelsForResults[0][i].Text = ""));
                            this.Invoke((MethodInvoker)(() => Usernames[i].Visible = false));
                            this.Invoke((MethodInvoker)(() => labelsForResults[2][i].Text = ""));
                        }
                    }
                }
                catch (Exception x)
                {
                    if (mes != null) mes.Close();
                    mes = new MessageForm(x.Message, "Ok");
                    mes.ShowDialog();
                }
            }
        }

        private void SearchForHashtags(string query)
        {
            if (textPrevTag == query || textPrevTag == AccSearch.Text) return;
            lock (tagLock)
            {
                textPrevTag = query;
                if (String.IsNullOrWhiteSpace(query) || (query == loc["EnterQuery"] && !fromMain))
                {
                    for (byte i = 0; i < 30; i++)
                    {
                        this.Invoke((MethodInvoker)(() => Tags[i].Visible = false));
                        this.Invoke((MethodInvoker)(() => labelsForResults[1][i].Text = ""));
                        this.Invoke((MethodInvoker)(() => labelsForResults[3][i].Text = ""));
                    }
                    return;
                }
                tagsFromSearch = new string[30];
                string[] picsAmm = new string[30];
                WebRequest wr = null;
                Encoding encode = Encoding.Default;
                try
                {
                    wr = WebRequest.Create(String.Format("https://api.instagram.com/v1/tags/search?q={0}&client_id={1}&count=29", query.StartsWith("#") ? query.Substring(1) : query, strClientID));
                    wr.Proxy = null;
                    var resp = wr.GetResponse();
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream(), encode))
                    {
                        JToken token = JObject.Parse(reader.ReadToEnd());
                        var tags = (JArray)token.SelectToken("data");
                        byte i = 0;
                        foreach (var tag in tags)
                        {
                            tagsFromSearch[i] = tag.SelectToken("name").ToString();
                            picsAmm[i] = tag.SelectToken("media_count").ToString().Split('.')[0];
                            i++;
                        }
                    }
                    resp.Close();
                    if ((AccSearch.Text == loc["EnterQuery"] || AccSearch.Visible == false) && !fromMain) return;
                    for (int i = 0; i < 30; i++)
                    {
                        if (!String.IsNullOrWhiteSpace(tagsFromSearch[i]))
                        {
                            this.Invoke((MethodInvoker)(() => labelsForResults[1][i].Text = "#" + tagsFromSearch[i]));
                            this.Invoke((MethodInvoker)(() => labelsForResults[3][i].Text = picsAmm[i]));
                            this.Invoke((MethodInvoker)(() => labelsForResults[3][i].Location = new Point(Tags[i].Width - labelsForResults[3][i].Width - 5)));
                            this.Invoke((MethodInvoker)(() => Tags[i].Visible = true));
                        }
                        else
                        {
                            this.Invoke((MethodInvoker)(() => Tags[i].Visible = false));
                            this.Invoke((MethodInvoker)(() => labelsForResults[3][i].Text = ""));
                            this.Invoke((MethodInvoker)(() => labelsForResults[1][i].Text = ""));
                        }
                    }
                }
                catch (Exception x)
                {
                    if (mes != null) mes.Close();
                    mes = new MessageForm(x.Message + Environment.NewLine + "Tags", "Ok");
                    mes.ShowDialog();
                }
            }
        }

        void ShowAddFrom()
        {
            Controls["cartPanel"].Visible = false;
            fromCart = CartOk.Visible;
            RtnMain.Visible = CartLbl.Visible = false;
            if(fromCart) CartOk.Visible = OrderDel.Visible = false;
            else Print.Visible = PrintBack.Visible = CashAmmLbl.Visible = CashNeeded.Visible = PhotoAmmLbl.Visible = NoCoinsLbl.Visible = SupportLbl.Visible = AcceptedCashLbl.Visible = NeedCashLbl.Visible = PrintEven.Visible = PaidCashLbl.Visible = OrderedLbl.Visible = false;
            EditButtons(false);
            this.BackgroundImage = new Bitmap(Image.FromFile("WhiteBG.jpg"), this.Size);
            Button bt = new Button();
            bt.Name = "FromInstag";
            bt.Click += FromInstag_Click;
            bt.EnabledChanged += Button_EnabledChanged;
            bt.FlatStyle = FlatStyle.Flat;
            bt.FlatAppearance.BorderSize = 0;
            bt.FlatAppearance.MouseDownBackColor = Color.Transparent;
            bt.FlatAppearance.MouseOverBackColor = Color.Transparent;
            bt.BackColor = Color.Transparent;
            bt.Size = new Size(500, 120);
            bt.BackgroundImage = new Bitmap(bttnEnabledBG, bt.Size);
            PictureBox pb = new PictureBox();
            Label lbl = new Label();
            pb.Click += FromInstag_Click;
            lbl.Click += FromInstag_Click;
            pb.Size = new Size(60, 60);
            pb.Image = new Bitmap(Image.FromFile("Insta.png"), pb.Size);
            bt.Controls.Add(pb);
            pb.Location = new Point(50, 30);
            lbl.Text = loc["FromSearch"];
            lbl.AutoSize = true;
            lbl.ForeColor = Color.White;
            lbl.Font = new Font("PF Centro Slab Pro", 28);
            bt.Controls.Add(lbl);
            lbl.Location = new Point(280 - lbl.Width / 2, 60 - lbl.Height / 2);
            bt.Location = new Point(300, 600);
            Controls.Add(bt);
            bt = new Button();
            bt.Name = "AddSelfie";
            bt.Click += AddSelfie_Click;
            bt.EnabledChanged += Button_EnabledChanged;
            bt.FlatStyle = FlatStyle.Flat;
            bt.FlatAppearance.BorderSize = 0;
            bt.FlatAppearance.MouseDownBackColor = Color.Transparent;
            bt.FlatAppearance.MouseOverBackColor = Color.Transparent;
            bt.BackColor = Color.Transparent;
            bt.Size = new Size(500, 120);
            bt.BackgroundImage = new Bitmap(bttnEnabledBG, bt.Size);
            pb = new PictureBox();
            lbl = new Label();
            pb.Click += AddSelfie_Click;
            lbl.Click += AddSelfie_Click;
            pb.Size = new Size(60, 60);
            pb.Image = new Bitmap(Image.FromFile("Self.png"), pb.Size);
            bt.Controls.Add(pb);
            pb.Location = new Point(50, 30);
            lbl.Text = loc["Selfie"];
            lbl.AutoSize = true; 
            lbl.ForeColor = Color.White; 
            lbl.Font = new Font("PF Centro Slab Pro", 28);
            bt.Controls.Add(lbl);
            lbl.Location = new Point(280 - lbl.Width / 2, 60 - lbl.Height / 2);
            bt.Location = new Point(300, 900);
            Controls.Add(bt);
            bt = new Button();
            bt.Name = "CancelBttn";
            bt.Click += CancelBttn_Click;
            bt.EnabledChanged += Button_EnabledChanged;
            bt.FlatStyle = FlatStyle.Flat;
            bt.FlatAppearance.BorderSize = 0;
            bt.FlatAppearance.MouseDownBackColor = Color.Transparent;
            bt.FlatAppearance.MouseOverBackColor = Color.Transparent;
            bt.BackColor = Color.Transparent;
            bt.Size = new Size(500, 120);
            bt.BackgroundImage = new Bitmap(bttnEnabledBG, bt.Size);
            pb = new PictureBox();
            lbl = new Label();
            pb.Click += CancelBttn_Click;
            lbl.Click += CancelBttn_Click;
            pb.Size = new Size(60, 60);
            pb.Image = new Bitmap(Image.FromFile("Cancel.png"), pb.Size);
            bt.Controls.Add(pb);
            pb.Location = new Point(50, 30);
            lbl.Font = new Font("PF Centro Slab Pro", 28);
            lbl.AutoSize = true;
            lbl.Text = loc["Cancel"];
            lbl.ForeColor = Color.White;
            bt.Controls.Add(lbl);
            lbl.Location = new Point(280 - lbl.Width / 2, 60 - lbl.Height / 2);
            bt.Location = new Point(300, 1200);
            Controls.Add(bt);
        }

        void reset()
        {
            //All goes to starting conditions
            foreach (Control c in Controls) c.Visible = false;
            if(mes != null) mes.Close();
            mes = null;
            token = "logout";
            LogOutFromInstagram();
            log = null;
            tmer.Stop();
            AccSearch.Text = "";
            imgs.Clear();
            imgsUrls.Clear();
            nextPageUrl = null;
            swtch = 0;
            chngNum = -1;
            try { cam.Dispose(); }
            catch { }
            picsPanel.Controls.Clear();
            pics.Clear();
            imageNumbers.Clear();
            chosenImages.Clear();
            cartRefresh();
            cart[0].Visible = false;
            Controls["cartPanel"].Visible = false;
            cash = toPay = 0;
            CashPaid.Text = "0 " + loc["Rub"];
            SetLang(curLocale);
            BG = Image.FromFile("Main.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            InstaSearch.Visible = MkSelfie.Visible = InfoBttn.Visible = Lang1.Visible = Lang2.Visible = true;
            MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = true;
        }

        private void ApplyFilters()
        {
            LoadInProgress.Create("");
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            filters[0].Image = new Bitmap((Image)imToProcess.Clone(), filters[0].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.monochrome(imForFilters);
            filters[1].Image = new Bitmap(imForFilters.GetBitmap(), filters[1].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.sepia(imForFilters);
            filters[2].Image = new Bitmap(imForFilters.GetBitmap(), filters[2].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.contrast(imForFilters, 100);
            filters[3].Image = new Bitmap(imForFilters.GetBitmap(), filters[3].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.brighten(imForFilters, 50);
            filters[4].Image = new Bitmap(imForFilters.GetBitmap(), filters[4].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.noise(imForFilters, 30);
            filters[5].Image = new Bitmap(imForFilters.GetBitmap(), filters[5].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.brighten(imForFilters, 10);
            imForFilters = ef.saturate(imForFilters, 30);
            filters[6].Image = new Bitmap(imForFilters.GetBitmap(), filters[6].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.saturate(imForFilters, 50);
            filters[7].Image = new Bitmap(imForFilters.GetBitmap(), filters[7].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.brighten(imForFilters, -30);
            filters[8].Image = new Bitmap(imForFilters.GetBitmap(), filters[8].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.invert(imForFilters);
            filters[9].Image = new Bitmap(imForFilters.GetBitmap(), filters[9].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.contrast(imForFilters, 30);
            imForFilters = ef.posterize(imForFilters, 4);
            filters[10].Image = new Bitmap(imForFilters.GetBitmap(), filters[10].Size);
            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
            imForFilters = ef.saturate(imForFilters, 25);
            imForFilters = ef.brighten(imForFilters, -20);
            filters[11].Image = new Bitmap(imForFilters.GetBitmap(), filters[11].Size);
            LoadInProgress.Destroy();
        }

        void cartRefresh()
        {
            byte i = 0;
            Controls["cartPanel"].Visible = true;
            for (; i < 20; i++)
            {
                cart[i].Visible = false;
                cart[i].Controls.Clear();
            }
            for (i = 0; i < 20; i++)
            {
                if (cart[i].Image != null) cart[i].Image = null;
                if (i == chosenImages.Count)
                {
                    cart[i].BackgroundImage = new Bitmap(PhotoAdd, 200, 200);
                    Label l = new Label();
                    l.AutoSize = true;
                    l.Name = "add";
                    l.Text = loc["InsertPhoto"];
                    l.TextAlign = ContentAlignment.MiddleCenter;
                    l.Font = new Font("PF Centro Slab Pro", 24);
                    l.Click += new EventHandler(cartIm_Click);
                    l.ForeColor = Color.Red;
                    cart[i].Controls.Add(l);
                    l.Location = new Point(cart[i].Width / 2 - l.Width / 2, 110);
                    cart[i].Visible = true;
                    break;
                }
                cart[i].Size = new Size(200, 200);
                cart[i].BackgroundImage = new Bitmap((Image)chosenImages[i].Image.Clone(), cart[i].Size);
                cart[i].Visible = true;
            }
            EditButtons(false);
        }

        private void PrintStarted()
        {
            try
            {
                timeToPrint = (byte)((chosenImages.Count / 2 + chosenImages.Count % 2) * printTime);
                this.Invoke((MethodInvoker)(() => PrintPageLbl.Visible = true));
                this.Invoke((MethodInvoker)(() => PrintingLbl.Visible = true));
                this.Invoke((MethodInvoker)(() => PrintProgressBar.Maximum = (chosenImages.Count / 2 + chosenImages.Count % 2) * printTime));
                this.Invoke((MethodInvoker)(() => PrintProgressBar.Visible = true));
                this.Invoke((MethodInvoker)(() => PrintProgressBar.Value = 0));
                pd.Print();     //печать, используя принтер по умолчанию
                this.Invoke((MethodInvoker)(() => printTimer.Enabled = true));
                //PlayVideo();
            }
            catch
            {
                this.Invoke((MethodInvoker)(() => PrintPageLbl.Visible = false));
                this.Invoke((MethodInvoker)(() => PrintingLbl.Visible = false));
                this.Invoke((MethodInvoker)(() => PrintProgressBar.Visible = false));
                this.Invoke((MethodInvoker)(() => printTimer.Enabled = false));
                StopVideo();
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["ErrPrint"], "Ok");
                mes.ShowDialog(); 
                return;
            }
        }

        async void CheckMonitoring()
        {
            rs = await api.TryRegisterTerminalAsync();
            if(rs == RegisterState.UnknownClientError)
            {
                if (mes != null) mes.Close();
                mes = new MessageForm("Unknown terminal hash.\nProgram will close", "Ok");
                mes.ShowDialog();
                if (!isShown)
                {
                    Load += (s, e) => Close();
                    return;
                }
                else Close();
            }
        }

        private bool PingMonitoring()
        {
            switch (s)
            {
                case RegisterState.TerminalLocked:
                    {
                        return false;
                    }
                case RegisterState.AwaitOwner:
                    {
                        return false;
                    }
                default:
                    {
                        return true;
                    }
            }
        }

        private async void PurchaseDone(int printed)
        {
            await api.SendPurchaseAsync(cash, printed, phoneNum, cash - printed * photoPrice, DateTime.Now);
            await api.SendReportAsync(paperAmm);
            if (paperAmm <= 100) await api.SendlogAsync(new LogMessage(InstamatLogType.Other, "Paper amount low", DateTime.Now));
        }

        private void PrintEnded()
        {
            printTimer.Stop();
            PrintPageLbl.Visible = false;
            PrintProgressBar.Visible = false;
            PrintingLbl.Visible = false;
            BG = Image.FromFile("Thanks.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            timePast = 15;
            tmer.Start();
            AcceptedCashLbl.Visible = SupportLbl.Visible = NoCoinsLbl.Visible = true;
            ThanksLbl.Visible = DoNotForgetLbl.Visible = AutoMainLbl.Visible = true;
            RstBttn.Visible = true;
            StopVideo();
        }

        int GetLastPosOfSame(List<int[]> l, int[] s)
        {
            int i = 0, j = -1;
            foreach (int[] m in l)
            {
                if (m[0] == s[0])
                    if (m[1] == s[1]) j = i;
                i++;
            }
            return j;
        }

        int GetNumberOfSame(List<int[]> l, int[] s)
        {
            int i = 0;
            foreach (int[] m in l)
            {
                if (m[0] == s[0])
                    if (m[1] == s[1]) i++;
            }
            return i;
        }

        private void ExitEdit()
        {
            WaitTimeLeft = 1800;
            Controls.RemoveByKey("LeftArr");
            Controls.RemoveByKey("RightArr"); 
            SelfView.Visible = TurnLeft.Visible = TurnRight.Visible = InputBox.Visible = FiltersNext.Visible = FiltersPrev.Visible = false;
            EditCancel.Visible = EditOk.Visible = false;
            Keyboard.Visible = false;
            Controls["FilPan"].Visible = false;
            int w, h;
            for (byte i = 0; i < 20; i++)
            {
                w = (i % 5) * 200 + (1 + i % 5) * (this.Width - 1000) / 7;
                h = (i / 5) * 215;
                cart[i].Location = new Point(w, h);
            }
            BG = Image.FromFile("Cart.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            OrderDel.Visible = CartOk.Visible = CartLbl.Visible = true;
            cartRefresh();
            WaitTimeLeft = 1800;
        }

        private void editNextPic()
        {
            if (chngNum != chosenImages.Count - 1) chngNum++;
            else chngNum = 0;
        }

        private void editPrevPic()
        {
            if (chngNum != 0) chngNum--;
            else chngNum = chosenImages.Count - 1;
        }

        private bool connectToCoin()
        {
            bool connected = false;
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                try
                {
                    mySerialPort = new SerialPort(port);
                    mySerialPort.BaudRate = 19200;
                    mySerialPort.Parity = Parity.None;
                    mySerialPort.StopBits = StopBits.One;
                    mySerialPort.DataBits = 8;
                    mySerialPort.Handshake = Handshake.None;
                    mySerialPort.RtsEnable = true;
                    mySerialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler); //слушаем порт на фоне
                    mySerialPort.Open();
                    connected = true;
                    break;
                }
                catch
                {
                    mySerialPort.Close();
                    continue;
                }
            }
            return connected;
        }

        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e) //постоянно слушаем порт
        {
            int c;
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            byte[] ba = Encoding.Default.GetBytes(indata);
            var hexString = BitConverter.ToString(ba);
            c = Convert.ToInt32(hexString.Substring(9, 2), 16);
            cash += c;
            cashInside += c;
            this.Invoke((MethodInvoker)(() => CashAmmLbl.Text = CashPaid.Text = cash.ToString() + " " + loc["Rub"]));
            api.SendlogAsync(new LogMessage(InstamatLogType.Other, "Money inserted: " + c + "\nTotal user's cash: " + cash, DateTime.Now));
        }

        void reload()
        {
            reset();
            LoadInProgress.Destroy();
            foreach (Control c in Controls) c.Enabled = true;
        }

        void EditButtons(bool show)
        {
            Controls.RemoveByKey("PhotoEdit");
            Controls.RemoveByKey("PhotoCopy");
            Controls.RemoveByKey("PhotoChange");
            Controls.RemoveByKey("PhotoDelete");
            if (show)
            {
                Button bt = new Button();
                bt.Name = "PhotoEdit";
                bt.Click += PhotoRed_Click;
                bt.FlatStyle = FlatStyle.Flat;
                bt.FlatAppearance.BorderSize = 0;
                bt.FlatAppearance.MouseDownBackColor = Color.Transparent;
                bt.FlatAppearance.MouseOverBackColor = Color.Transparent;
                bt.BackColor = Color.Transparent;
                bt.Size = new Size(350, 90);
                bt.BackgroundImage = new Bitmap(bttnEnabledBG, bt.Size);
                PictureBox pb = new PictureBox();
                Label lbl = new Label();
                pb.Click += PhotoRed_Click;
                lbl.Click += PhotoRed_Click;
                pb.Size = new Size(50, 50);
                pb.Image = new Bitmap(Image.FromFile("edit.png"), pb.Size);
                bt.Controls.Add(pb);
                pb.Location = new Point(30, 20);
                lbl.Text = loc["Edit"];
                lbl.AutoSize = true;
                lbl.ForeColor = Color.White;
                lbl.Font = new Font("PF Centro Slab Pro", 24);
                bt.Controls.Add(lbl);
                lbl.Location = new Point(200 - lbl.Width / 2, 45 - lbl.Height / 2);
                bt.Location = new Point(120, 1200);
                Controls.Add(bt);
                bt = new Button();
                bt.Name = "PhotoChange";
                bt.Click += PhotoChange_Click;
                bt.FlatStyle = FlatStyle.Flat;
                bt.FlatAppearance.BorderSize = 0;
                bt.FlatAppearance.MouseDownBackColor = Color.Transparent;
                bt.FlatAppearance.MouseOverBackColor = Color.Transparent;
                bt.BackColor = Color.Transparent;
                bt.Size = new Size(350, 90);
                bt.BackgroundImage = new Bitmap(bttnEnabledBG, bt.Size);
                pb = new PictureBox();
                lbl = new Label();
                pb.Click += PhotoChange_Click;
                lbl.Click += PhotoChange_Click;
                pb.Size = new Size(50, 50);
                pb.Image = new Bitmap(Image.FromFile("Change.png"), pb.Size);
                bt.Controls.Add(pb);
                pb.Location = new Point(30, 20);
                lbl.Text = loc["Change"];
                lbl.AutoSize = true;
                lbl.ForeColor = Color.White;
                lbl.Font = new Font("PF Centro Slab Pro", 24);
                bt.Controls.Add(lbl);
                lbl.Location = new Point(200 - lbl.Width / 2, 45 - lbl.Height / 2);
                bt.Location = new Point(620, 1200);
                Controls.Add(bt);
                bt = new Button();
                bt.Name = "PhotoCopy";
                bt.Click += PhotoCopy_Click;
                bt.FlatStyle = FlatStyle.Flat;
                bt.FlatAppearance.BorderSize = 0;
                bt.FlatAppearance.MouseDownBackColor = Color.Transparent;
                bt.FlatAppearance.MouseOverBackColor = Color.Transparent;
                bt.BackColor = Color.Transparent;
                bt.Size = new Size(350, 90);
                if (chosenImages.Count == 20) 
                {
                    bt.Enabled = false;
                    bt.BackgroundImage = new Bitmap(bttnDisabledBG, bt.Size);
                }
                else
                {
                    bt.Enabled = true;
                    bt.BackgroundImage = new Bitmap(bttnEnabledBG, bt.Size);
                }
                pb = new PictureBox();
                lbl = new Label();
                pb.Click += PhotoCopy_Click;
                lbl.Click += PhotoCopy_Click;
                pb.Size = new Size(50, 50);
                pb.Image = new Bitmap(Image.FromFile("Copy.png"), pb.Size);
                bt.Controls.Add(pb);
                pb.Location = new Point(30, 20);
                lbl.Font = new Font("PF Centro Slab Pro", 24);
                lbl.AutoSize = true;
                lbl.Text = loc["Copy"];
                lbl.ForeColor = Color.White;
                bt.Controls.Add(lbl);
                lbl.Location = new Point(200 - lbl.Width / 2, 45 - lbl.Height / 2);
                bt.Location = new Point(120, 1320);
                Controls.Add(bt);
                bt = new Button();
                bt.Name = "PhotoDelete";
                bt.Click += PhotoDelete_Click;
                bt.FlatStyle = FlatStyle.Flat;
                bt.FlatAppearance.BorderSize = 0;
                bt.FlatAppearance.MouseDownBackColor = Color.Transparent;
                bt.FlatAppearance.MouseOverBackColor = Color.Transparent;
                bt.BackColor = Color.Transparent;
                bt.Size = new Size(350, 90);
                bt.BackgroundImage = new Bitmap(bttnEnabledBG, bt.Size);
                pb = new PictureBox();
                lbl = new Label();
                pb.Click += PhotoDelete_Click;
                lbl.Click += PhotoDelete_Click;
                pb.Size = new Size(50, 50);
                pb.Image = new Bitmap(Image.FromFile("Delete.png"), pb.Size);
                bt.Controls.Add(pb);
                pb.Location = new Point(30, 20);
                lbl.Text = loc["Delete"];
                lbl.AutoSize = true;
                lbl.ForeColor = Color.White;
                lbl.Font = new Font("PF Centro Slab Pro", 24);
                bt.Controls.Add(lbl);
                lbl.Location = new Point(200 - lbl.Width / 2, 45 - lbl.Height / 2);
                bt.Location = new Point(620, 1320);
                Controls.Add(bt);
            }
        }
        #endregion

        #region Form events
        public Form1()
        {
            if (File.Exists("config.ini"))
            {
                LoadFromIni();
            }
            else
            {
                if (mes != null) mes.Close();
                mes = new MessageForm("No config file found.\nProgram will be closed", "Ok");
                mes.ShowDialog();
                Close();
            }
            LoadBlacklists();
            api = new InVendMonitorApi(siteName, terminalHash, TerminalType.Instamat, TimeSpan.FromSeconds(20), 80, true);
            CheckMonitoring();
            ArrLeft.RotateFlip(RotateFlipType.Rotate270FlipNone);
            ArrRight = (Image)ArrLeft.Clone();
            ArrRight.RotateFlip(RotateFlipType.Rotate180FlipNone);
            FiltRight = (Image)FiltLeft.Clone();
            FiltRight.RotateFlip(RotateFlipType.Rotate180FlipNone);
            lm = new LocalizationManager();
            lm.BasePath = "Localization";
            lm.Initialize();
            try
            {
                lm.DetectAllLocalizations();
            }
            catch (LocalizationException ex)
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(ex.Message, "Ok");
                mes.ShowDialog(); 
                return;
            }
            ServicePointManager.DefaultConnectionLimit = 30;
            InitializeComponent();
            locTimer.Interval = timerSecs * 1000;
            DoubleBufferedPanel SearchPanel = new DoubleBufferedPanel();
            SearchPanel.Name = "SearchPanel";
            SearchPanel.BackColor = Color.Transparent;
            SearchPanel.Size = new Size(1200, 1050);
            SearchPanel.AutoScroll = true;
            SearchPanel.MouseDown += SearchPanel_MouseDown;
            SearchPanel.MouseUp += SearchPanel_MouseUp;
            SearchPanel.MouseMove += SearchPanel_MouseMove;
            SearchPanel.Visible = false;
            Controls.Add(SearchPanel);
            SearchPanel.Location = new Point(0, 230);
            Tags = new Panel[30];
            Usernames = new Panel[30];
            userPics = new PictureBox[30];
            labelsForResults[0] = new Label[30];
            labelsForResults[1] = new Label[30];
            labelsForResults[2] = new Label[30];
            labelsForResults[3] = new Label[30];
            for (int i = 0; i < 30; i++)
            {
                Panel p = new Panel();
                p.Click += new EventHandler(User_Click);
                p.Name = "GetTag_" + (i + 1);
                p.BackColor = Color.FromArgb(229, 227, 227);
                p.Visible = false;
                p.Size = new Size(530, 60);
                Label lb = new Label();
                lb.Click += new EventHandler(User_Click);
                lb.Name = "Tag_" + (i + 1);
                lb.Font = new Font("PF Centro Slab Pro", 18f);
                p.Controls.Add(lb);
                labelsForResults[1][i] = lb;
                lb.AutoSize = true;
                lb.Location = new Point(5, 20);
                lb = new Label();
                lb.Click += new EventHandler(User_Click);
                lb.Name = "TagPicAmm_" + (i + 1);
                lb.Font = new Font("PF Centro Slab Pro", 16f);
                p.Controls.Add(lb);
                labelsForResults[3][i] = lb;
                lb.AutoSize = true;
                lb.Location = new Point(p.Width - lb.Width - 5, 20);
                SearchPanel.Controls.Add(p);
                p.Location = new Point(550, i * 100);
                Tags[i] = p;
            }
            for (int i = 0; i < 30; i++)
            {
                Panel p = new Panel();
                p.Click += new EventHandler(User_Click);
                p.Name = "GetUser_" + (i + 1);
                p.BackColor = Color.FromArgb(229, 227, 227);
                p.Visible = false;
                p.Size = new Size(530, 60);
                PictureBox pic = new PictureBox();
                pic.Name = "UserPic_" + (i + 1);
                pic.Size = new Size(50, 50);
                p.Controls.Add(pic);
                userPics[i] = pic;
                pic.Location = new Point(5, 5);
                Label lb = new Label();
                lb.Click += new EventHandler(User_Click);
                lb.Name = "User_" + (i + 1);
                lb.Font = new Font("PF Centro Slab Pro", 22f);
                p.Controls.Add(lb);
                labelsForResults[0][i] = lb;
                lb.AutoSize = true;
                lb.Location = new Point(60, 0);
                lb = new Label();
                lb.Click += new EventHandler(User_Click);
                lb.Name = "FullName_" + (i + 1);
                lb.Font = new Font("PF Centro Slab Pro", 16f);
                p.Controls.Add(lb);
                labelsForResults[2][i] = lb;
                lb.AutoSize = true;
                lb.Location = new Point(60, 35);
                SearchPanel.Controls.Add(p);
                p.Location = new Point(5, i * 100);
                Usernames[i] = p;
            }
            foreach (Control c in SearchPanel.Controls)
            {
                foreach (Control cnt in c.Controls)
                {
                    cnt.MouseDown += SearchPanel_MouseDown;
                    cnt.MouseUp += SearchPanel_MouseUp;
                    cnt.MouseMove += SearchPanel_MouseMove;
                }
                c.MouseDown += SearchPanel_MouseDown;
                c.MouseUp += SearchPanel_MouseUp;
                c.MouseMove += SearchPanel_MouseMove;
            }
            picsSelected = new Label();
            picsSelected.Visible = false;
            picsSelected.AutoSize = true;
            picsSelected.Font = PhotoAmmLbl.Font;
            picsSelected.BackColor = Color.Transparent;
            this.Controls.Add(picsSelected);
            for (int i = 0; i < lm.Localizations.Count(); i++)
            {
                locales.Add(lm.Localizations.ElementAt(i).Info.Culture);
            }
            Button bttn = new Button();
            bttn.Name = "Srch";
            bttn.BackColor = Color.Transparent;
            bttn.FlatStyle = FlatStyle.Flat;
            bttn.FlatAppearance.BorderSize = 0;
            bttn.FlatAppearance.MouseDownBackColor = Color.Transparent;
            bttn.FlatAppearance.MouseOverBackColor = Color.Transparent;
            bttn.ForeColor = Color.White;
            bttn.Font = new Font("PF Centro Slab Pro", 32f);
            bttn.Click += SearchButtonClick;
            bttn.Size = new Size(320, 75);
            bttn.BackgroundImage = new Bitmap(bttnEnabledBG, bttn.Size);
            rtnMnPnl.Controls.Add(bttn);
            bttn.Location = new Point(700, 60 - bttn.Height / 2);
            //
            SetLang(locales[1]);
            //
            PrintController pc = new StandardPrintController();
            pd.PrintController = pc;
            wdth = (float)Screen.GetWorkingArea(this).Width / 1090.4f;
            hght = (float)Screen.GetWorkingArea(this).Height / 1941.7f;
            loadingPic.Size = loadingGif.Size;
            loadingPic.Image = loadingGif;
            loadingPic.Location = new Point(540-loadingPic.Width/2,250);
            loadingPic.Visible = false;
            picsPanel.Location = new Point(0,220);
            picsPanel.Size = new Size(1150, 1290);
            picsPanel.Visible = false;
            Lang1.Location = new Point(80, 82);
            Lang2.Location = new Point(300, 82);
            Lang1.Size = Lang2.Size = new Size(150, 70);
            Lang1.BackgroundImage = new Bitmap(bttnEnabledBG, Lang1.Size);
            Lang2.BackgroundImage = new Bitmap(bttnEnabledBG, Lang2.Size); 
            Lang1.Text = locales[0];
            Lang2.Text = locales[1];
            MkSelfie.Location = new Point(457, 1632);
            MkSelfie.Size = new Size((int)(165 * wdth),(int)(165 * hght));
            InstaSearch.Location = new Point(195, 930);
            InstaSearch.Size = new Size((int)(690 * wdth), (int)(135 * hght));
            InfoBttn.Location = new Point(870, 80);
            InfoBttn.Size = new Size((int)(150 * wdth), (int)(70 * hght));
            InfoBttn.BackgroundImage = new Bitmap(bttnEnabledBG, InfoBttn.Size);
            SelfPreview.Location = new Point((int)(-180), 520);
            SelfPreview.Size = new Size((int)(1440), (int)(1080));
            RtnMain.Location = new Point(75, 1755); 
            RtnMain.Size = new Size(310, 90);
            RtnMain.BackgroundImage = new Bitmap(bttnEnabledBG, RtnMain.Size);
            SelfieTake.Location = new Point(462, 80);
            SelfieTake.Size = MkSelfie.Size;
            MainScreenLbl.Location = new Point(540 - MainScreenLbl.Width/2, 430 - MainScreenLbl.Height/2);
            TimerChecker.Location = new Point(750, 80);
            RetakeSelf.Location = new Point(60, 160);
            RetakeSelf.Size = RtnMain.Size;
            RetakeSelf.BackgroundImage = new Bitmap(bttnEnabledBG, RtnMain.Size);
            PhotoOk.Location = new Point(730, 160);
            PhotoOk.Size = RtnMain.Size;
            PhotoOk.BackgroundImage = new Bitmap(bttnEnabledBG, RtnMain.Size);
            CartOk.Location = new Point(730, 1810);
            CartOk.Size = RtnMain.Size;
            CartOk.BackgroundImage = new Bitmap(bttnEnabledBG, RtnMain.Size);
            Print.Location = CartOk.Location;
            Print.Size = RtnMain.Size;
            Print.BackgroundImage = new Bitmap(bttnEnabledBG, RtnMain.Size);
            OrderedLbl.Location = new Point(65, 320);
            PhotoAmmLbl.Location = new Point(65, 390);
            CashNeeded.Location = new Point(640, 1030);
            NeedCashLbl.Location = new Point(200, 1030);
            CashAmmLbl.Location = new Point(640, 1140);
            PaidCashLbl.Location = new Point(200, 1140);
            PrintBack.Location = new Point(60, 1810);
            ThanksLbl.Location = new Point(Width / 2 - ThanksLbl.Width / 2, Height / 2 - ThanksLbl.Height / 2);
            AutoMainLbl.Location = new Point(Width / 2 - AutoMainLbl.Width / 2, 1090);
            DoNotForgetLbl.Location = new Point(95, 1300); 
            PrintBack.Size = Print.Size;
            AcceptedCashLbl.Location = new Point(320 - AcceptedCashLbl.Width, 1630);
            SupportLbl.Location = new Point(640 - SupportLbl.Width, 1630);
            NoCoinsLbl.Location = new Point(960 - NoCoinsLbl.Width, 1630);
            PrintBack.BackgroundImage = new Bitmap(bttnEnabledBG, RtnMain.Size);
            RstBttn.Location = PrintBack.Location;
            RstBttn.Size = Print.Size;
            RstBttn.BackgroundImage = new Bitmap(bttnEnabledBG, RtnMain.Size);
            AccSearch.Location = new Point(100, 75);
            AccSearch.Width = 570;
            AccSearch.Height = 90;
            BttnSearchBack.Size = new Size(320, 75);
            BttnSearchBack.BackgroundImage = new Bitmap(bttnEnabledBG, BttnSearchBack.Size);
            Next.Location = new Point(725, 1800);
            Prev.Location = new Point(60, 1800);
            Next.Size = Prev.Size = Print.Size;
            Next.BackgroundImage = Prev.BackgroundImage = new Bitmap(bttnEnabledBG, Prev.Size);
            SearchBack.Location = new Point(90, 57);
            SearchBack.Size = new Size((int)(910.2f * wdth), (int)(109.9f * hght));
            //FiltersOk.Location = new Point(730, 1790);
            FiltersOk.Location = new Point(730, 50);
            CashPaid.Location = new Point(1050 - CashPaid.Width, CashPaid.Location.Y);
            BalLbl.Location = new Point(CashPaid.Location.X + CashPaid.Width / 2 - BalLbl.Width / 2, BalLbl.Location.Y);
            FiltersBack.Size = new Size((int)(274 * wdth), (int)(74 * hght));
            FiltersBack.BackgroundImage = new Bitmap(bttnEnabledBG, FiltersBack.Size);
            FiltersOk.Size = FiltersBack.Size;
            FiltersOk.BackgroundImage = new Bitmap(bttnEnabledBG, FiltersBack.Size);
            FiltersNext.Size = new Size((int)(30 * wdth), (int)(40 * hght));
            FiltersPrev.Size = FiltersNext.Size;
            FiltersNext.Image = new Bitmap(FiltRight, FiltersNext.Size);
            FiltersPrev.Image = new Bitmap(FiltLeft, FiltersPrev.Size);
            InfoClose.Location = new Point(85, 1815);
            InfoClose.Size = new Size((int)(243 * wdth), (int)(72 * hght));
            InfoClose.BackgroundImage = new Bitmap(bttnEnabledBG, InfoClose.Size);
            CartLbl.Location = new Point(540 - CartLbl.Width / 2, 75);
            PrintEven.Location = new Point(100, 100);
            ToEditLabel.Location = new Point(this.Width/2 - ToEditLabel.Width/2, 1200);
            InactivityTest.Location = new Point(300,500);
            PrintingLbl.Location = new Point(540 - PrintingLbl.Width / 2, 1200);
            PrintPhoneLbl.Location = new Point(240 - PrintPhoneLbl.Width / 2, MkSelfie.Location.Y + MkSelfie.Height + 20);
            MkSelfieLbl.Location = new Point(540 - MkSelfieLbl.Width / 2, PrintPhoneLbl.Location.Y);
            VideoFeedbackLbl.Location = new Point(850 - VideoFeedbackLbl.Width / 2, PrintPhoneLbl.Location.Y);
            PrintEven.Location = new Point(540 - PrintEven.Width / 2, PrintEven.Location.Y);
            InactivityTest.Size = new Size((int)(350 * wdth), (int)(90 * hght));
            OrderDel.Location = new Point(50, 1810);
            OrderDel.Size = new Size((int)(450 * wdth), (int)(90 * hght));
            OrderDel.BackgroundImage = new Bitmap(bttnEnabledBG, OrderDel.Size);
            InputBox.Location = new Point(0, 1000);
            InputBox.Size = new Size(840, 500);
            EditCancel.Size = EditOk.Size = new Size(250, 80);
            TurnLeft.Size = TurnRight.Size = new Size(120,120);
            TurnLeft.Image = new Bitmap(Image.FromFile("TurnLeft.jpg"), TurnLeft.Size);
            TurnRight.Image = new Bitmap (Image.FromFile("TurnRight.jpg"), TurnRight.Size);
            EditOk.BackgroundImage = new Bitmap(bttnEnabledBG, EditOk.Size);
            EditCancel.BackgroundImage = new Bitmap(bttnEnabledBG, EditCancel.Size);
            AddText.Location = new Point(350, 50);
            AddText.Size = new Size(120, 120);
            AddText.Image = new Bitmap(Image.FromFile("Text.jpg"), AddText.Size);
            TextOk.Location = AddText.Location;
            TextOk.Size = AddText.Size;
            TextOk.Text = "OK";
            rtnMnPnl.Controls.Add(BttnSearchBack);
            BttnSearchBack.Location = new Point(60, 60 - BttnSearchBack.Height / 2);
            VideoPlay.Location = new Point(0, 150);
            TextOk.Image = new Bitmap(bttnEnabledBG, TextOk.Size);
            PrintProgressBar.Location = new Point(90, 1500);
            PrintProgressBar.Size = new Size(900, 40);
            #region Keyboard setting
            Keyboard.Size = new Size(1080, 370);
            Keyboard.Location = new Point(0, 1550);
            Keyboard.BackgroundImage = new Bitmap(KeyboardBG, Keyboard.Size);
            u = new Button[] { u1, u2, u3, u4, u5, u6, u7, u8, u9, u10, u11, u12 };
            m = new Button[] { m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11 };
            l = new Button[] { l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11 };
            spc = new Button[] { bcsp, Search, lShift, rShift, lSymb, langChg, Space, rSymb, HdKey };
            KeyboardButtons = new Button[][] { u, m, l, spc };
            ApplyAlphabet("Eng");
            langChg.Text = "Рус";
            lSymb.Text = "Eng";
            rSymb.Text = ".?123";
            #endregion
            DoubleBufferedPanel cartImages = new DoubleBufferedPanel();
            cartImages.Visible = false;
            cartImages.Name = "cartPanel";
            cartImages.BackColor = Color.Transparent;
            cart = new PictureBox[] { cartIm_1, cartIm_2, cartIm_3, cartIm_4, cartIm_5, cartIm_6, cartIm_7, cartIm_8, cartIm_9, cartIm_10,
            cartIm_11, cartIm_12, cartIm_13, cartIm_14, cartIm_15, cartIm_16, cartIm_17, cartIm_18, cartIm_19, cartIm_20};
            foreach (PictureBox p in cart)
            {
                p.MouseMove += cartImages_MouseMove;
                p.MouseUp += cartImages_MouseUp;
                p.MouseDown += cartImages_MouseDown;
            }
            cartImages.AutoScroll = true;
            cartImages.MouseMove += cartImages_MouseMove;
            cartImages.MouseUp += cartImages_MouseUp;
            cartImages.MouseDown += cartImages_MouseDown;
            cartImages.Size = new Size(1200, 500);
            cartImages.Controls.AddRange(cart);
            Controls.Add(cartImages);
            cartImages.Location = new Point(0, 300);
            filters = new PictureBox[] { filterPic1, filterPic2, filterPic3, filterPic4, filterPic5, filterPic6, filterPic7, filterPic8, filterPic9, filterPic10, filterPic11, filterPic12 };
            DoubleBufferedPanel pan = new DoubleBufferedPanel();
            pan.Name = "FilPan";
            pan.AutoScroll = true;
            pan.VerticalScroll.Visible = false;
            pan.MouseDown += pan_MouseDown;
            pan.MouseUp += pan_MouseUp;
            pan.MouseMove += pan_MouseMove;
            pan.HorizontalScroll.Visible = true;
            pan.Size = new Size(850, 500);
            pan.Visible = false;
            pan.BackColor = Color.Transparent;
            Controls.Add(pan);
            pan.Location = new Point(110 + FiltersPrev.Width / 2, 1645);
            pan.Controls.AddRange(filters);
            FiltersPrev.Location = new Point(100 - FiltersPrev.Width / 2, pan.Location.Y + 100 - FiltersPrev.Height / 2);
            FiltersNext.Location = new Point(pan.Location.X + pan.Width + 10, pan.Location.Y + 100 - FiltersPrev.Height / 2);
            for (byte i = 0; i < 12; i++)
            {
                filters[i].Location = new Point(10 + i * 210, 0);
                filters[i].MouseDown += pan_MouseDown;
                filters[i].MouseUp += pan_MouseUp;
                filters[i].MouseMove += pan_MouseMove;
            }
            BG = Image.FromFile("Main.jpg");
            this.BackgroundImage = new Bitmap(BG, new Size(1080,1920));
            InstaSearch.Visible = MkSelfie.Visible = InfoBttn.Visible = true;
            int w, h;
            for (byte i = 0; i < 20; i++)
            {
                w = (i % 5) * 200 + (1 + i % 5) * (this.Width - 1000) / 7;
                h = (i / 5) * 215;
                cart[i].Location = new Point(w, h);
            }
            WaitTimeLeft = 1800;
            WaitModeTmr.Start();
            this.DoubleBuffered = true;
            typeof(Panel).InvokeMember("DoubleBuffered",
            BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
            null, Keyboard, new object[] { true });
            locTimer.Start();
            hl = new HttpListener();
            hl.Start();
            hl.Prefixes.Add(@"http://localhost:5678/");
            hl.BeginGetContext(new AsyncCallback(Received), hl);
            try
            {
                portname = ccnet.findport(sp);
                if (String.IsNullOrWhiteSpace(portname) || portname == "Не найден COM порт ошибка ответа" || portname == "Не найден COM порт ошибка 20")
                {
                    if (!connectToCoin()) throw new Exception(loc["CashErrConnect"]);
                    else return;
                }
            }
            catch (Exception ex)
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(ex.Message, "Ok");
                mes.ShowDialog();
                return;
            }
            bool open = cashr.openport(sp, portname);
            if (!open)
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["CashErrPort"], "Ok");
                mes.ShowDialog();
                return;
            }
            bool start = cashr.start(sp);
            if (start)
            {
                tmr.Start();
            }
            else
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["CashErrStart"], "Ok");
                mes.ShowDialog();
                return;
            }
        }

        void cartImages_MouseDown(object sender, MouseEventArgs e)
        {
            startMousePos = MousePos = MousePosition.Y;
            CursorPos = Cursor.Position.Y;
            cartScroll = true;
        }

        void cartImages_MouseUp(object sender, MouseEventArgs e)
        {
            cartScroll = false;
        }

        void cartImages_MouseMove(object sender, MouseEventArgs e)
        {
            if(cartScroll)
            {
                ((Panel)Controls["cartPanel"]).AutoScrollPosition = new Point(((Panel)Controls["cartPanel"]).AutoScrollPosition.X, -((Panel)Controls["cartPanel"]).AutoScrollPosition.Y + (MousePos - MousePosition.Y));
                MousePos = MousePosition.Y;
                Controls["cartPanel"].Refresh();
            }
        }

        void SearchPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (SearchScroll)
            {
                ((Panel)Controls["SearchPanel"]).AutoScrollPosition = new Point(((Panel)Controls["SearchPanel"]).AutoScrollPosition.X, -((Panel)Controls["SearchPanel"]).AutoScrollPosition.Y + (MousePos - e.Y));
                if (((Control)sender).Name == "SearchPanel") MousePos = e.Y;
                Controls["SearchPanel"].Refresh();
            }
        }

        void SearchPanel_MouseUp(object sender, MouseEventArgs e)
        {
            SearchScroll = false;
        }

        void SearchPanel_MouseDown(object sender, MouseEventArgs e)
        {
            SearchScroll = true;
            MousePos = e.Y;
            CursorPos = Cursor.Position.Y;
        }

        private void SearchButtonClick(object sender, EventArgs e)
        {
            if (AccSearch.Text == "" || AccSearch.Text == loc["EnterQuery"])
            {
                return;
            }
            if (CheckBlacklist(AccSearch.Text))
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["InBlacklistUser"], "Ok");
                mes.ShowDialog();
                return;
            }
            LoadInProgress.Create("");
            try
            {
                nextImages(AccSearch.Text);
            }
            catch (ArgumentOutOfRangeException)
            {
                LoadInProgress.Destroy();
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["NoPhoto"], "Ok");
                mes.ShowDialog();
                return;
            }
            catch (Exception x)
            {
                if (x.Message.Contains("400"))
                {
                    LoadInProgress.Destroy();
                    WaitModeTmr.Stop();
                    log = new Login(loc["Back"], loc["Private"], loc["Authorize"], loc["NotSavingPass"], AccSearch.Text);
                    DialogResult dr = log.ShowDialog();
                    WaitModeTmr.Start();
                    if (dr == DialogResult.Cancel)
                    {
                        return;
                    }
                    LoadInProgress.Create("");
                    try
                    {
                        nextImages(AccSearch.Text);
                    }
                    catch (Exception xx)
                    {
                        LoadInProgress.Destroy();
                        if (mes != null) mes.Close();
                        if (xx.Message.Contains("400"))
                        {
                            mes = new MessageForm(loc["NoConnectionToUser"], "Ok");
                            log.LogOut();
                        }
                        else
                        {
                            mes = new MessageForm(xx.Message, "Ok");
                        }
                        mes.ShowDialog();
                        return;
                    }
                }
                else
                {
                    LoadInProgress.Destroy();
                    if (mes != null) mes.Close();
                    mes = new MessageForm(x.Message, "Ok");
                    mes.ShowDialog();
                    return;
                }
            }
            try
            {
                for (byte j = 0; j < 30; j++)
                    pics[pics.Count - 1][j].Image = new Bitmap(nxtImages[j], pics[pics.Count - 1][j].Size);
            }
            catch
            {
            }
            AccSearch.Text = "";
            AccSearch.Visible = rtnMnPnl.Visible = Keyboard.Visible = false;
            Controls["SearchPanel"].Visible = false;
            Next.Visible = SearchBack.Visible = picsPanel.Visible = true;
            LoadInProgress.Destroy();
            return;
        }

        private void ChangeLang_Click(object sender, EventArgs e)
        {
            lm.Unload(curLocale);
            SetLang(((Button)sender).Text);
            MainScreenLbl.Location = new Point(540 - MainScreenLbl.Width / 2, 430 - MainScreenLbl.Height / 2);
            PrintPhoneLbl.Location = new Point(240 - PrintPhoneLbl.Width / 2, MkSelfie.Location.Y + MkSelfie.Height + 20);
            MkSelfieLbl.Location = new Point(540 - MkSelfieLbl.Width / 2, PrintPhoneLbl.Location.Y);
            CartLbl.Location = new Point(540 - CartLbl.Width / 2, 75);
            VideoFeedbackLbl.Location = new Point(850 - VideoFeedbackLbl.Width / 2, PrintPhoneLbl.Location.Y);
            PrintEven.Location = new Point(540 - PrintEven.Width / 2, PrintEven.Location.Y);
            PrintingLbl.Location = new Point(540 - PrintingLbl.Width / 2, 1200);
            CashPaid.Location = new Point(1050 - CashPaid.Width, CashPaid.Location.Y);
            BalLbl.Location = new Point(CashPaid.Location.X + CashPaid.Width / 2 - BalLbl.Width / 2, BalLbl.Location.Y);
            AcceptedCashLbl.Location = new Point(320 - AcceptedCashLbl.Width, 1630);
            SupportLbl.Location = new Point(640 - SupportLbl.Width, 1630);
            NoCoinsLbl.Location = new Point(960 - NoCoinsLbl.Width, 1630);
            ThanksLbl.Location = new Point(Width / 2 - ThanksLbl.Width / 2, Height / 2 - ThanksLbl.Height / 2);
            AutoMainLbl.Location = new Point(Width / 2 - AutoMainLbl.Width / 2, 1090);
            DoNotForgetLbl.Location = new Point(95, 1300);
            ToEditLabel.Location = new Point(Width / 2 - ToEditLabel.Width / 2, 1200);
            WaitTimeLeft = 1800;
        }

        private void InstaSearch_Click(object sender, EventArgs e)
        {
            timeout = false;
            LoadInProgress.Create("");
            timer1.Start();
            foreach (Control c in Controls) c.Enabled = false;
            if (!PingMonitoring())
            {
                reload();
                if (mes != null) mes.Close();
                mes = new MessageForm(loc[s.ToString()], "Ok");
                mes.ShowDialog();
                return;
            }
            if (paperAmm <= 5)
            {
                LoadInProgress.Destroy();
                if (mes != null) mes.Close();
                if (paperAmm <= 0)
                {
                    reload();
                    mes = new MessageForm(loc["NoPaper"], "Ok");
                    mes.ShowDialog();
                    return;
                }
                mes = new MessageForm(loc["LowPaper",paperAmm*2], "Ok");
                mes.ShowDialog();
                LoadInProgress.Create("");
            }
            LoadInProgress.ChangeLabelText(loc[""]);
            try
            {
                using (var client = new MyWebClient())
                {
                    using (var stream = client.OpenRead("https://instagram.com"))
                    {
                    }
                }
            }
            catch
            {
                reload();
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["ErrInstagramUnav"], "Ok");
                mes.ShowDialog();
                WaitTimeLeft = 1800;
                return;
            }
            fromMain = true;
            AccSearch.Text = loc["EnterQuery"];
            SearchForUsers("like");
            SearchForHashtags("like");
            fromMain = false;
            timer1.Stop(); 
            if (timeout) return;
            MkSelfie.Visible = InstaSearch.Visible = InfoBttn.Visible = Lang1.Visible = Lang2.Visible = false;
            MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = false; 
            BG = Image.FromFile("SearchRes.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            BttnSearchBack.Text = loc["Back"];
            Search.Text = loc["Search"];
            BalLbl.Visible = CashPaid.Visible = true;
            rtnMnPnl.Location = new Point(0, 1430);
            AccSearch.Visible = rtnMnPnl.Visible = Keyboard.Visible = true;
            Controls["SearchPanel"].Visible = true;
            foreach (Control c in Controls) c.Enabled = true;
            LoadInProgress.Destroy();
            swtch = 1;
            WaitTimeLeft = 1800;
        }

        private void MkSelfie_Click(object sender, EventArgs e)
        {
            timeout = false;
            LoadInProgress.Create("");
            timer1.Start();
            foreach (Control c in Controls) c.Enabled = false;
            if (!PingMonitoring())
            {
                reload();
                if (mes != null) mes.Close();
                mes = new MessageForm(loc[s.ToString()], "Ok");
                mes.ShowDialog();
                return;
            }
            if (paperAmm <= 5)
            {
                LoadInProgress.Destroy();
                if (mes != null) mes.Close();
                if (paperAmm <= 0)
                {
                    reload();
                    mes = new MessageForm(loc["NoPaper"], "Ok");
                    mes.ShowDialog();
                    return;
                }
                mes = new MessageForm(loc["LowPaper", paperAmm * 2], "Ok");
                mes.ShowDialog();
                LoadInProgress.Create("");
            }
            timer1.Stop();
            if (timeout) return;
            LoadInProgress.Destroy();
            MkSelfie.Visible = InstaSearch.Visible = InfoBttn.Visible = Lang1.Visible = Lang2.Visible = false;
            MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = false;
            BG = Image.FromFile("SelfieMake.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            TimerChecker.Image = new Bitmap(tmrChecked ? TimerCh : TimerUn, TimerChecker.Size);
            TimerTime.Text = loc["Timer"];
            TimerTime.Location = new Point(TimerChecker.Location.X + 50 - TimerTime.Width/2, TimerChecker.Location.Y + 130);
            RtnMain.Text = loc["ToMainPage"];
            SelfPreview.Visible = RtnMain.Visible = SelfieTake.Visible = TimerChecker.Visible = TimerTime.Visible = true;
            swtch = 2;
            foreach (Control c in Controls) c.Enabled = true;
            WaitTimeLeft = 1800;
        }

        private void SelfPreview_VisibleChanged(object sender, EventArgs e)
        {
            if (SelfPreview.Visible)
                try
                {
                    cam = new Capture(VIDEODEVICE, SelfPreview);
                }
                catch
                {
                    SelfPreview.Visible = false;
                    SelfieTake.Enabled = false;
                    mes = new MessageForm(loc["CamError"], "Ok", 15);
                    mes.ShowDialog();
                }
            else
            {
                try { cam.Dispose(); }
                catch { }
                SelfieTake.Enabled = true;
            }
        }

        private void RtnMain_Click(object sender, EventArgs e)
        {
            switch (swtch)
            {
                case 1:
                    {
                        imgs.Clear();
                        imgsUrls.Clear();
                        nextPageUrl = null;
                        foreach (Panel p in Usernames)
                        {
                            p.Visible = false;
                        }
                        AccSearch.Visible = false;
                        AccSearch.Text = "";
                        RtnMain.Visible = false;
                        rtnMnPnl.Visible = false;
                        BalLbl.Visible = CashPaid.Visible = false;
                        textPrevTag = textPrevUser = "";
                        rtnMnPnl.Visible = false;
                        Keyboard.Visible = false;
                        Controls["SearchPanel"].Visible = false;
                        break;
                    }
                case 2:
                    {
                        RtnMain.Visible = SelfPreview.Visible = SelfieTake.Visible = TimerChecker.Visible = TimerTime.Visible = false;
                        try { cam.Dispose(); }
                        catch { }
                        SelfView.Visible = PhotoOk.Visible = RetakeSelf.Visible = false;
                        break;
                    }
                case 3:
                    {
                        Search.Text = loc["Search"];
                        chosenImages[chngNum].Image = SelfView.Image;
                        SelfView.Visible = RtnMain.Visible = AddText.Visible = TurnLeft.Visible = TurnRight.Visible = false;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            if (((Button)sender).Text == loc["ToMainPage"] || ((Button)sender).Text == loc["Back"])
            {
                BG = Image.FromFile("Main.jpg");
                this.BackgroundImage = new Bitmap(BG, this.Size);
                MkSelfie.Visible = InstaSearch.Visible = InfoBttn.Visible = Lang1.Visible = Lang2.Visible = true;
                MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = true;
            }
            else
            {
                BG = Image.FromFile("Cart.jpg");
                CartOk.Visible = OrderDel.Visible = CartLbl.Visible = true;
                this.BackgroundImage = new Bitmap(BG, this.Size);
                cartRefresh();
            }
            WaitTimeLeft = 1800;
        }

        private void SelfieTake_Click(object sender, EventArgs e)
        {
            if (tmrChecked)
            {
                foreach (Control c in Controls)
                {
                    c.Enabled = false;
                }
                slfTimerCnt = selfTimerSecs;
                TimerTime.Text = slfTimerCnt + "...";
                TimerTime.Location = new Point(TimerChecker.Location.X + 50 - TimerTime.Width / 2, TimerChecker.Location.Y + 130);
                RtnMain.Enabled = false;
                slfyTimer.Start();
            }
            else TakeSelfie();
            WaitTimeLeft = 1800;
        }

        private void RetakeSelf_Click(object sender, EventArgs e)
        {
            SelfView.Visible = PhotoOk.Visible = RetakeSelf.Visible = false;
            BG = Image.FromFile("SelfieMake.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            TimerTime.Text = loc["Timer"];
            TimerTime.Location = new Point(TimerChecker.Location.X + 50 - TimerTime.Width / 2, TimerChecker.Location.Y + 130);
            SelfPreview.Visible = RtnMain.Visible = TimerChecker.Visible = TimerTime.Visible = SelfieTake.Visible = true;
            WaitTimeLeft = 1800;
        }
        
        private void AccSearch_TextChanged(object sender, EventArgs e)
        {
            searchTimer.Stop();
            if (AccSearch.Text == loc["EnterQuery"] || AccSearch.Text.Trim() == "")
            {
                AccSearch.ForeColor = Color.DarkGray;
                AccSearch.SelectionLength = 0;
                AccSearch.SelectionStart = AccSearch.Text.Length + 1;
                foreach (Panel p in Usernames)
                {
                    if (p.Visible) p.Visible = false;
                }
                foreach (Panel p in Tags)
                {
                    if (p.Visible) p.Visible = false;
                }
            }
            else
            {
                AccSearch.ForeColor = Color.Black;
                searchPause = 10;
                searchTimer.Start();
            }
            WaitTimeLeft = 1800;
        }

        private void searchTimer_Tick(object sender, EventArgs e)
        {
            searchPause--;
            if (searchPause == 0)
            {
                byte a = 0;
                if (CheckBlacklist(AccSearch.Text)) return;
                thrU = new Thread(() => SearchForUsers(AccSearch.Text));
                if (!AccSearch.Text.StartsWith("#"))
                {
                    try
                    {
                        thrU.Start();
                    }
                    catch
                    {
                        thrU.Abort();
                    }
                }
                else
                {
                    foreach (PictureBox p in userPics)
                    {
                        Usernames[a].Visible = false;
                        p.Image = null;
                        a++;
                    }
                }
                thrH = new Thread(() => SearchForHashtags(AccSearch.Text));
                if (AccSearch.Text.Length > 1)
                {
                    try
                    {
                        thrH.Start();
                    }
                    catch
                    {
                        thrH.Abort();
                    }
                }
                else
                {
                    a = 0;
                    foreach (Label l in labelsForResults[1])
                    {
                        l.Text = "";
                        labelsForResults[3][a].Text = "";
                        Tags[a].Visible = false;
                        a++;
                    }
                }
            }
        }

        private void PhotoOk_Click(object sender, EventArgs e)
        {
            SelfView.Visible = PhotoOk.Visible = RetakeSelf.Visible = RtnMain.Visible = false;
            SelfView.Location = new Point(0, 3 * this.Height / 16);
            BG = Image.FromFile("Filters.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            ApplyFilters();
            FiltersNext.Visible = FiltersOk.Visible = FiltersPrev.Visible = true;
            SelfView.Visible = true;
            ((Panel)Controls["FilPan"]).AutoScrollPosition = new Point(0, 0);
            Controls["FilPan"].Visible = true;
            WaitTimeLeft = 1800;
        }
 
        private void tmr_Tick(object sender, EventArgs e)
        {
            if (cash < (chosenImages.Count / 2) * photoPrice + chosenImages.Count % 2 * photoPrice || cash == 0) Print.Enabled = false;
            else 
            Print.Enabled = true;
            int c = 0;
            try
            {
                c = ccnet.tolk(sp);
                if (c != 0)
                {
                    cash += c;
                    cashInside += c;
                    CashAmmLbl.Text = CashPaid.Text = cash.ToString() + " " + loc["Rub"];
                    api.SendlogAsync(new LogMessage(InstamatLogType.Other, "Money inserted: " + c + Environment.NewLine + "Total user's cash: " + cash, DateTime.Now));
                }
            }
            catch
            {
                tmr.Dispose();
            }
        }

        private void cartIm_Click(object sender, EventArgs e)
        {
            if (startMousePos >= MousePosition.Y + 20 || startMousePos <= MousePosition.Y - 20) return;
            try
            {
                if (((PictureBox)sender).Name.Split('_')[1] != (chosenImages.Count + 1).ToString() || chosenImages.Count == 20)
                {
                    if (Print.Visible) return;
                    if (((PictureBox)sender).Image == null)
                    {
                        foreach (PictureBox p in cart)
                        {
                            p.Image = null;
                        }
                        ToEditLabel.Visible = false;
                        EditButtons(true);
                        ((PictureBox)sender).Image = new Bitmap(Image.FromFile("Chosen.png"), ((PictureBox)sender).Size);
                    }
                    else
                    {
                        if (!Print.Visible) ToEditLabel.Visible = true;
                        EditButtons(false);
                        ((PictureBox)sender).Image = null;
                    }
                    int.TryParse(((PictureBox)sender).Name.Split('_')[1], out chngNum);
                    chngNum--;
                    return;
                }
                else
                {
                    ToEditLabel.Visible = false;
                    chngNum = -1;
                    ShowAddFrom();
                }
            }
            catch
            {
                ToEditLabel.Visible = false;
                chngNum = -1;
                ShowAddFrom();
            }
        }

        private void CartOk_Click(object sender, EventArgs e)
        {
            if (chosenImages.Count == 0) return;
            CartOk.Visible = OrderDel.Visible = false;
            CartLbl.Visible = false;
            if (Controls.IndexOfKey("PhotoEdit") != -1) EditButtons(false);
            else ToEditLabel.Visible = false;
            BG = Image.FromFile("Order.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            Print.Visible = PrintBack.Visible = true;
            cntr = 0;
            PhotoAmmLbl.Text = loc["Photo", chosenImages.Count];
            toPay = (chosenImages.Count % 2 == 0) ? chosenImages.Count * (int)photoPrice / 2 : (chosenImages.Count + 1) * (int)photoPrice / 2;
            CashAmmLbl.Text = cash.ToString() + " " + loc["Rub"];
            CashNeeded.Text = toPay.ToString() + " " + loc["Rub"];
            PhotoAmmLbl.Visible = CashNeeded.Visible = CashAmmLbl.Visible = true;
            OrderedLbl.Visible = PaidCashLbl.Visible = NeedCashLbl.Visible = true;
            AcceptedCashLbl.Visible = SupportLbl.Visible = NoCoinsLbl.Visible = true;
            if (chosenImages.Count % 2 != 0) PrintEven.Visible = true;
            if (cash < (chosenImages.Count / 2) * 50 + chosenImages.Count % 2 * 50 || cash == 0) Print.Enabled = false;
            else Print.Enabled = true;
            Controls["cartPanel"].Location = new Point(Controls["cartPanel"].Location.X, Controls["cartPanel"].Location.Y + 180);
            for (byte i = 0; i < 20; i++)
            {
                cart[i].Image = null;
            }
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            float PW = e.PageBounds.Width;
            float sw = PW/2 - 30;
            float PH = e.PageBounds.Height;
            float sh = PW/2 - 30;
            for (; cntr < chosenImages.Count; cntr++)
            {
                try
                {
                    Bitmap Bmp = new Bitmap(chosenImages[cntr].Image);
                    if (Bmp.Width / sw < Bmp.Height / sh)
                        sw = Bmp.Width * sh / Bmp.Height;
                    else
                        sh = Bmp.Height * sw / Bmp.Width;
                    e.Graphics.DrawImage(Bmp, 15, 15, sh, sw);
                    e.Graphics.DrawString(chosenImages[cntr].Text, new Font(InputBox.Font.FontFamily, 8.0f), Brushes.Black, new RectangleF(15, 25 + sh, sh, PW - sw - 15));
                    cntr++;
                    if (cntr < chosenImages.Count) Bmp = new Bitmap(chosenImages[cntr].Image);
                    sw = PW/2 - 30;
                    sh = PW/2 - 30;
                    if (Bmp.Width / sw < Bmp.Height / sh)
                        sw = Bmp.Width * sh / Bmp.Height;
                    else
                        sh = Bmp.Height * sw / Bmp.Width;
                    e.Graphics.DrawImage(Bmp, PW/2 + 15, 15, sh, sw);
                    e.Graphics.DrawString(chosenImages[cntr==chosenImages.Count?cntr-1:cntr].Text, new Font(InputBox.Font.FontFamily, 8.0f), Brushes.Black, new RectangleF(PW/2 + 15, 25 + sh, sh, PW - sw - 15));
                    break;
                }
                catch
                {
                }
            }
            cntr++;
            //if (cntr < chosenImages.Count)
            //{
            //    e.HasMorePages = true;
            //}
            //else
            //{
            //    return;
            //}
        }

        private void Print_Click(object sender, EventArgs e)
        {
            if (cash > (chosenImages.Count / 2) * photoPrice + chosenImages.Count % 2 * photoPrice)
            {
                mes = new MessageForm(loc["MoneyLeftCaut"], loc["Yes"], loc["No"]);
                DialogResult r = mes.ShowDialog();
                if (r == DialogResult.OK)
                {
                    Print.Visible = PrintBack.Visible = false;
                    PhotoAmmLbl.Visible = CashAmmLbl.Visible = CashNeeded.Visible = false;
                    PaidCashLbl.Visible = NeedCashLbl.Visible = OrderedLbl.Visible = false;
                    AcceptedCashLbl.Visible = SupportLbl.Visible = NoCoinsLbl.Visible = false;
                    PrintEven.Visible = false;
                    BG = Image.FromFile("Cart.jpg");
                    this.BackgroundImage = new Bitmap(BG, this.Size);
                    CartOk.Visible = OrderDel.Visible = CartLbl.Visible = true;
                    ToEditLabel.Visible = true;
                    byte i;
                    Controls["cartPanel"].Location = new Point(Controls["cartPanel"].Location.X, Controls["cartPanel"].Location.Y + 180);
                    for (i = 0; i < 20; i++)
                    {
                        if (i == chosenImages.Count)
                        {
                            cart[i++].Visible = true;
                            break;
                        }
                    }
                    cartRefresh();
                    WaitTimeLeft = 1800;
                    return;
                }
            }
            Print.Visible = PrintBack.Visible = PrintEven.Visible = false;
            PaidCashLbl.Visible = NeedCashLbl.Visible = OrderedLbl.Visible = false;
            AcceptedCashLbl.Visible = SupportLbl.Visible = NoCoinsLbl.Visible = false;
            int printed = chosenImages.Count / 2 + chosenImages.Count % 2;
            Controls["cartPanel"].Location = new Point(Controls["cartPanel"].Location.X, Controls["cartPanel"].Location.Y + 180);
            for (int i = 0; i < 20; i++)
            {
                if (!cart[i].Visible) break;
                cart[i].Visible = false;
                cart[i].Controls.Clear();
            }
            PhotoAmmLbl.Visible = CashAmmLbl.Visible = CashNeeded.Visible = false;
            BG = Image.FromFile("Printing.jpg");
            PrintPageLbl.Text = loc["Printed", 0, printed];
            PrintPageLbl.Location = new Point(540 - PrintPageLbl.Width / 2, 1700);
            Pr = new Thread(PrintStarted);
            paperAmm -= printed;
            PurchaseDone(printed);
            Pr.Start();
            this.BackgroundImage = new Bitmap(BG, this.Size);
            WaitTimeLeft = 1800;
        }

        private void PrintBack_Click(object sender, EventArgs e)
        {
            Print.Visible = PrintBack.Visible = false;
            PhotoAmmLbl.Visible = CashAmmLbl.Visible = CashNeeded.Visible = false;
            PaidCashLbl.Visible = NeedCashLbl.Visible = OrderedLbl.Visible = false;
            AcceptedCashLbl.Visible = SupportLbl.Visible = NoCoinsLbl.Visible = false;
            PrintEven.Visible = false;
            BG = Image.FromFile("Cart.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            CartOk.Visible = OrderDel.Visible = CartLbl.Visible = true;
            ToEditLabel.Visible = true;
            byte i;
            Controls["cartPanel"].Location = new Point(Controls["cartPanel"].Location.X, Controls["cartPanel"].Location.Y - 180);
            for (i = 0; i < 20; i++)
            {
                if (i == chosenImages.Count)
                {
                    cart[i++].Visible = true;
                    break;
                }
            }
            cartRefresh();
            WaitTimeLeft = 1800;
        }

        private void RstBttn_Click(object sender, EventArgs e)
        {
            reset();
            WaitTimeLeft = 1800;
        }

        private void Next_Click(object sender, EventArgs e)
        {
            if (imageNumbers.Count > 0)
            {
                LoadInProgress.Create("");
                byte i = 0;
                doneEvents = new ManualResetEvent[imageNumbers.Count];
                foreach (int[] name in imageNumbers)
                {
                    InstaSearch f;
                    doneEvents[i] = new ManualResetEvent(false);
                    f = new InstaSearch(doneEvents[i], i, imgsUrls[name[0]][name[1]]);
                    ThreadPool.QueueUserWorkItem(f.ThreadPoolCallback, i);
                    i++;
                }
                foreach (ManualResetEvent ev in doneEvents)
                    if (ev != null) ev.WaitOne();
                for (byte w = 0; w < imageNumbers.Count; w++) chosenImages.Add(new ImageWithText(nxtImages[w], imgs[imageNumbers[w][0]][imageNumbers[w][1]].Text));
                picsPanel.Visible = false;
                foreach (PictureBox[] ps in pics)
                {
                    i = 0;
                    foreach (PictureBox p in ps)
                    {
                        ps[i] = null;
                        i++;
                    }
                }
                SearchBack.Visible = Prev.Visible = Next.Visible = false;
                picsSelected.Visible = false;
                BalLbl.Visible = CashPaid.Visible = false;
                loadingPic.Visible = false;
                picsPanel.Controls.Clear();
                pics.Clear();
                imageNumbers.Clear();
                imgs.Clear();
                imgsUrls.Clear();
                int r, h;
                for (i = 0; i < 20; i++)
                {
                    r = (i % 5) * 200 + (1 + i % 5) * (this.Width - 1000) / 7;
                    h = (i / 5) * 215;
                    cart[i].Location = new Point(r, h);
                }
                BG = Image.FromFile("Cart.jpg");
                this.BackgroundImage = new Bitmap(BG, this.Size);
                cartRefresh();
                foreach (Panel u in Usernames) u.Visible = false;
                CartOk.Visible = OrderDel.Visible = true;
                CartLbl.Visible = true;
                ToEditLabel.Visible = true;
                LoadInProgress.Destroy();
                WaitTimeLeft = 1800;
            }
        }
            
        private void SearchBack_Click(object sender, EventArgs e)
        {
            AccSearch.Text = loc["EnterQuery"];
            imgs.Clear();
            imageNumbers.Clear();
            imgsUrls.Clear();
            nextPageUrl = null;
            picsPanel.Controls.Clear();
            pics.Clear();
            foreach (Panel p in Usernames) p.Visible = false;
            picsSelected.Visible = false;
            Next.Visible = Prev.Visible = SearchBack.Visible = picsPanel.Visible = false;
            rtnMnPnl.Visible = AccSearch.Visible = Keyboard.Visible = true;
            Controls["SearchPanel"].Visible = true;
            WaitTimeLeft = 1800;
        }

        private void Button_EnabledChanged(object sender, EventArgs e)
        {
            if (((Button)sender).Enabled) ((Button)sender).BackgroundImage = new Bitmap(bttnEnabledBG,((Button)sender).Size);
            else ((Button)sender).BackgroundImage = new Bitmap(bttnDisabledBG, ((Button)sender).Size);
        }

        private void FiltersOk_Click(object sender, EventArgs e)
        {
            FiltersNext.Visible = FiltersBack.Visible = FiltersOk.Visible = FiltersPrev.Visible = false;
            SelfView.Visible = false;
            Controls["FilPan"].Visible = false;
            BG = Image.FromFile("Cart.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            if (chngNum != -1)
            {
                chosenImages[chngNum].Image = (Image)SelfView.Image.Clone();
            }
            else
            {
                chosenImages.Add(new ImageWithText((Image)SelfView.Image.Clone(), ""));
            }
            if (swtch == 1)
            {
                AccSearch.Text = "";
                imgs.Clear();
                imgsUrls.Clear();
                nextPageUrl = null;
            }
            swtch = 3;
            cartRefresh();
            CartOk.Visible = OrderDel.Visible = CartLbl.Visible = true;
            ToEditLabel.Visible = true;
            WaitTimeLeft = 1800;
        }

        private void FiltersNext_Click(object sender, EventArgs e)
        {
            WaitTimeLeft = 1800;
        }
        
        private void FiltersPrev_Click(object sender, EventArgs e)
        {
            WaitTimeLeft = 1800;
        }

        private void filterPic_Click(object sender, EventArgs e)
        {
            if (Cursor.Position.X <= CursorPos + 20 && Cursor.Position.X >= CursorPos - 20)
            {
                LoadInProgress.Create("");
                switch (((PictureBox)sender).Name)
                {
                    case "filterPic1":
                        {
                            if (imToProcess != null) SelfView.Image = new Bitmap((Image)imToProcess.Clone(), SelfView.Size);
                            break;
                        }
                    case "filterPic2":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.monochrome(imForFilters);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic3":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.sepia(imForFilters);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic4":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.contrast(imForFilters, 100);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic5":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.brighten(imForFilters, 50);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic6":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.noise(imForFilters, 30);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic7":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.brighten(imForFilters, 10);
                            imForFilters = ef.saturate(imForFilters, 30);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic8":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.saturate(imForFilters, 50);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic9":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.brighten(imForFilters, -30);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic10":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.invert(imForFilters);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic11":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.contrast(imForFilters, 30);
                            imForFilters = ef.posterize(imForFilters, 4);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    case "filterPic12":
                        {
                            imForFilters = new BitmapW((Bitmap)imToProcess.Clone());
                            imForFilters = ef.saturate(imForFilters, 25);
                            imForFilters = ef.brighten(imForFilters, -20);
                            SelfView.Image = new Bitmap(imForFilters.GetBitmap(), SelfView.Size);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
                LoadInProgress.Destroy();
            }
            WaitTimeLeft = 1800;
        }

        private void InfoBttn_Click(object sender, EventArgs e)
        {
            InstaSearch.Visible = InfoBttn.Visible = MkSelfie.Visible = Lang1.Visible = Lang2.Visible = false;
            MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = false;
            BG = Image.FromFile("Info.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            InfoClose.Visible = true;
            WaitTimeLeft = 1800;
        }

        private void InfoClose_Click(object sender, EventArgs e)
        {
            InfoClose.Visible = false;
            BG = Image.FromFile("Main.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            InstaSearch.Visible = InfoBttn.Visible = MkSelfie.Visible = Lang1.Visible = Lang2.Visible = true;
            MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = true;
            WaitTimeLeft = 1800;
        }

        private void Key_Click(object sender, EventArgs e)
        {
            int i;
            if (AccSearch.Visible) i = AccSearch.SelectionStart;
            else i = InputBox.SelectionStart;
            if (((Button)sender).Name.Length < 4)
            {
                if(AccSearch.Visible)
                {
                    if (AccSearch.SelectionLength >= 0) AccSearch.Text = AccSearch.Text.Remove(i, AccSearch.SelectionLength);
                    AccSearch.Text = AccSearch.Text == loc["EnterQuery"]?((Button)sender).Text:AccSearch.Text.Insert(i, ((Button)sender).Text);
                    AccSearch.SelectionStart = i + 1;
                    AccSearch.SelectionLength = 0;
                }
                else
                {
                    InputBox.Text = InputBox.Text.Insert(i, ((Button)sender).Text);
                    InputBox.SelectionStart = i + 1;
                    InputBox.SelectionLength = 0;
                }
            }
            else
                switch (((Button)sender).Name)
                {
                    case "bcsp":
                        {
                            if (AccSearch.Visible)
                            {
                                if (AccSearch.Text != "")
                                {
                                    if (i > 0)
                                    {
                                        if (AccSearch.Text == loc["EnterQuery"])
                                        {
                                            AccSearch.Text = "";
                                            return;
                                        }
                                        if (AccSearch.SelectionLength > 0)
                                        {
                                            AccSearch.Text = AccSearch.Text.Remove(i, AccSearch.SelectionLength);
                                            AccSearch.SelectionStart = i;
                                        }
                                        else
                                        {
                                            AccSearch.Text = AccSearch.Text.Remove(i - 1, 1);
                                            AccSearch.SelectionStart = (i - 1) <= 0 ? 0 : (i - 1);
                                        }
                                    }
                                }
                            }
                            else
                                if (InputBox.Text != "")
                                {
                                    if (i > 0)
                                    {
                                        if (InputBox.SelectionLength > 0)
                                        {
                                            InputBox.Text = InputBox.Text.Remove(i, InputBox.SelectionLength);
                                            InputBox.SelectionStart = i;
                                        }
                                        else
                                        {
                                            InputBox.Text = InputBox.Text.Remove(i - 1, 1);
                                            InputBox.SelectionStart = (i - 1) <= 0 ? 0 : (i - 1);
                                        }
                                    }
                                }
                            break;
                        }
                    case "Search":
                        {
                            if (AccSearch.Visible)
                            {
                                if (AccSearch.Text == "" || AccSearch.Text == loc["EnterQuery"])
                                {
                                    return;
                                }
                                if (AccSearch.Text == "SecretCode")
                                {
                                    ShowAdminPanel();
                                    return;
                                }
                                if (CheckBlacklist(AccSearch.Text))
                                {
                                    if (mes != null) mes.Close();
                                    mes = new MessageForm(loc["InBlacklistUser"], "Ok");
                                    mes.ShowDialog();
                                    return;
                                } 
                                LoadInProgress.Create("");
                                try
                                {
                                    nextImages(AccSearch.Text);
                                }
                                catch (ArgumentOutOfRangeException)
                                {
                                    LoadInProgress.Destroy();
                                    if (mes != null) mes.Close();
                                    mes = new MessageForm(loc["NoPhoto"], "Ok");
                                    mes.ShowDialog();
                                    return;
                                }
                                catch (Exception x)
                                {
                                    if (x.Message.Contains("400"))
                                    {
                                        LoadInProgress.Destroy();
                                        WaitModeTmr.Stop();
                                        log = new Login(loc["Back"], loc["Private"], loc["Authorize"], loc["NotSavingPass"], AccSearch.Text);
                                        DialogResult dr = log.ShowDialog();
                                        WaitModeTmr.Start();
                                        if (dr == DialogResult.Cancel)
                                        {
                                            return;
                                        }
                                        LoadInProgress.Create("");
                                        try
                                        {
                                            nextImages(AccSearch.Text);
                                        }
                                        catch (Exception xx)
                                        {
                                            LoadInProgress.Destroy();
                                            if (mes != null) mes.Close();
                                            if (xx.Message.Contains("400"))
                                            {
                                                mes = new MessageForm(loc["NoConnectionToUser"], "Ok");
                                                log.LogOut();
                                            }
                                            else
                                            {
                                                mes = new MessageForm(xx.Message, "Ok");
                                            }
                                            mes.ShowDialog();
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        LoadInProgress.Destroy();
                                        if (mes != null) mes.Close();
                                        mes = new MessageForm(x.Message, "Ok");
                                        mes.ShowDialog();
                                        return;
                                    }
                                }
                                try
                                {
                                    for (byte j = 0; j < 30; j++)
                                        pics[pics.Count - 1][j].Image = new Bitmap(nxtImages[j], pics[pics.Count - 1][j].Size);
                                }
                                catch
                                {
                                }
                                AccSearch.Text = "";
                                AccSearch.Visible = rtnMnPnl.Visible = Keyboard.Visible = false;
                                Controls["SearchPanel"].Visible = false;
                                Next.Visible = SearchBack.Visible = picsPanel.Visible = true;
                                LoadInProgress.Destroy();
                                return;
                            }
                            else
                            {
                                InputBox.Text = InputBox.Text.Insert(i, Environment.NewLine);
                                InputBox.SelectionStart = i + 1;
                                InputBox.SelectionLength = 0;
                                break;
                            }
                        }
                    case "rShift":
                    case "lShift":
                        {
                            shiftPressed = !shiftPressed;
                            byte j = 0;
                            foreach (Button[] bts in KeyboardButtons)
                            {
                                j++;
                                if (j != 4)
                                    foreach (Button bt in bts)
                                    {
                                        if (bt.Name.Length <= 3 && bt.Text != "#" && bt.Text != "." && bt.Text != "!" && bt.Text != "_")
                                        {
                                            if (shiftPressed) bt.Text = bt.Text.ToUpper();
                                            else bt.Text = bt.Text.ToLower();
                                        }
                                        else
                                        {
                                            if (bt.Text == "#") bt.Text = "_";
                                            else if (bt.Text == ".") bt.Text = "!";
                                            else if (bt.Text == "!") bt.Text = ".";
                                            else if (bt.Text == "_") bt.Text = "#";
                                        }
                                    }
                                else break;
                            }
                            break;
                        }
                    case "Space":
                        {
                            if (AccSearch.Visible)
                            {
                                if (AccSearch.SelectionLength >= 0) AccSearch.Text = AccSearch.Text.Remove(i, AccSearch.SelectionLength);
                                AccSearch.Text = AccSearch.Text == loc["EnterQuery"] ? " " : AccSearch.Text.Insert(i, " ");
                                AccSearch.SelectionStart = i + 1;
                                AccSearch.SelectionLength = 0;
                            }
                            else
                            {
                                InputBox.Text = InputBox.Text.Insert(i, " ");
                                InputBox.SelectionStart = i + 1;
                                InputBox.SelectionLength = 0;
                            }
                            break;
                        }
                    case "HdKey":
                        {
                            Keyboard.Visible = false;
                            rtnMnPnl.Location = new Point(0, 1800);
                            if (AccSearch.Visible) RtnMain.Focus();
                            else TurnLeft.Focus();
                            return;
                        }
                    case "lSymb":
                        {
                            ApplyAlphabet(lSymb.Text);
                            break;
                        }
                    case "rSymb":
                        {
                            ApplyAlphabet("Symb");
                            break;
                        }
                    case "langChg":
                        {
                            ApplyAlphabet(langChg.Text);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            if (AccSearch.Visible) AccSearch.Focus();
            else InputBox.Focus();
            WaitTimeLeft = 1800;
        }

        private void AccSearch_Enter(object sender, EventArgs e)
        {
            if (!Keyboard.Visible)
            {
                Keyboard.Visible = true;
                if (AccSearch.Visible) if (AccSearch.Text == loc["EnterQuery"]) AccSearch.Text = "";
                rtnMnPnl.Location = new Point(0, 1430);
            }
            WaitTimeLeft = 1800;
        }

        private void tmer_Tick(object sender, EventArgs e)
        {
            AutoMainLbl.Text = loc["AutoToMain", --timePast];
            if (timePast == 0) reset();
        }

        private void slfyTimer_Tick(object sender, EventArgs e)
        {
            slfTimerCnt--;
            TimerTime.Text = slfTimerCnt + "...";
            TimerTime.Location = new Point(TimerChecker.Location.X + 50 - TimerTime.Width / 2, TimerChecker.Location.Y + 130);
            if (slfTimerCnt == 0)
            {
                TakeSelfie();
                slfyTimer.Stop();
                foreach (Control c in Controls)
                {
                    c.Enabled = true;
                }
            }
        }

        private void TimerChecker_Click(object sender, EventArgs e)
        {
            tmrChecked = !tmrChecked;
            if (tmrChecked) TimerChecker.Image = new Bitmap(TimerCh,TimerChecker.Size);
            else TimerChecker.Image = new Bitmap(TimerUn, TimerChecker.Size);
            WaitTimeLeft = 1800;
        }

        private void User_Click(object sender, EventArgs e)
        {
            if(Cursor.Position.Y >= CursorPos + 20 || Cursor.Position.Y <= CursorPos - 20)
            {
                return;
            }
            string text = "";
            string fullname = "";
            switch (((Control)sender).Name.StartsWith("Tag") || ((Control)sender).Name.StartsWith("GetTag"))
            {
                case true:
                    {
                        try
                        {
                            text = labelsForResults[1][int.Parse(((Control)sender).Name.Split('_')[1]) - 1].Text;
                        }
                        catch
                        {
//                            text = labelsForResults[1][int.Parse(((Control)sender).Name.Substring(((Control)sender).Name.Length - 2)) - 1].Text;
                        }
                        if (CheckBlacklist(text)) 
                        {
                            if (mes != null) mes.Close();
                            mes = new MessageForm(loc["InBlacklistTag"], "Ok");
                            mes.ShowDialog();
                            return;
                        }
                        break;
                    }
                case false:
                    {
                        try
                        {
                            text = labelsForResults[0][int.Parse(((Control)sender).Name.Split('_')[1]) - 1].Text;
                            fullname = labelsForResults[2][int.Parse(((Control)sender).Name.Split('_')[1]) - 1].Text;
                        }
                        catch
                        {
//                            text = labelsForResults[0][int.Parse(((Control)sender).Name.Substring(((Control)sender).Name.Length - 2)) - 1].Text;
//                            fullname = labelsForResults[2][int.Parse(((Control)sender).Name.Substring(((Control)sender).Name.Length - 2)) - 1].Text;
                        }
                        if (CheckBlacklist(text) || CheckBlacklist(fullname))
                        {
                            if (mes != null) mes.Close();
                            mes = new MessageForm(loc["InBlacklistUser"], "Ok");
                            mes.ShowDialog();
                            return;
                        }
                        break;
                    }
            }
            try
            {
                LoadInProgress.Create("");
                nextImages(text);
            }
            catch (ArgumentOutOfRangeException)
            {
                LoadInProgress.Destroy();
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["NoPhoto"], "Ok");
                mes.ShowDialog();
                return;
            }
            catch (Exception x)
            {
                if (x.Message.Contains("400"))
                {
                    LoadInProgress.Destroy();
                    WaitModeTmr.Stop();
                    log = new Login(loc["Back"], loc["Private"], loc["Authorize"], loc["NotSavingPass"], text);
                    DialogResult dr = log.ShowDialog();
                    WaitModeTmr.Start();
                    if (dr == DialogResult.Cancel)
                    {
                        return;
                    }
                    LoadInProgress.Create("");
                    try
                    {
                        nextImages(text);
                    }
                    catch (Exception ex)
                    {
                        LoadInProgress.Destroy();
                        if (mes != null) mes.Close(); 
                        if (ex.Message.Contains("400"))
                        {
                            mes = new MessageForm(loc["NoConnectionToUser"], "Ok");
                            log.LogOut();
                        }
                        else
                        {
                            mes = new MessageForm(x.Message, "Ok");
                        }
                        mes.ShowDialog();
                        return;
                    }
                }
                else
                {
                    LoadInProgress.Destroy();
                    if (mes != null) mes.Close();
                    mes = new MessageForm(x.Message, "Ok");
                    mes.ShowDialog();
                    return;
                }
            }
            AccSearch.Text = "";
            for (byte i = 0; i < 30; i++)
                try
                {
                    pics[pics.Count - 1][i].Image = new Bitmap(nxtImages[i], pics[pics.Count - 1][i].Size);
                }
                catch
                {
                }
            AccSearch.Visible = rtnMnPnl.Visible = Keyboard.Visible = false;
            picsSelected.Text = loc["SelectPhotos"];
            picsSelected.Location = new Point(this.Width / 2 - picsSelected.Width/2, 1600);
            Controls["SearchPanel"].Visible = false;
            picsSelected.Visible = true;
            Next.Visible = SearchBack.Visible = picsPanel.Visible = true;
            textPrevTag = textPrevUser = "";
            LoadInProgress.Destroy();
            WaitTimeLeft = 1800;
        }

        private void PhotoDelete_Click(object sender, EventArgs e)
        {
            chosenImages.RemoveAt(chngNum);
            cartRefresh();
            WaitTimeLeft = 1800;
        }

        private void PhotoRed_Click(object sender, EventArgs e)
        {
            EditButtons(false); 
            RtnMain.Visible = false;
            fromCart = CartOk.Visible;
            swtch = 3;
            SelfView.Location = new Point(120, 200);
            SelfView.Size = new Size(840, 840);
            InputBox.Location = new Point(120, 1050);
            InputBox.Size = new Size(840, 210);
            InputBox.Font = new Font(InputBox.Font.FontFamily, 20.0f);
            label1.Font = label2.Font = InputBox.Font;
            label1.MaximumSize = new Size(InputBox.Width, 0);
            Search.Text = "Enter";
            if(fromCart)OrderDel.Visible = CartOk.Visible = CartLbl.Visible = false;
            else Print.Visible = PrintBack.Visible = CashNeeded.Visible = CashPaid.Visible = PhotoAmmLbl.Visible = false;
            foreach (PictureBox p in cart)
            {
                p.Visible = false;
            }
            Controls["cartPanel"].Visible = false;
            BG = Image.FromFile("GrayedBG.jpg");
            PictureBox pc = new PictureBox();
            pc.Name = "LeftArr";
            pc.BackColor = Color.Transparent;
            pc.Size = new Size(80, 1550);
            pc.Image = new Bitmap(ArrLeft, pc.Size);
            pc.Location = new Point(0, 0);
            Controls.Add(pc);
            pc = new PictureBox();
            pc.Name = "RightArr";
            pc.BackColor = Color.Transparent; 
            pc.Size = new Size(80, 1550);
            pc.Image = new Bitmap(ArrRight, pc.Size);
            pc.Location = new Point(1000, 0);
            Controls.Add(pc);
            this.BackgroundImage = new Bitmap(BG, this.Size);
            TurnLeft.Visible = TurnRight.Visible = InputBox.Visible = true;
            EditOk.Visible = EditCancel.Visible = true;
            Original = chosenImages[chngNum];
            SelfView.Image = new Bitmap(chosenImages[chngNum].Image, SelfView.Size);
            EditCancel.Location = new Point(SelfView.Location.X - 20, SelfView.Location.Y - EditCancel.Height - 60);
            TurnLeft.Location = new Point(this.Width/2 - TurnLeft.Width - 100, 1340);
            TurnRight.Location = new Point(this.Width / 2 + 100, 1340);
            EditOk.Location = new Point((SelfView.Location.X + SelfView.Width + 20) - EditOk.Width, SelfView.Location.Y - EditCancel.Height - 60);
            FiltersPrev.Visible = FiltersNext.Visible = true;
            imToProcess = SelfView.Image;
            ApplyFilters();
            ((Panel)Controls["FilPan"]).AutoScrollPosition = new Point(0, 0);
            Controls["FilPan"].Visible = true;
            InputBox.Text = chosenImages[chngNum].Text;
            SelfView.Visible = true;
            WaitTimeLeft = 1800;
        }

        private void PhotoCopy_Click(object sender, EventArgs e)
        {
            chosenImages.Add(chosenImages[chngNum]);
            cartRefresh();
            WaitTimeLeft = 1800;
        }

        private void PhotoChange_Click(object sender, EventArgs e)
        {
            fromCart = CartOk.Visible;
            ShowAddFrom();
            WaitTimeLeft = 1800;
        }

        private void FromInstag_Click(object sender, EventArgs e)
        {
            LoadInProgress.Create("");
            try
            {
                using (var client = new MyWebClient())
                {
                    using (var stream = client.OpenRead("https://instagram.com"))
                    {
                    }
                }
            }
            catch
            {
                LoadInProgress.Destroy();
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["ErrInstagramUnav"], "Ok");
                mes.ShowDialog(); 
                return;
            }
            Controls.RemoveByKey("FromInstag");
            Controls.RemoveByKey("AddSelfie");
            Controls.RemoveByKey("CancelBttn");
            BG = Image.FromFile("SearchRes.jpg"); 
            int w, h;
            for (byte i = 0; i < 20; i++)
            {
                w = (i % 5) * 200 + (1 + i % 5) * (this.Width - 1000) / 7;
                h = (i / 5) * 215;
                cart[i].Location = new Point(w, h);
            } 
            this.BackgroundImage = new Bitmap(BG, this.Size);
            BttnSearchBack.Text = loc["ToCart"];
            rtnMnPnl.Location = new Point(0,1430);
            Controls["SearchPanel"].Visible = true;
            rtnMnPnl.Visible = AccSearch.Visible = rtnMnPnl.Visible = Keyboard.Visible = true;
            BalLbl.Visible = CashPaid.Visible = true;
            AccSearch.Focus();
            LoadInProgress.Destroy();
            swtch = 1;
            WaitTimeLeft = 1800;
        }

        private void AddSelfie_Click(object sender, EventArgs e)
        {
            Controls.RemoveByKey("FromInstag");
            Controls.RemoveByKey("AddSelfie");
            Controls.RemoveByKey("CancelBttn");
            BG = Image.FromFile("SelfieMake.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            TimerChecker.Image = new Bitmap(tmrChecked ? TimerCh : TimerUn, TimerChecker.Size);
            TimerTime.Text = loc["Timer"];
            RtnMain.Text = loc["ToCart"];
            TimerTime.Location = new Point(TimerChecker.Location.X + 50 - TimerTime.Width / 2, TimerChecker.Location.Y + 130);
            int w, h;
            for (byte i = 0; i < 20; i++)
            {
                w = (i % 5) * 200 + (1 + i % 5) * 10;
                h = (i / 5) * 215;
                cart[i].Location = new Point(w, h);
            }
            FiltersPrev.Location = new Point(100 - FiltersPrev.Width / 2, Controls["FilPan"].Location.Y + 100 - FiltersPrev.Height / 2);
            FiltersNext.Location = new Point(Controls["FilPan"].Location.X + Controls["FilPan"].Width + 10, Controls["FilPan"].Location.Y + 100 - FiltersPrev.Height / 2);
            SelfPreview.Visible = RtnMain.Visible = SelfieTake.Visible = TimerChecker.Visible = TimerTime.Visible = true;
            swtch = 2;
            WaitTimeLeft = 1800;
        }

        private void CancelBttn_Click(object sender, EventArgs e)
        {
            cartRefresh();
            Controls.RemoveByKey("FromInstag");
            Controls.RemoveByKey("AddSelfie");
            Controls.RemoveByKey("CancelBttn");
            if (fromCart)
            {
                CartOk.Visible = OrderDel.Visible = CartLbl.Visible = true;
                ToEditLabel.Visible = true;
                this.BackgroundImage = new Bitmap(Image.FromFile("Cart.jpg"), this.Size);
            }
            else
            {
                Print.Visible = PrintBack.Visible = CashAmmLbl.Visible = CashNeeded.Visible = PhotoAmmLbl.Visible = NoCoinsLbl.Visible = SupportLbl.Visible = AcceptedCashLbl.Visible = PaidCashLbl.Visible = NeedCashLbl.Visible = OrderedLbl.Visible = true;
                if (chosenImages.Count % 2 == 1) PrintEven.Visible = true;
                this.BackgroundImage = new Bitmap(Image.FromFile("Order.jpg"), this.Size);
            }
            WaitTimeLeft = 1800;
        }

        private void OrderDel_Click(object sender, EventArgs e)
        {
            if (cash != 0)
            {
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["MoneyLeft"], "Ok", loc["Cancel"]);
                DialogResult dr = mes.ShowDialog();
                if (dr == DialogResult.Cancel) return;
            }
            reset();
            WaitTimeLeft = 1800;
        }

        private void TurnLeft_Click(object sender, EventArgs e)
        {
            SelfView.Image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            imToProcess = SelfView.Image;
            ApplyFilters();
            Refresh();
            WaitTimeLeft = 1800;
            if (Keyboard.Visible)
            {
                Keyboard.Visible = false;
                SelfView.Focus();
            }
        }

        private void TurnRight_Click(object sender, EventArgs e)
        {
            SelfView.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            imToProcess = SelfView.Image;
            ApplyFilters(); 
            Refresh();
            WaitTimeLeft = 1800;
            if (Keyboard.Visible)
            {
                Keyboard.Visible = false;
                SelfView.Focus();
            }
        }

        private void AddText_Click(object sender, EventArgs e)
        {
            InputBox.Visible = Keyboard.Visible = TextOk.Visible = true;
            AddText.Visible = false;
            WaitTimeLeft = 1800;
        }

        private void TextOk_Click(object sender, EventArgs e)
        {
            TextOk.Visible = Keyboard.Visible = InputBox.Visible = false;
            AddText.Visible = true;
            WaitTimeLeft = 1800;
        }

        private void printTimer_Tick(object sender, EventArgs e)
        {
            timeToPrint--;
            PrintProgressBar.Value++;
            if (timeToPrint % printTime == 0 && timeToPrint != 0)
            {
                pd.Print();
                PrintPageLbl.Text = loc["Printed", PrintProgressBar.Value / printTime, PrintProgressBar.Maximum / printTime];
            }
            if (timeToPrint == 0)
            {
                printTimer.Stop();
                PrintEnded();
            }
        }

        private void Key_MouseUp(object sender, MouseEventArgs e)
        {
            bttnName = ((Button)sender).Name;
            buttonTimer.Stop();
            if (bttnName.Length < 4)
            {
                ((Button)sender).BackgroundImage = new Bitmap(KeyBg, ((Button)sender).Size);
            }
            else switch (bttnName)
                {
                    case "bcsp":
                        {
                            bcsp.BackgroundImage = new Bitmap(BcspBG, bcsp.Size);
                            break;
                        }
                    case "Search":
                        {
                            Search.BackgroundImage = new Bitmap(LangChangeBG, Search.Size);
                            break;
                        }
                    case "rShift":
                        {
                            rShift.BackgroundImage = new Bitmap(rShiftBG, rShift.Size);
                            break;
                        }
                    case "lShift":
                        {
                            lShift.BackgroundImage = new Bitmap(lShiftBG, lShift.Size);
                            break;
                        }
                    case "Space":
                        {
                            Space.BackgroundImage = new Bitmap(SpaceBG, Space.Size);
                            break;
                        }
                    case "HdKey":
                        {
                            HdKey.BackgroundImage = new Bitmap(HideKeyBG, HdKey.Size);
                            return;
                        }
                    case "lSymb":
                        {
                            lSymb.BackgroundImage = new Bitmap(LangChangeBG, lSymb.Size);
                            break;
                        }
                    case "rSymb":
                        {
                            rSymb.BackgroundImage = new Bitmap(LangChangeBG, rSymb.Size);
                            break;
                        }
                    case "langChg":
                        {
                            langChg.BackgroundImage = new Bitmap(LangChangeBG, langChg.Size);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
        }

        private void Key_MouseDown(object sender, MouseEventArgs e)
        {
            bttnName = ((Button)sender).Name.Length >= 4 ? ((Button)sender).Name : ((Button)sender).Text;
            if (InputBox.Visible && (bttnName.Length <= 4 || bttnName == "Space"))
            {
                bttnRptTmr = 5;
                buttonTimer.Start();
            }
            if (bttnName.Length < 4)
            {
                ((Button)sender).BackgroundImage = new Bitmap(KeyHightBg, ((Button)sender).Size);
            }
            else switch (bttnName)
                {
                    case "bcsp":
                        {
                            bcsp.BackgroundImage = new Bitmap(BcspHightBG, bcsp.Size);
                            break;
                        }
                    case "Search":
                        {
                            Search.BackgroundImage = new Bitmap(LangChangeHightBG, Search.Size);
                            break;
                        }
                    case "rShift":
                        {
                            rShift.BackgroundImage = new Bitmap(rShiftHightBG, rShift.Size);
                            break;
                        }
                    case "lShift":
                        {
                            lShift.BackgroundImage = new Bitmap(lShiftHightBG, lShift.Size);
                            break;
                        }
                    case "Space":
                        {
                            Space.BackgroundImage = new Bitmap(SpaceHightBG, Space.Size);
                            break;
                        }
                    case "HdKey":
                        {
                            HdKey.BackgroundImage = new Bitmap(HideKeyHightBG, HdKey.Size);
                            return;
                        }
                    case "lSymb":
                        {
                            lSymb.BackgroundImage = new Bitmap(LangChangeHightBG, lSymb.Size);
                            break;
                        }
                    case "rSymb":
                        {
                            rSymb.BackgroundImage = new Bitmap(LangChangeHightBG, rSymb.Size);
                            break;
                        }
                    case "langChg":
                        {
                            langChg.BackgroundImage = new Bitmap(LangChangeHightBG, langChg.Size);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            log = new Login("","","","","");
            log.LogOut();
            StopVideo();
            SaveToIni();
        }

        private void InactivityTest_Click(object sender, EventArgs e)
        {
            StopVideo();
            BG = Image.FromFile("Main.jpg");
            this.BackgroundImage = new Bitmap(BG, this.Size);
            InactivityTest.Visible = false;
            Lang1.Visible = Lang2.Visible = InfoBttn.Visible = InstaSearch.Visible = MkSelfie.Visible = true;
            MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = true;
            WaitTimeLeft = 1800;
            WaitModeTmr.Start();
        }

        private void picClick(object sender, MouseEventArgs e)
        {
            if (Cursor.Position.Y <= CursorPos + 20 && Cursor.Position.Y >= CursorPos - 20)
            {
                int page = int.Parse(((PictureBox)sender).Name.Split('_')[1]) - 1;
                int picNum = int.Parse(((PictureBox)sender).Name.Split('_')[2]) - 1;
                int pos = GetLastPosOfSame(imageNumbers, new int[] { page, picNum });
                int num = GetNumberOfSame(imageNumbers, new int[] { page, picNum });
                Label lbl = new Label();
                if (pos < 0 && imageNumbers.Count + ((chngNum==-1)?chosenImages.Count:chosenImages.Count - 1) != 20)
                {
                    ((PictureBox)sender).Image = Image.FromFile("Chosen.png");
                    Button bttn = new Button();
                    bttn.Size = new Size(50, 50);
                    bttn.FlatAppearance.BorderSize = 0;
                    bttn.BackColor = bttn.FlatAppearance.MouseDownBackColor = bttn.FlatAppearance.MouseOverBackColor = bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
                    bttn.FlatStyle = FlatStyle.Flat;
                    bttn.ForeColor = Color.White;
                    bttn.Font = new Font("PF Centro Slab Pro", 28);
                    bttn.Text = "+";
                    bttn.Click += new EventHandler(AddButton_Click);
                    bttn.BackgroundImage = new Bitmap(Image.FromFile("BttnAdd.png"), bttn.Size);
                    bttn.Name = "Add_" + page + "_" + picNum;
                    ((PictureBox)sender).Controls.Add(bttn);
                    bttn.Location = new Point(10, bttn.Parent.Height - 60);
                    lbl.AutoSize = true;
                    lbl.Name = "Text_" + page + "_" + picNum;
                    lbl.Font = new Font("PF Centro Slab Pro", 30);
                    lbl.Text = "1";
                    ((PictureBox)sender).Controls.Add(lbl);
                    lbl.Location = new Point(lbl.Parent.Width / 2 - lbl.Width / 2, lbl.Parent.Height - 60);
                    bttn = new Button();
                    bttn.Size = new Size(50, 50);
                    bttn.FlatAppearance.BorderSize = 0;
                    bttn.BackColor = bttn.FlatAppearance.MouseDownBackColor = bttn.FlatAppearance.MouseOverBackColor = bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
                    bttn.FlatStyle = FlatStyle.Flat;
                    bttn.ForeColor = Color.White;
                    bttn.Text = "-";
                    bttn.Click += new EventHandler(RemoveButton_Click);
                    bttn.BackgroundImage = new Bitmap(Image.FromFile("BttnAdd.png"), bttn.Size);
                    bttn.Font = new Font("PF Centro Slab Pro", 28);
                    bttn.Name = "Remove_" + page + "_" + picNum;
                    ((PictureBox)sender).Controls.Add(bttn);
                    bttn.Location = new Point(bttn.Parent.Width - 60, bttn.Parent.Height - 60);
                    //bttn = new Button();
                    //bttn.Size = new Size(50, 50);
                    //bttn.FlatAppearance.BorderSize = 0;
                    //bttn.BackColor = bttn.FlatAppearance.MouseDownBackColor = bttn.FlatAppearance.MouseOverBackColor = bttn.FlatAppearance.CheckedBackColor = Color.Transparent;
                    //bttn.FlatStyle = FlatStyle.Flat;
                    //bttn.ForeColor = Color.White; 
                    //bttn.BackgroundImage = new Bitmap(bttnEnabledBG, bttn.Size);
                    //bttn.Name = "Edit";
                    //bttn.Click += new EventHandler(EditButton_Click);
                    //bttn.Image = new Bitmap(Edit, bttn.Size);
                    //((PictureBox)sender).Controls.Add(bttn);
                    //bttn.Location = new Point(75, bttn.Parent.Height - 60);
                    imageNumbers.Add(new int[] { page, picNum });
                }
                else if (pos >= 0)
                {
                    imageNumbers.RemoveAt(pos);
                    if (num == 1)
                    {
                        ((PictureBox)sender).Image = null;
                        ((PictureBox)sender).Controls.Clear();
                    }
                    else ((PictureBox)sender).Controls.Find("Text_" + page + "_" + picNum, false)[0].Text = (num - 1).ToString();
                }
            }
            picsSelected.Text = (imageNumbers.Count>0)?loc["SelectedPhotos", imageNumbers.Count]:loc["SelectPhotos"];
            picsSelected.Location = new Point(this.Width/2 - picsSelected.Width/2, picsSelected.Location.Y);
            WaitTimeLeft = 1800;
        }

        private void picsPanel_MouseDown(object sender, MouseEventArgs e)
        {
            MousePos = e.Y;
            CursorPos = Cursor.Position.Y;
            scrolling = true;
        }

        private void picsPanel_MouseUp(object sender, MouseEventArgs e)
        {
            scrolling = false;
        }

        private void picsPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (scrolling)
            {
                picsPanel.AutoScrollPosition = new Point(picsPanel.AutoScrollPosition.X, -picsPanel.AutoScrollPosition.Y + (MousePos - e.Y));
                if (((Control)sender).Name == picsPanel.Name) MousePos = e.Y;
                if (-picsPanel.AutoScrollPosition.Y == picsPanel.DisplayRectangle.Height - 1290 && !backgroundWorker1.IsBusy)
                {
                        scrolling = false;
                        backgroundWorker1.RunWorkerAsync();
                    }
                picsPanel.Refresh();
            }
            WaitTimeLeft = 1800;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(nextPageUrl))
            {
                nextImages("something");
            }
        }

        private void WaitModeTmr_Tick(object sender, EventArgs e)
        {
            WaitTimeLeft--;
            if (AccSearch.Focused && AccSearch.Text == loc["EnterQuery"]) AccSearch.Text = "";
            if (!AccSearch.Focused && AccSearch.Text == "") AccSearch.Text = loc["EnterQuery"];
            if(WaitTimeLeft == 300)
            {
                WaitTimeLeft--;
                if (mes != null) mes.Close();
                mes = new MessageForm(loc["AddTime"], loc["Yes"], loc["No"], 30); 
                DialogResult r = mes.ShowDialog();
                if (r == DialogResult.OK) WaitTimeLeft = 1800;
                if (r == DialogResult.Cancel) WaitTimeLeft = 1;
            }
            if (WaitTimeLeft == 0 && !InactivityTest.Visible)
            {
                reset();
                Lang1.Visible = Lang2.Visible = InfoBttn.Visible = InstaSearch.Visible = MkSelfie.Visible = false;
                MainScreenLbl.Visible = PrintPhoneLbl.Visible = MkSelfieLbl.Visible = VideoFeedbackLbl.Visible = false;
                BG = Image.FromFile("Screen.jpg");
                this.BackgroundImage = new Bitmap(BG, this.Size);
                InactivityTest.Location = new Point(220, 1000);
                InactivityTest.Size = new Size(640, 170);
                InactivityTest.BackgroundImage = new Bitmap(bttnEnabledBG, InactivityTest.Size);
                InactivityTest.Visible = true;
                if (Directory.Exists("videos") && Directory.GetFiles("videos").Length != 0)
                {
                    PlayVideo();
                    WaitTimeLeft = 2000;
                }
                else WaitModeTmr.Stop();
            }
            if (WaitTimeLeft == 0 && InactivityTest.Visible)
            {
                if (Directory.Exists("videos") && Directory.GetFiles("videos").Length != 0)
                {
                    PlayVideo();
                    WaitTimeLeft = 2000;
                }
                else WaitModeTmr.Stop();
            }
        }
        
        private void EditCancel_Click(object sender, EventArgs e)
        {
            chosenImages[chngNum] = Original;
            ExitEdit();
        }

        private void EditOk_Click(object sender, EventArgs e)
        {
            chosenImages[chngNum].Image = SelfView.Image;
            chosenImages[chngNum].Text = InputBox.Text;
            ExitEdit();
        }

        private void CashPaid_TextChanged(object sender, EventArgs e)
        {
            CashPaid.Location = new Point(1050 - CashPaid.Width, CashPaid.Location.Y);
        }

        private void buttonTimer_Tick(object sender, EventArgs e)
        {
            if (bttnRptTmr != 0)
            {
                bttnRptTmr--;
                return;
            }
            int i = InputBox.SelectionStart;
            if (bttnName == "bcsp")
            {
                if (InputBox.Text != "" && i >= 0)
                {
                    if (InputBox.SelectionLength > 0) 
                    {
                        InputBox.Text = InputBox.Text.Remove(i, InputBox.SelectionLength);
                        InputBox.SelectionStart = i;
                    }
                    else
                    {
                        InputBox.Text = InputBox.Text.Remove(i - 1, 1);
                        InputBox.SelectionStart = (i - 1) <= 0 ? 0 : (i - 1);
                    }
                }
            }
            else
            {
                InputBox.Text = InputBox.Text.Insert(i, bttnName=="Space"?" ":bttnName);
                InputBox.SelectionStart = i + 1;
                InputBox.SelectionLength = 0;
            }
        }

        private void picsPanel_Scroll(object sender, ScrollEventArgs e)
        {
            picsPanel.VerticalScroll.Value = e.NewValue>=0?e.NewValue:0;
        }

        private void SelfView_MouseDown(object sender, MouseEventArgs e)
        {
            MousePos = MousePosition.X;
        }

        private void SelfView_MouseUp(object sender, MouseEventArgs e)
        {
            if (!TurnLeft.Visible) return;
            if (MousePosition.X <= MousePos + 20 && MousePosition.X >= MousePos - 20) return;
            if (MousePosition.X < MousePos - 20) editNextPic();
            else editPrevPic();
            Original = chosenImages[chngNum];
            imToProcess = SelfView.Image = new Bitmap(chosenImages[chngNum].Image, SelfView.Size);
            ApplyFilters();
            justCameToPage = true;
            InputBox.Text = chosenImages[chngNum].Text;
            WaitTimeLeft = 1800;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (imageNumbers.Count + chosenImages.Count >= 20) return;
            int page = int.Parse(((Button)sender).Name.Split('_')[1]);
            int picNum = int.Parse(((Button)sender).Name.Split('_')[2]);
            imageNumbers.Add(new int[] { page, picNum });
            ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Text = GetNumberOfSame(imageNumbers, new int[] { page, picNum }).ToString();
            ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Location = new Point(((Button)sender).Parent.Width / 2 - ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Width / 2, ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Location.Y);
            picsSelected.Text = (imageNumbers.Count > 0) ? loc["SelectedPhotos", imageNumbers.Count] : loc["SelectPhotos"];
            picsSelected.Location = new Point(this.Width / 2 - picsSelected.Width / 2, picsSelected.Location.Y);
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            int page = int.Parse(((Button)sender).Name.Split('_')[1]);
            int picNum = int.Parse(((Button)sender).Name.Split('_')[2]);
            int pos = GetLastPosOfSame(imageNumbers, new int[] { page, picNum });
            int num = GetNumberOfSame(imageNumbers, new int[] { page, picNum });
            imageNumbers.RemoveAt(pos);
            ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Text = (num - 1).ToString();
            ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Location = new Point(((Button)sender).Parent.Width / 2 - ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Width / 2, ((Button)sender).Parent.Controls.Find("Text_" + page + "_" + picNum, false)[0].Location.Y);
            pos = GetLastPosOfSame(imageNumbers, new int[] { page, picNum });
            if (num == 1)
            {
                foreach (PictureBox p in picsPanel.Controls)
                {
                    if (p.Name == "pic_" + (page+1) + "_" + (picNum+1))
                    {
                        p.Controls.Clear();
                        p.Image = null;
                        break;
                    }
                }
            }
            picsSelected.Text = (imageNumbers.Count>0)?loc["SelectedPhotos",imageNumbers.Count]:loc["SelectPhotos"];
            picsSelected.Location = new Point(this.Width/2 - picsSelected.Width/2, picsSelected.Location.Y);
        }

        private async void locTimer_Tick(object sender, EventArgs e)
        {
            CheckMonitoring();
            if(rs != RegisterState.ServerDown)
            {
                s = rs;
            }
            if(s == RegisterState.ImageAvailable)
            {
                Down();
            }
            lm.Unload(curLocale);
            loc = lm.Load(curLocale);            
            if(s == RegisterState.AvaibableConfig)
            {
                var config = await api.GetConfigAsync();
                if (!config.Succesfull) return;
                photoPrice = config.Price;
                timerSecs = config.Seconds == 0 ? timerSecs : config.Seconds;
                locTimer.Interval = timerSecs * 1000;
            }
        }

        private void Waiting_Tick(object sender, EventArgs e)
        {
            timeout = true;
            reload();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            isShown = true;
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (InputBox.Visible)
            {
                if (Keyboard.Visible)
                {
                    Keyboard.Visible = false;
                    SelfView.Focus();
                }
            }
            else return;
        }

        private void SelfView_MouseClick(object sender, MouseEventArgs e)
        {
            if (InputBox.Visible)
            {
                if (Keyboard.Visible)
                {
                    Keyboard.Visible = false;
                    SelfView.Focus();
                }
            }
            else return;
        }

        private void pan_MouseDown(object sender, MouseEventArgs e)
        {
            MousePos = e.X;
            CursorPos = Cursor.Position.X;
            filtersScroll = true;
        }

        private void pan_MouseUp(object sender, MouseEventArgs e)
        {
            filtersScroll = false;
        }

        private void pan_MouseMove(object sender, MouseEventArgs e)
        {
            if(filtersScroll)
            {
                ((Panel)Controls["FilPan"]).AutoScrollPosition = new Point(-((Panel)Controls["FilPan"]).AutoScrollPosition.X + (MousePos - e.X), ((Panel)Controls["FilPan"]).AutoScrollPosition.Y);
                if (((Control)sender).Name == "FilPan") MousePos = e.Y;
                Controls["FilPan"].Refresh();
            }
        }

        private void InputBox_TextChanged(object sender, EventArgs e)
        {
            int selInd = InputBox.SelectionStart;
            label1.Text = InputBox.Text;
            if (label1.Text.EndsWith(Environment.NewLine)) label1.Text += "+";
            if (label1.Height >= InputBox.Height)
            {
                InputBox.TextChanged -= InputBox_TextChanged;
                if (justCameToPage)
                {
                    justCameToPage = false;
                    string[] textLines = InputBox.Lines;
                    int i = 0;
                    label1.Text = "";
                    for (; i < InputBox.Lines.Length; i++)
                    {
                        inpTemp = label1.Text;
                        label1.Text += InputBox.Lines[i] + Environment.NewLine;
                        if (label1.Height + label2.Height >= InputBox.Height)
                        {
                            InputBox.Text = inpTemp + Environment.NewLine;
                            break;
                        }
                    }
                    label1.Text = inpTemp;
                    for (int j = 0; j < textLines[i].Length; j++)
                    {
                        label1.Text += textLines[i][j].ToString();
                        if (label1.Height >= InputBox.Height)
                        {
                            break;
                        }
                        inpTemp = label1.Text;
                    }
                }
                selInd -= InputBox.Text.Length - inpTemp.Length;
                InputBox.Text = inpTemp;
                InputBox.SelectionLength = 0;
                InputBox.SelectionStart = selInd <= 0 ? 0 : selInd;
                InputBox.TextChanged += InputBox_TextChanged;
                textChecker.Start();
                return;
            }

            inpTemp = InputBox.Text;
            InputBox.SelectionStart = selInd;
        }

        private void textChecker_Tick(object sender, EventArgs e)
        {
            int selInd = InputBox.SelectionStart;
            if (label1.Height >= InputBox.Height)
            {
                string[] tempLines = InputBox.Lines;
                label1.Text = "";
                int i = 0;
                for (; i < tempLines.Length - 1; i++)
                {
                    label1.Text += tempLines[i] + Environment.NewLine;
                }
                for (int j = 0; j < tempLines[i].Length; j++)
                {
                    label1.Text += tempLines[i][j].ToString();
                    if (label1.Height >= InputBox.Height)
                    {
                        break;
                    }
                    inpTemp = label1.Text;
                }
                InputBox.Text = inpTemp;
                textChecker.Stop();
            }
            if (InputBox.Text.Length >= selInd) InputBox.SelectionStart = selInd;
        }
        #endregion
    }

    class InstaSearch
    {
        private ManualResetEvent _doneEvent;
        private int ind;
        private string imgURL;
        public InstaSearch(ManualResetEvent doneEvent, int i, string imURL)
        {
            _doneEvent = doneEvent;
            ind = i;
            imgURL = imURL;
        }
        public void ThreadPoolCallback(Object threadContext)
        {
            int threadIndex = int.Parse((string)(threadContext.ToString().Clone()));
            WebRequest webRequest = WebRequest.Create(imgURL);
            webRequest.Proxy = null;
            var imageResponse = webRequest.GetResponse();
            Form1.NextImages[ind] = (Image)Image.FromStream(imageResponse.GetResponseStream()).Clone();
            imageResponse.Close();
            _doneEvent.Set();
        }
    }

    class InstagramUser
    {
        string usrname;
        string fllName;
        long i;
        Image profPic;
        public string Username
        {
            get { return usrname; }
        }
        public string FullName
        {
            get { return fllName; }
        }
        public long Id
        {
            get { return i; }
        }
        public Image ProfilePic
        {
            get { return profPic; }
        }
        public InstagramUser(string username, string fullname, long id, Image profilePic)
        {
            usrname = username;
            fllName = fullname;
            i = id;
            profPic = profilePic;
        }
    }

    class ImageWithText
    {
        Image img;
        string txt;
        public ImageWithText(Image image, string text)
        {
            img = image;
            txt = text;
        }
        public Image Image
        {
            get { return img; }
            set { img = value; }
        }
        public string Text
        {
            get { return txt; }
            set { txt = value; }
        }
    }

    class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 4500;
            ((HttpWebRequest)w).ReadWriteTimeout = 4500;
            return w;
        }
    }
}
#region unused methods
/*int QuintOut(int t, int b, int c, int d)
{
    t /= d;
    t--;
    return c*(t*t*t*t*t + 1) + b;
}*/

/*private void Prev_Click(object sender, EventArgs e)
{
    byte i;
    curPage--;
    for (i = 0; i < 30; i++)
    {
        if (imgs[curPage-1][i] != null) nxtImages[i] = (Image)imgs[curPage-1][i].Image.Clone();
        else break;
    }
    for (i = 0; i < 30; i++)
    {
        if (!pics[i].Visible) pics[i].Visible = true;
        pics[i].Image = new Bitmap(nxtImages[i], pics[i].Size);
    }
    Next.Enabled = true;
    if (curPage == 1) Prev.Enabled = false;
}*/

/*private void FiltersBack_Click(object sender, EventArgs e)
{
    FiltersNext.Visible = FiltersBack.Visible = FiltersOk.Visible = FiltersPrev.Visible = false;
    SelfView.Visible = false;
    foreach (PictureBox pb in filters)
    {
        pb.Visible = false;
    }
    if (swtch == 1)
    {
        BG = Image.FromFile("SearchRes.jpg");
        this.BackgroundImage = new Bitmap(BG, this.Size);
        foreach (PictureBox[] ps in pics) foreach(PictureBox p in ps) p.Visible = true;
        Next.Visible = Prev.Visible = SearchBack.Visible = true;
    }
    if (swtch == 2)
    {
        SelfView.Location = new Point(0, this.Height / 2 - this.Width * 3 / 8);
        BG = Image.FromFile("SelfieMake.jpg");
        this.BackgroundImage = new Bitmap(BG, this.Size);
        TimerTime.Text = loc["Timer"];
        TimerTime.Location = new Point(800 - TimerTime.Width / 2, 1770);
        SelfPreview.Visible = RtnMain.Visible = SelfieTake.Visible = TimerChecker.Visible = TimerTime.Visible = true;
    }   
}*/

/*private void EditButton_Click(object sender, EventArgs e)
   {
       MessageBox.Show("Editing " + ((Button)sender).Parent.Name);
   }*/

/*private void pic_Click(object sender, EventArgs e)
{
    if (String.IsNullOrEmpty(imgsUrls[curPage - 1][int.Parse(((PictureBox)sender).Name.Substring(3)) - 1])) return;
    LoadInProgress.Create("");
    var imageResponse = WebRequest.Create(imgsUrls[curPage - 1][int.Parse(((PictureBox)sender).Name.Substring(3)) - 1]).GetResponse().GetResponseStream();
    imToProcess = (Image)Image.FromStream(imageResponse).Clone();
    foreach (PictureBox p in pics) p.Visible = false;
    Next.Visible = Prev.Visible = SearchBack.Visible = false;
    for (byte i = 0; i < 4; i++)
    {
        filters[i].Location = new Point(i * 210 + 125, 1375);
        filters[i].Size = new Size((int)(200 * wdth), (int)(200 * wdth));
    }
    BG = Image.FromFile("Filters.jpg");
    this.BackgroundImage = new Bitmap(BG, this.Size);
    FiltersNext.Visible = FiltersBack.Visible = FiltersOk.Visible = FiltersPrev.Visible = true;
    SelfView.Location = new Point(0, this.Height / 16);
    SelfView.Size = new Size(this.Width, this.Width);
    SelfView.Image = new Bitmap((Image)imToProcess.Clone(), SelfView.Size);
    foreach (PictureBox pb in filters)
    {
        pb.Visible = true;
        pb.Height = pb.Width;
    }
    fltPg = 0;
    ApplyFilters();
    SelfView.Visible = true;
    LoadInProgress.Destroy();
}*/
#endregion