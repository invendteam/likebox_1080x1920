﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using InVendApi;
using InVendApi.InVendMonitorApi;

namespace Lkbox
{
    static class Program
    {
        static InVendMonitorApi api;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Process[] ps = Process.GetProcessesByName("Lkbox");
            Process pc = Process.GetCurrentProcess();
            if (ps.Length > 1)
            {
                foreach (Process p in ps)
                {
                    if (p.Id != pc.Id) p.Kill();
                }
            }
            try
            {
                //api = new InVendMonitorApi("online.invend.ru", "1", OnlineMonitorEnums.TerminalType.Instamat, TimeSpan.FromSeconds(10));
                Application.Run(new Form1());
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message + Environment.NewLine + x.StackTrace);
                //Task s = api.SendlogAsync(new LogMessage(OnlineMonitorEnums.InstamatLogType.TerminalError, "Last user money: " + Form1.cash + Environment.NewLine + "Error: " + x.Message, DateTime.Now));
            }
            pc.Kill();
        }
    }
}
