﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lkbox
{
    public partial class LoadInProgress : Form
    {
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    {
                        base.WndProc(ref m);
                        if (m.Result.ToInt32() == -1) m.Result = new IntPtr(1);
                        break;
                    }
                default:
                    {
                        base.WndProc(ref m);
                        break;
                    }
            }
        }
        private static LoadInProgress mInstance;
        public static void Create(string str)
        {
            var t = new System.Threading.Thread(() =>
            {
                mInstance = new LoadInProgress(str);
                mInstance.FormClosed += (s, e) => mInstance = null;
                Application.Run(mInstance);
            });
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.IsBackground = true;
            t.Start();
        }
        public static void Destroy()
        {
            if (mInstance != null) mInstance.Invoke(new Action(() => mInstance.Close()));
        }
        Image gif = Image.FromFile("loading.gif");
        public LoadInProgress(string str)
        {
            InitializeComponent();
            TransparencyKey = Color.Fuchsia;
            pictureBox1.Size = gif.Size;
            pictureBox1.Location = new Point(540 - gif.Width / 2, 960 - gif.Height / 2);
            pictureBox1.Image = gif;
            label1.Text = str;
            label1.Location = new Point(540 - label1.Width / 2, pictureBox1.Location.Y + gif.Height + 50);
        }

        public static void ChangeLabelText(string str)
        {
            if (mInstance != null)
            {
                mInstance.Invoke(new Action(() => mInstance.label1.Text = str));
                mInstance.Invoke(new Action(() => mInstance.label1.Location = new Point(540 - mInstance.label1.Width / 2, mInstance.label1.Location.Y)));
            }
        }
    }
}
