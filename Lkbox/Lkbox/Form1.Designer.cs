﻿namespace Lkbox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MkSelfie = new System.Windows.Forms.Button();
            this.InstaSearch = new System.Windows.Forms.Button();
            this.sp = new System.IO.Ports.SerialPort(this.components);
            this.tmr = new System.Windows.Forms.Timer(this.components);
            this.SelfPreview = new System.Windows.Forms.PictureBox();
            this.SelfView = new System.Windows.Forms.PictureBox();
            this.RtnMain = new System.Windows.Forms.Button();
            this.SelfieTake = new System.Windows.Forms.Button();
            this.PhotoOk = new System.Windows.Forms.Button();
            this.RetakeSelf = new System.Windows.Forms.Button();
            this.BttnSearchBack = new System.Windows.Forms.Button();
            this.CartOk = new System.Windows.Forms.Button();
            this.pd = new System.Drawing.Printing.PrintDocument();
            this.tmer = new System.Windows.Forms.Timer(this.components);
            this.cartIm_1 = new System.Windows.Forms.PictureBox();
            this.cartIm_2 = new System.Windows.Forms.PictureBox();
            this.cartIm_3 = new System.Windows.Forms.PictureBox();
            this.cartIm_4 = new System.Windows.Forms.PictureBox();
            this.cartIm_5 = new System.Windows.Forms.PictureBox();
            this.cartIm_6 = new System.Windows.Forms.PictureBox();
            this.cartIm_7 = new System.Windows.Forms.PictureBox();
            this.cartIm_8 = new System.Windows.Forms.PictureBox();
            this.Print = new System.Windows.Forms.Button();
            this.PhotoAmmLbl = new System.Windows.Forms.Label();
            this.CashAmmLbl = new System.Windows.Forms.Label();
            this.CashNeeded = new System.Windows.Forms.Label();
            this.PrintBack = new System.Windows.Forms.Button();
            this.RstBttn = new System.Windows.Forms.Button();
            this.Next = new System.Windows.Forms.Button();
            this.Prev = new System.Windows.Forms.Button();
            this.SearchBack = new System.Windows.Forms.Button();
            this.FiltersBack = new System.Windows.Forms.Button();
            this.FiltersOk = new System.Windows.Forms.Button();
            this.FiltersNext = new System.Windows.Forms.Button();
            this.FiltersPrev = new System.Windows.Forms.Button();
            this.filterPic1 = new System.Windows.Forms.PictureBox();
            this.filterPic2 = new System.Windows.Forms.PictureBox();
            this.filterPic3 = new System.Windows.Forms.PictureBox();
            this.InfoBttn = new System.Windows.Forms.Button();
            this.InfoClose = new System.Windows.Forms.Button();
            this.cartIm_9 = new System.Windows.Forms.PictureBox();
            this.cartIm_10 = new System.Windows.Forms.PictureBox();
            this.filterPic4 = new System.Windows.Forms.PictureBox();
            this.slfyTimer = new System.Windows.Forms.Timer(this.components);
            this.TimerChecker = new System.Windows.Forms.PictureBox();
            this.TimerTime = new System.Windows.Forms.Label();
            this.AccSearch = new System.Windows.Forms.TextBox();
            this.BalLbl = new System.Windows.Forms.Label();
            this.CashPaid = new System.Windows.Forms.Label();
            this.OrderDel = new System.Windows.Forms.Button();
            this.InputBox = new System.Windows.Forms.RichTextBox();
            this.AddText = new System.Windows.Forms.Button();
            this.TurnRight = new System.Windows.Forms.Button();
            this.TurnLeft = new System.Windows.Forms.Button();
            this.TextOk = new System.Windows.Forms.Button();
            this.PrintPageLbl = new System.Windows.Forms.Label();
            this.printTimer = new System.Windows.Forms.Timer(this.components);
            this.searchTimer = new System.Windows.Forms.Timer(this.components);
            this.Lang1 = new System.Windows.Forms.Button();
            this.Lang2 = new System.Windows.Forms.Button();
            this.PrintEven = new System.Windows.Forms.Label();
            this.VideoPlay = new System.Windows.Forms.Panel();
            this.InactivityTest = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.loadingPic = new System.Windows.Forms.PictureBox();
            this.WaitModeTmr = new System.Windows.Forms.Timer(this.components);
            this.EditCancel = new System.Windows.Forms.Button();
            this.EditOk = new System.Windows.Forms.Button();
            this.rtnMnPnl = new System.Windows.Forms.Panel();
            this.MainScreenLbl = new System.Windows.Forms.Label();
            this.PrintPhoneLbl = new System.Windows.Forms.Label();
            this.MkSelfieLbl = new System.Windows.Forms.Label();
            this.VideoFeedbackLbl = new System.Windows.Forms.Label();
            this.CartLbl = new System.Windows.Forms.Label();
            this.OrderedLbl = new System.Windows.Forms.Label();
            this.NeedCashLbl = new System.Windows.Forms.Label();
            this.PaidCashLbl = new System.Windows.Forms.Label();
            this.PrintingLbl = new System.Windows.Forms.Label();
            this.AcceptedCashLbl = new System.Windows.Forms.Label();
            this.NoCoinsLbl = new System.Windows.Forms.Label();
            this.SupportLbl = new System.Windows.Forms.Label();
            this.ThanksLbl = new System.Windows.Forms.Label();
            this.AutoMainLbl = new System.Windows.Forms.Label();
            this.DoNotForgetLbl = new System.Windows.Forms.Label();
            this.buttonTimer = new System.Windows.Forms.Timer(this.components);
            this.locTimer = new System.Windows.Forms.Timer(this.components);
            this.ToEditLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.filterPic5 = new System.Windows.Forms.PictureBox();
            this.filterPic6 = new System.Windows.Forms.PictureBox();
            this.filterPic7 = new System.Windows.Forms.PictureBox();
            this.filterPic8 = new System.Windows.Forms.PictureBox();
            this.filterPic9 = new System.Windows.Forms.PictureBox();
            this.filterPic10 = new System.Windows.Forms.PictureBox();
            this.filterPic11 = new System.Windows.Forms.PictureBox();
            this.filterPic12 = new System.Windows.Forms.PictureBox();
            this.FullName1 = new System.Windows.Forms.Label();
            this.User1 = new System.Windows.Forms.Label();
            this.UserPic1 = new System.Windows.Forms.PictureBox();
            this.GetUser1 = new System.Windows.Forms.Panel();
            this.FullName6 = new System.Windows.Forms.Label();
            this.User6 = new System.Windows.Forms.Label();
            this.UserPic6 = new System.Windows.Forms.PictureBox();
            this.GetUser6 = new System.Windows.Forms.Panel();
            this.User3 = new System.Windows.Forms.Label();
            this.FullName3 = new System.Windows.Forms.Label();
            this.UserPic3 = new System.Windows.Forms.PictureBox();
            this.GetUser3 = new System.Windows.Forms.Panel();
            this.User7 = new System.Windows.Forms.Label();
            this.FullName7 = new System.Windows.Forms.Label();
            this.UserPic7 = new System.Windows.Forms.PictureBox();
            this.GetUser7 = new System.Windows.Forms.Panel();
            this.UserPic5 = new System.Windows.Forms.PictureBox();
            this.FullName5 = new System.Windows.Forms.Label();
            this.User5 = new System.Windows.Forms.Label();
            this.GetUser5 = new System.Windows.Forms.Panel();
            this.UserPic8 = new System.Windows.Forms.PictureBox();
            this.FullName8 = new System.Windows.Forms.Label();
            this.User8 = new System.Windows.Forms.Label();
            this.GetUser8 = new System.Windows.Forms.Panel();
            this.User2 = new System.Windows.Forms.Label();
            this.FullName2 = new System.Windows.Forms.Label();
            this.UserPic2 = new System.Windows.Forms.PictureBox();
            this.GetUser2 = new System.Windows.Forms.Panel();
            this.User9 = new System.Windows.Forms.Label();
            this.FullName9 = new System.Windows.Forms.Label();
            this.UserPic9 = new System.Windows.Forms.PictureBox();
            this.GetUser9 = new System.Windows.Forms.Panel();
            this.User4 = new System.Windows.Forms.Label();
            this.FullName4 = new System.Windows.Forms.Label();
            this.UserPic4 = new System.Windows.Forms.PictureBox();
            this.GetUser4 = new System.Windows.Forms.Panel();
            this.User10 = new System.Windows.Forms.Label();
            this.FullName10 = new System.Windows.Forms.Label();
            this.UserPic10 = new System.Windows.Forms.PictureBox();
            this.GetUser10 = new System.Windows.Forms.Panel();
            this.cartIm_11 = new System.Windows.Forms.PictureBox();
            this.cartIm_12 = new System.Windows.Forms.PictureBox();
            this.cartIm_13 = new System.Windows.Forms.PictureBox();
            this.cartIm_14 = new System.Windows.Forms.PictureBox();
            this.cartIm_15 = new System.Windows.Forms.PictureBox();
            this.cartIm_16 = new System.Windows.Forms.PictureBox();
            this.cartIm_17 = new System.Windows.Forms.PictureBox();
            this.cartIm_18 = new System.Windows.Forms.PictureBox();
            this.cartIm_19 = new System.Windows.Forms.PictureBox();
            this.cartIm_20 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.picsPanel = new Lkbox.DoubleBufferedPanel();
            this.PrintProgressBar = new Lkbox.ProgressBarEx();
            this.Keyboard = new Lkbox.DoubleBufferedPanel();
            this.HdKey = new System.Windows.Forms.Button();
            this.rSymb = new System.Windows.Forms.Button();
            this.Space = new System.Windows.Forms.Button();
            this.langChg = new System.Windows.Forms.Button();
            this.lSymb = new System.Windows.Forms.Button();
            this.rShift = new System.Windows.Forms.Button();
            this.l9 = new System.Windows.Forms.Button();
            this.l8 = new System.Windows.Forms.Button();
            this.l7 = new System.Windows.Forms.Button();
            this.l6 = new System.Windows.Forms.Button();
            this.l5 = new System.Windows.Forms.Button();
            this.l4 = new System.Windows.Forms.Button();
            this.l3 = new System.Windows.Forms.Button();
            this.l2 = new System.Windows.Forms.Button();
            this.l1 = new System.Windows.Forms.Button();
            this.lShift = new System.Windows.Forms.Button();
            this.Search = new System.Windows.Forms.Button();
            this.m9 = new System.Windows.Forms.Button();
            this.m8 = new System.Windows.Forms.Button();
            this.m7 = new System.Windows.Forms.Button();
            this.m6 = new System.Windows.Forms.Button();
            this.m5 = new System.Windows.Forms.Button();
            this.m4 = new System.Windows.Forms.Button();
            this.m3 = new System.Windows.Forms.Button();
            this.m2 = new System.Windows.Forms.Button();
            this.m1 = new System.Windows.Forms.Button();
            this.bcsp = new System.Windows.Forms.Button();
            this.u10 = new System.Windows.Forms.Button();
            this.u9 = new System.Windows.Forms.Button();
            this.l11 = new System.Windows.Forms.Button();
            this.l10 = new System.Windows.Forms.Button();
            this.m11 = new System.Windows.Forms.Button();
            this.m10 = new System.Windows.Forms.Button();
            this.u12 = new System.Windows.Forms.Button();
            this.u11 = new System.Windows.Forms.Button();
            this.u8 = new System.Windows.Forms.Button();
            this.u7 = new System.Windows.Forms.Button();
            this.u6 = new System.Windows.Forms.Button();
            this.u5 = new System.Windows.Forms.Button();
            this.u4 = new System.Windows.Forms.Button();
            this.u3 = new System.Windows.Forms.Button();
            this.u2 = new System.Windows.Forms.Button();
            this.u1 = new System.Windows.Forms.Button();
            this.textChecker = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.SelfPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerChecker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadingPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic1)).BeginInit();
            this.GetUser1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic6)).BeginInit();
            this.GetUser6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic3)).BeginInit();
            this.GetUser3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic7)).BeginInit();
            this.GetUser7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic5)).BeginInit();
            this.GetUser5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic8)).BeginInit();
            this.GetUser8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic2)).BeginInit();
            this.GetUser2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic9)).BeginInit();
            this.GetUser9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic4)).BeginInit();
            this.GetUser4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic10)).BeginInit();
            this.GetUser10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_20)).BeginInit();
            this.Keyboard.SuspendLayout();
            this.SuspendLayout();
            // 
            // MkSelfie
            // 
            this.MkSelfie.BackColor = System.Drawing.Color.Transparent;
            this.MkSelfie.FlatAppearance.BorderSize = 0;
            this.MkSelfie.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.MkSelfie.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MkSelfie.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MkSelfie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MkSelfie.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MkSelfie.ForeColor = System.Drawing.Color.White;
            this.MkSelfie.Location = new System.Drawing.Point(10, 5);
            this.MkSelfie.Name = "MkSelfie";
            this.MkSelfie.Size = new System.Drawing.Size(79, 73);
            this.MkSelfie.TabIndex = 0;
            this.MkSelfie.TabStop = false;
            this.MkSelfie.UseVisualStyleBackColor = false;
            this.MkSelfie.Visible = false;
            this.MkSelfie.Click += new System.EventHandler(this.MkSelfie_Click);
            // 
            // InstaSearch
            // 
            this.InstaSearch.BackColor = System.Drawing.Color.Transparent;
            this.InstaSearch.FlatAppearance.BorderSize = 0;
            this.InstaSearch.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.InstaSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InstaSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InstaSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InstaSearch.Font = new System.Drawing.Font("PF Centro Slab Pro", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstaSearch.ForeColor = System.Drawing.Color.LightGray;
            this.InstaSearch.Location = new System.Drawing.Point(12, 91);
            this.InstaSearch.Name = "InstaSearch";
            this.InstaSearch.Size = new System.Drawing.Size(64, 42);
            this.InstaSearch.TabIndex = 1;
            this.InstaSearch.TabStop = false;
            this.InstaSearch.Text = "Поиск";
            this.InstaSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InstaSearch.UseVisualStyleBackColor = false;
            this.InstaSearch.Visible = false;
            this.InstaSearch.Click += new System.EventHandler(this.InstaSearch_Click);
            // 
            // tmr
            // 
            this.tmr.Tick += new System.EventHandler(this.tmr_Tick);
            // 
            // SelfPreview
            // 
            this.SelfPreview.BackColor = System.Drawing.Color.Transparent;
            this.SelfPreview.Location = new System.Drawing.Point(12, 12);
            this.SelfPreview.Name = "SelfPreview";
            this.SelfPreview.Size = new System.Drawing.Size(640, 480);
            this.SelfPreview.TabIndex = 2;
            this.SelfPreview.TabStop = false;
            this.SelfPreview.Visible = false;
            this.SelfPreview.VisibleChanged += new System.EventHandler(this.SelfPreview_VisibleChanged);
            // 
            // SelfView
            // 
            this.SelfView.BackColor = System.Drawing.Color.Transparent;
            this.SelfView.Location = new System.Drawing.Point(12, 12);
            this.SelfView.Name = "SelfView";
            this.SelfView.Size = new System.Drawing.Size(640, 480);
            this.SelfView.TabIndex = 2;
            this.SelfView.TabStop = false;
            this.SelfView.Visible = false;
            this.SelfView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SelfView_MouseClick);
            this.SelfView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SelfView_MouseDown);
            this.SelfView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SelfView_MouseUp);
            // 
            // RtnMain
            // 
            this.RtnMain.BackColor = System.Drawing.Color.Transparent;
            this.RtnMain.FlatAppearance.BorderSize = 0;
            this.RtnMain.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.RtnMain.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.RtnMain.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RtnMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RtnMain.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RtnMain.ForeColor = System.Drawing.Color.White;
            this.RtnMain.Location = new System.Drawing.Point(666, 284);
            this.RtnMain.Name = "RtnMain";
            this.RtnMain.Size = new System.Drawing.Size(75, 23);
            this.RtnMain.TabIndex = 3;
            this.RtnMain.TabStop = false;
            this.RtnMain.UseVisualStyleBackColor = false;
            this.RtnMain.Visible = false;
            this.RtnMain.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.RtnMain.Click += new System.EventHandler(this.RtnMain_Click);
            // 
            // SelfieTake
            // 
            this.SelfieTake.BackColor = System.Drawing.Color.Transparent;
            this.SelfieTake.FlatAppearance.BorderSize = 0;
            this.SelfieTake.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.SelfieTake.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SelfieTake.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SelfieTake.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelfieTake.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelfieTake.ForeColor = System.Drawing.Color.White;
            this.SelfieTake.Location = new System.Drawing.Point(674, 217);
            this.SelfieTake.Name = "SelfieTake";
            this.SelfieTake.Size = new System.Drawing.Size(75, 23);
            this.SelfieTake.TabIndex = 3;
            this.SelfieTake.TabStop = false;
            this.SelfieTake.UseVisualStyleBackColor = false;
            this.SelfieTake.Visible = false;
            this.SelfieTake.Click += new System.EventHandler(this.SelfieTake_Click);
            // 
            // PhotoOk
            // 
            this.PhotoOk.BackColor = System.Drawing.Color.Transparent;
            this.PhotoOk.FlatAppearance.BorderSize = 0;
            this.PhotoOk.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.PhotoOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PhotoOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PhotoOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PhotoOk.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PhotoOk.ForeColor = System.Drawing.Color.White;
            this.PhotoOk.Location = new System.Drawing.Point(758, 217);
            this.PhotoOk.Name = "PhotoOk";
            this.PhotoOk.Size = new System.Drawing.Size(75, 23);
            this.PhotoOk.TabIndex = 3;
            this.PhotoOk.TabStop = false;
            this.PhotoOk.UseVisualStyleBackColor = false;
            this.PhotoOk.Visible = false;
            this.PhotoOk.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.PhotoOk.Click += new System.EventHandler(this.PhotoOk_Click);
            // 
            // RetakeSelf
            // 
            this.RetakeSelf.BackColor = System.Drawing.Color.Transparent;
            this.RetakeSelf.FlatAppearance.BorderSize = 0;
            this.RetakeSelf.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.RetakeSelf.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.RetakeSelf.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RetakeSelf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RetakeSelf.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RetakeSelf.ForeColor = System.Drawing.Color.White;
            this.RetakeSelf.Location = new System.Drawing.Point(755, 264);
            this.RetakeSelf.Name = "RetakeSelf";
            this.RetakeSelf.Size = new System.Drawing.Size(75, 23);
            this.RetakeSelf.TabIndex = 3;
            this.RetakeSelf.TabStop = false;
            this.RetakeSelf.UseVisualStyleBackColor = false;
            this.RetakeSelf.Visible = false;
            this.RetakeSelf.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.RetakeSelf.Click += new System.EventHandler(this.RetakeSelf_Click);
            // 
            // BttnSearchBack
            // 
            this.BttnSearchBack.BackColor = System.Drawing.Color.Transparent;
            this.BttnSearchBack.FlatAppearance.BorderSize = 0;
            this.BttnSearchBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BttnSearchBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BttnSearchBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BttnSearchBack.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.BttnSearchBack.ForeColor = System.Drawing.Color.White;
            this.BttnSearchBack.Location = new System.Drawing.Point(389, 531);
            this.BttnSearchBack.Name = "BttnSearchBack";
            this.BttnSearchBack.Size = new System.Drawing.Size(127, 47);
            this.BttnSearchBack.TabIndex = 5;
            this.BttnSearchBack.TabStop = false;
            this.BttnSearchBack.UseVisualStyleBackColor = false;
            this.BttnSearchBack.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.BttnSearchBack.Click += new System.EventHandler(this.RtnMain_Click);
            // 
            // CartOk
            // 
            this.CartOk.BackColor = System.Drawing.Color.Transparent;
            this.CartOk.FlatAppearance.BorderSize = 0;
            this.CartOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.CartOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.CartOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CartOk.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CartOk.ForeColor = System.Drawing.Color.White;
            this.CartOk.Location = new System.Drawing.Point(674, 179);
            this.CartOk.Name = "CartOk";
            this.CartOk.Size = new System.Drawing.Size(138, 32);
            this.CartOk.TabIndex = 6;
            this.CartOk.TabStop = false;
            this.CartOk.UseVisualStyleBackColor = false;
            this.CartOk.Visible = false;
            this.CartOk.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.CartOk.Click += new System.EventHandler(this.CartOk_Click);
            // 
            // pd
            // 
            this.pd.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.pd_PrintPage);
            // 
            // tmer
            // 
            this.tmer.Interval = 1000;
            this.tmer.Tick += new System.EventHandler(this.tmer_Tick);
            // 
            // cartIm_1
            // 
            this.cartIm_1.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_1.Location = new System.Drawing.Point(658, 12);
            this.cartIm_1.Name = "cartIm_1";
            this.cartIm_1.Size = new System.Drawing.Size(200, 200);
            this.cartIm_1.TabIndex = 7;
            this.cartIm_1.TabStop = false;
            this.cartIm_1.Visible = false;
            this.cartIm_1.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_2
            // 
            this.cartIm_2.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_2.Location = new System.Drawing.Point(658, 12);
            this.cartIm_2.Name = "cartIm_2";
            this.cartIm_2.Size = new System.Drawing.Size(200, 200);
            this.cartIm_2.TabIndex = 7;
            this.cartIm_2.TabStop = false;
            this.cartIm_2.Visible = false;
            this.cartIm_2.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_3
            // 
            this.cartIm_3.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_3.Location = new System.Drawing.Point(658, 12);
            this.cartIm_3.Name = "cartIm_3";
            this.cartIm_3.Size = new System.Drawing.Size(200, 200);
            this.cartIm_3.TabIndex = 7;
            this.cartIm_3.TabStop = false;
            this.cartIm_3.Visible = false;
            this.cartIm_3.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_4
            // 
            this.cartIm_4.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_4.Location = new System.Drawing.Point(658, 12);
            this.cartIm_4.Name = "cartIm_4";
            this.cartIm_4.Size = new System.Drawing.Size(200, 200);
            this.cartIm_4.TabIndex = 7;
            this.cartIm_4.TabStop = false;
            this.cartIm_4.Visible = false;
            this.cartIm_4.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_5
            // 
            this.cartIm_5.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_5.Location = new System.Drawing.Point(658, 12);
            this.cartIm_5.Name = "cartIm_5";
            this.cartIm_5.Size = new System.Drawing.Size(200, 200);
            this.cartIm_5.TabIndex = 7;
            this.cartIm_5.TabStop = false;
            this.cartIm_5.Visible = false;
            this.cartIm_5.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_6
            // 
            this.cartIm_6.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_6.Location = new System.Drawing.Point(658, 12);
            this.cartIm_6.Name = "cartIm_6";
            this.cartIm_6.Size = new System.Drawing.Size(200, 200);
            this.cartIm_6.TabIndex = 7;
            this.cartIm_6.TabStop = false;
            this.cartIm_6.Visible = false;
            this.cartIm_6.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_7
            // 
            this.cartIm_7.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_7.Location = new System.Drawing.Point(658, 11);
            this.cartIm_7.Name = "cartIm_7";
            this.cartIm_7.Size = new System.Drawing.Size(200, 200);
            this.cartIm_7.TabIndex = 7;
            this.cartIm_7.TabStop = false;
            this.cartIm_7.Visible = false;
            this.cartIm_7.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_8
            // 
            this.cartIm_8.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_8.Location = new System.Drawing.Point(658, 12);
            this.cartIm_8.Name = "cartIm_8";
            this.cartIm_8.Size = new System.Drawing.Size(200, 200);
            this.cartIm_8.TabIndex = 7;
            this.cartIm_8.TabStop = false;
            this.cartIm_8.Visible = false;
            this.cartIm_8.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // Print
            // 
            this.Print.BackColor = System.Drawing.Color.Transparent;
            this.Print.FlatAppearance.BorderSize = 0;
            this.Print.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Print.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Print.ForeColor = System.Drawing.Color.White;
            this.Print.Location = new System.Drawing.Point(658, 219);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(89, 64);
            this.Print.TabIndex = 8;
            this.Print.TabStop = false;
            this.Print.UseVisualStyleBackColor = false;
            this.Print.Visible = false;
            this.Print.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.Print.Click += new System.EventHandler(this.Print_Click);
            // 
            // PhotoAmmLbl
            // 
            this.PhotoAmmLbl.AutoSize = true;
            this.PhotoAmmLbl.BackColor = System.Drawing.Color.Transparent;
            this.PhotoAmmLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PhotoAmmLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.PhotoAmmLbl.Location = new System.Drawing.Point(44, 88);
            this.PhotoAmmLbl.Name = "PhotoAmmLbl";
            this.PhotoAmmLbl.Size = new System.Drawing.Size(398, 61);
            this.PhotoAmmLbl.TabIndex = 9;
            this.PhotoAmmLbl.Text = "PhotoAmmount";
            this.PhotoAmmLbl.Visible = false;
            // 
            // CashAmmLbl
            // 
            this.CashAmmLbl.AutoSize = true;
            this.CashAmmLbl.BackColor = System.Drawing.Color.Transparent;
            this.CashAmmLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 26F, System.Drawing.FontStyle.Bold);
            this.CashAmmLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.CashAmmLbl.Location = new System.Drawing.Point(658, 323);
            this.CashAmmLbl.Name = "CashAmmLbl";
            this.CashAmmLbl.Size = new System.Drawing.Size(175, 44);
            this.CashAmmLbl.TabIndex = 9;
            this.CashAmmLbl.Text = "CashPaid";
            this.CashAmmLbl.Visible = false;
            // 
            // CashNeeded
            // 
            this.CashNeeded.AutoSize = true;
            this.CashNeeded.BackColor = System.Drawing.Color.Transparent;
            this.CashNeeded.Font = new System.Drawing.Font("PF Centro Slab Pro", 26F, System.Drawing.FontStyle.Bold);
            this.CashNeeded.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.CashNeeded.Location = new System.Drawing.Point(658, 367);
            this.CashNeeded.Name = "CashNeeded";
            this.CashNeeded.Size = new System.Drawing.Size(228, 44);
            this.CashNeeded.TabIndex = 9;
            this.CashNeeded.Text = "CashNeeded";
            this.CashNeeded.Visible = false;
            // 
            // PrintBack
            // 
            this.PrintBack.BackColor = System.Drawing.Color.Transparent;
            this.PrintBack.FlatAppearance.BorderSize = 0;
            this.PrintBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PrintBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PrintBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PrintBack.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintBack.ForeColor = System.Drawing.Color.White;
            this.PrintBack.Location = new System.Drawing.Point(674, 246);
            this.PrintBack.Name = "PrintBack";
            this.PrintBack.Size = new System.Drawing.Size(75, 23);
            this.PrintBack.TabIndex = 10;
            this.PrintBack.TabStop = false;
            this.PrintBack.UseVisualStyleBackColor = false;
            this.PrintBack.Visible = false;
            this.PrintBack.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.PrintBack.Click += new System.EventHandler(this.PrintBack_Click);
            // 
            // RstBttn
            // 
            this.RstBttn.BackColor = System.Drawing.Color.Transparent;
            this.RstBttn.FlatAppearance.BorderSize = 0;
            this.RstBttn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.RstBttn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RstBttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RstBttn.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RstBttn.ForeColor = System.Drawing.Color.White;
            this.RstBttn.Location = new System.Drawing.Point(811, 267);
            this.RstBttn.Name = "RstBttn";
            this.RstBttn.Size = new System.Drawing.Size(75, 23);
            this.RstBttn.TabIndex = 11;
            this.RstBttn.TabStop = false;
            this.RstBttn.UseVisualStyleBackColor = false;
            this.RstBttn.Visible = false;
            this.RstBttn.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.RstBttn.Click += new System.EventHandler(this.RstBttn_Click);
            // 
            // Next
            // 
            this.Next.BackColor = System.Drawing.Color.Transparent;
            this.Next.FlatAppearance.BorderSize = 0;
            this.Next.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Next.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.Next.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Next.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next.ForeColor = System.Drawing.Color.White;
            this.Next.Location = new System.Drawing.Point(81, 555);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(75, 23);
            this.Next.TabIndex = 13;
            this.Next.TabStop = false;
            this.Next.UseVisualStyleBackColor = false;
            this.Next.Visible = false;
            this.Next.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Prev
            // 
            this.Prev.BackColor = System.Drawing.Color.Transparent;
            this.Prev.FlatAppearance.BorderSize = 0;
            this.Prev.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Prev.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.Prev.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Prev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Prev.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prev.ForeColor = System.Drawing.Color.White;
            this.Prev.Location = new System.Drawing.Point(81, 519);
            this.Prev.Name = "Prev";
            this.Prev.Size = new System.Drawing.Size(75, 23);
            this.Prev.TabIndex = 14;
            this.Prev.TabStop = false;
            this.Prev.UseVisualStyleBackColor = false;
            this.Prev.Visible = false;
            this.Prev.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            // 
            // SearchBack
            // 
            this.SearchBack.BackColor = System.Drawing.Color.Transparent;
            this.SearchBack.FlatAppearance.BorderSize = 0;
            this.SearchBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SearchBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SearchBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchBack.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBack.ForeColor = System.Drawing.Color.DimGray;
            this.SearchBack.Location = new System.Drawing.Point(730, 237);
            this.SearchBack.Name = "SearchBack";
            this.SearchBack.Size = new System.Drawing.Size(75, 23);
            this.SearchBack.TabIndex = 15;
            this.SearchBack.TabStop = false;
            this.SearchBack.Text = "Искать ещё";
            this.SearchBack.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SearchBack.UseVisualStyleBackColor = false;
            this.SearchBack.Visible = false;
            this.SearchBack.Click += new System.EventHandler(this.SearchBack_Click);
            // 
            // FiltersBack
            // 
            this.FiltersBack.BackColor = System.Drawing.Color.Transparent;
            this.FiltersBack.FlatAppearance.BorderSize = 0;
            this.FiltersBack.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.FiltersBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FiltersBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.FiltersBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FiltersBack.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FiltersBack.ForeColor = System.Drawing.Color.White;
            this.FiltersBack.Location = new System.Drawing.Point(856, 267);
            this.FiltersBack.Name = "FiltersBack";
            this.FiltersBack.Size = new System.Drawing.Size(75, 23);
            this.FiltersBack.TabIndex = 16;
            this.FiltersBack.TabStop = false;
            this.FiltersBack.UseVisualStyleBackColor = false;
            this.FiltersBack.Visible = false;
            // 
            // FiltersOk
            // 
            this.FiltersOk.BackColor = System.Drawing.Color.Transparent;
            this.FiltersOk.FlatAppearance.BorderSize = 0;
            this.FiltersOk.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.FiltersOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FiltersOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.FiltersOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FiltersOk.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FiltersOk.ForeColor = System.Drawing.Color.White;
            this.FiltersOk.Location = new System.Drawing.Point(775, 267);
            this.FiltersOk.Name = "FiltersOk";
            this.FiltersOk.Size = new System.Drawing.Size(75, 23);
            this.FiltersOk.TabIndex = 16;
            this.FiltersOk.TabStop = false;
            this.FiltersOk.UseVisualStyleBackColor = false;
            this.FiltersOk.Visible = false;
            this.FiltersOk.Click += new System.EventHandler(this.FiltersOk_Click);
            // 
            // FiltersNext
            // 
            this.FiltersNext.BackColor = System.Drawing.Color.Transparent;
            this.FiltersNext.FlatAppearance.BorderSize = 0;
            this.FiltersNext.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.FiltersNext.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FiltersNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.FiltersNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FiltersNext.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FiltersNext.ForeColor = System.Drawing.Color.White;
            this.FiltersNext.Location = new System.Drawing.Point(775, 238);
            this.FiltersNext.Name = "FiltersNext";
            this.FiltersNext.Size = new System.Drawing.Size(75, 23);
            this.FiltersNext.TabIndex = 16;
            this.FiltersNext.TabStop = false;
            this.FiltersNext.UseVisualStyleBackColor = false;
            this.FiltersNext.Visible = false;
            this.FiltersNext.Click += new System.EventHandler(this.FiltersNext_Click);
            // 
            // FiltersPrev
            // 
            this.FiltersPrev.BackColor = System.Drawing.Color.Transparent;
            this.FiltersPrev.FlatAppearance.BorderSize = 0;
            this.FiltersPrev.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.FiltersPrev.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FiltersPrev.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.FiltersPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FiltersPrev.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FiltersPrev.ForeColor = System.Drawing.Color.White;
            this.FiltersPrev.Location = new System.Drawing.Point(856, 238);
            this.FiltersPrev.Name = "FiltersPrev";
            this.FiltersPrev.Size = new System.Drawing.Size(75, 23);
            this.FiltersPrev.TabIndex = 16;
            this.FiltersPrev.TabStop = false;
            this.FiltersPrev.UseVisualStyleBackColor = false;
            this.FiltersPrev.Visible = false;
            this.FiltersPrev.Click += new System.EventHandler(this.FiltersPrev_Click);
            // 
            // filterPic1
            // 
            this.filterPic1.BackColor = System.Drawing.Color.Transparent;
            this.filterPic1.Location = new System.Drawing.Point(55, 641);
            this.filterPic1.Name = "filterPic1";
            this.filterPic1.Size = new System.Drawing.Size(200, 200);
            this.filterPic1.TabIndex = 17;
            this.filterPic1.TabStop = false;
            this.filterPic1.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic2
            // 
            this.filterPic2.BackColor = System.Drawing.Color.Transparent;
            this.filterPic2.Location = new System.Drawing.Point(55, 641);
            this.filterPic2.Name = "filterPic2";
            this.filterPic2.Size = new System.Drawing.Size(200, 200);
            this.filterPic2.TabIndex = 17;
            this.filterPic2.TabStop = false;
            this.filterPic2.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic3
            // 
            this.filterPic3.BackColor = System.Drawing.Color.Transparent;
            this.filterPic3.Location = new System.Drawing.Point(55, 641);
            this.filterPic3.Name = "filterPic3";
            this.filterPic3.Size = new System.Drawing.Size(200, 200);
            this.filterPic3.TabIndex = 17;
            this.filterPic3.TabStop = false;
            this.filterPic3.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // InfoBttn
            // 
            this.InfoBttn.BackColor = System.Drawing.Color.Transparent;
            this.InfoBttn.FlatAppearance.BorderSize = 0;
            this.InfoBttn.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.InfoBttn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InfoBttn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InfoBttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InfoBttn.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoBttn.ForeColor = System.Drawing.Color.White;
            this.InfoBttn.Location = new System.Drawing.Point(584, 651);
            this.InfoBttn.Name = "InfoBttn";
            this.InfoBttn.Size = new System.Drawing.Size(75, 23);
            this.InfoBttn.TabIndex = 18;
            this.InfoBttn.TabStop = false;
            this.InfoBttn.UseVisualStyleBackColor = false;
            this.InfoBttn.Visible = false;
            this.InfoBttn.Click += new System.EventHandler(this.InfoBttn_Click);
            // 
            // InfoClose
            // 
            this.InfoClose.BackColor = System.Drawing.Color.Transparent;
            this.InfoClose.FlatAppearance.BorderSize = 0;
            this.InfoClose.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.InfoClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InfoClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InfoClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InfoClose.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoClose.ForeColor = System.Drawing.Color.White;
            this.InfoClose.Location = new System.Drawing.Point(584, 689);
            this.InfoClose.Name = "InfoClose";
            this.InfoClose.Size = new System.Drawing.Size(75, 23);
            this.InfoClose.TabIndex = 19;
            this.InfoClose.TabStop = false;
            this.InfoClose.UseVisualStyleBackColor = false;
            this.InfoClose.Visible = false;
            this.InfoClose.Click += new System.EventHandler(this.InfoClose_Click);
            // 
            // cartIm_9
            // 
            this.cartIm_9.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_9.Location = new System.Drawing.Point(658, 13);
            this.cartIm_9.Name = "cartIm_9";
            this.cartIm_9.Size = new System.Drawing.Size(200, 200);
            this.cartIm_9.TabIndex = 7;
            this.cartIm_9.TabStop = false;
            this.cartIm_9.Visible = false;
            this.cartIm_9.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_10
            // 
            this.cartIm_10.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_10.Location = new System.Drawing.Point(658, 13);
            this.cartIm_10.Name = "cartIm_10";
            this.cartIm_10.Size = new System.Drawing.Size(200, 200);
            this.cartIm_10.TabIndex = 7;
            this.cartIm_10.TabStop = false;
            this.cartIm_10.Visible = false;
            this.cartIm_10.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // filterPic4
            // 
            this.filterPic4.BackColor = System.Drawing.Color.Transparent;
            this.filterPic4.Location = new System.Drawing.Point(55, 641);
            this.filterPic4.Name = "filterPic4";
            this.filterPic4.Size = new System.Drawing.Size(200, 200);
            this.filterPic4.TabIndex = 17;
            this.filterPic4.TabStop = false;
            this.filterPic4.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // slfyTimer
            // 
            this.slfyTimer.Interval = 1000;
            this.slfyTimer.Tick += new System.EventHandler(this.slfyTimer_Tick);
            // 
            // TimerChecker
            // 
            this.TimerChecker.BackColor = System.Drawing.Color.Transparent;
            this.TimerChecker.Location = new System.Drawing.Point(830, 237);
            this.TimerChecker.Name = "TimerChecker";
            this.TimerChecker.Size = new System.Drawing.Size(100, 100);
            this.TimerChecker.TabIndex = 22;
            this.TimerChecker.TabStop = false;
            this.TimerChecker.Visible = false;
            this.TimerChecker.Click += new System.EventHandler(this.TimerChecker_Click);
            // 
            // TimerTime
            // 
            this.TimerTime.AutoSize = true;
            this.TimerTime.BackColor = System.Drawing.Color.Transparent;
            this.TimerTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimerTime.ForeColor = System.Drawing.Color.White;
            this.TimerTime.Location = new System.Drawing.Point(884, 341);
            this.TimerTime.Name = "TimerTime";
            this.TimerTime.Size = new System.Drawing.Size(0, 51);
            this.TimerTime.TabIndex = 23;
            this.TimerTime.Visible = false;
            // 
            // AccSearch
            // 
            this.AccSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AccSearch.Font = new System.Drawing.Font("PF Centro Slab Pro", 40F);
            this.AccSearch.Location = new System.Drawing.Point(504, 59);
            this.AccSearch.Name = "AccSearch";
            this.AccSearch.Size = new System.Drawing.Size(75, 66);
            this.AccSearch.TabIndex = 25;
            this.AccSearch.TabStop = false;
            this.AccSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AccSearch.Visible = false;
            this.AccSearch.TextChanged += new System.EventHandler(this.AccSearch_TextChanged);
            this.AccSearch.Enter += new System.EventHandler(this.AccSearch_Enter);
            // 
            // BalLbl
            // 
            this.BalLbl.AutoSize = true;
            this.BalLbl.BackColor = System.Drawing.Color.Transparent;
            this.BalLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 29F);
            this.BalLbl.Location = new System.Drawing.Point(788, 30);
            this.BalLbl.Name = "BalLbl";
            this.BalLbl.Size = new System.Drawing.Size(234, 48);
            this.BalLbl.TabIndex = 28;
            this.BalLbl.Text = "Ваш баланс:";
            this.BalLbl.Visible = false;
            // 
            // CashPaid
            // 
            this.CashPaid.AutoSize = true;
            this.CashPaid.BackColor = System.Drawing.Color.Transparent;
            this.CashPaid.Font = new System.Drawing.Font("PF Centro Slab Pro Medium", 29F, System.Drawing.FontStyle.Bold);
            this.CashPaid.Location = new System.Drawing.Point(802, 80);
            this.CashPaid.Name = "CashPaid";
            this.CashPaid.Size = new System.Drawing.Size(193, 49);
            this.CashPaid.TabIndex = 29;
            this.CashPaid.Text = "0 рублей";
            this.CashPaid.Visible = false;
            this.CashPaid.TextChanged += new System.EventHandler(this.CashPaid_TextChanged);
            // 
            // OrderDel
            // 
            this.OrderDel.BackColor = System.Drawing.Color.Transparent;
            this.OrderDel.FlatAppearance.BorderSize = 0;
            this.OrderDel.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.OrderDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.OrderDel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.OrderDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OrderDel.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.OrderDel.ForeColor = System.Drawing.Color.White;
            this.OrderDel.Location = new System.Drawing.Point(55, 600);
            this.OrderDel.Name = "OrderDel";
            this.OrderDel.Size = new System.Drawing.Size(100, 28);
            this.OrderDel.TabIndex = 30;
            this.OrderDel.TabStop = false;
            this.OrderDel.UseVisualStyleBackColor = false;
            this.OrderDel.Visible = false;
            this.OrderDel.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.OrderDel.Click += new System.EventHandler(this.OrderDel_Click);
            // 
            // InputBox
            // 
            this.InputBox.AcceptsTab = true;
            this.InputBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InputBox.Font = new System.Drawing.Font("PF Centro Slab Pro", 21F);
            this.InputBox.Location = new System.Drawing.Point(811, 791);
            this.InputBox.Name = "InputBox";
            this.InputBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.InputBox.Size = new System.Drawing.Size(110, 75);
            this.InputBox.TabIndex = 31;
            this.InputBox.Text = "";
            this.InputBox.Visible = false;
            this.InputBox.TextChanged += new System.EventHandler(this.InputBox_TextChanged);
            this.InputBox.Enter += new System.EventHandler(this.AccSearch_Enter);
            // 
            // AddText
            // 
            this.AddText.BackColor = System.Drawing.Color.Transparent;
            this.AddText.FlatAppearance.BorderSize = 0;
            this.AddText.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.AddText.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.AddText.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.AddText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddText.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.AddText.ForeColor = System.Drawing.Color.White;
            this.AddText.Location = new System.Drawing.Point(687, 828);
            this.AddText.Name = "AddText";
            this.AddText.Size = new System.Drawing.Size(100, 28);
            this.AddText.TabIndex = 30;
            this.AddText.TabStop = false;
            this.AddText.UseVisualStyleBackColor = false;
            this.AddText.Visible = false;
            this.AddText.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.AddText.Click += new System.EventHandler(this.AddText_Click);
            // 
            // TurnRight
            // 
            this.TurnRight.BackColor = System.Drawing.Color.Transparent;
            this.TurnRight.FlatAppearance.BorderSize = 0;
            this.TurnRight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.TurnRight.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TurnRight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TurnRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TurnRight.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.TurnRight.ForeColor = System.Drawing.Color.White;
            this.TurnRight.Location = new System.Drawing.Point(480, 828);
            this.TurnRight.Name = "TurnRight";
            this.TurnRight.Size = new System.Drawing.Size(100, 28);
            this.TurnRight.TabIndex = 30;
            this.TurnRight.TabStop = false;
            this.TurnRight.UseVisualStyleBackColor = false;
            this.TurnRight.Visible = false;
            this.TurnRight.Click += new System.EventHandler(this.TurnRight_Click);
            // 
            // TurnLeft
            // 
            this.TurnLeft.BackColor = System.Drawing.Color.Transparent;
            this.TurnLeft.FlatAppearance.BorderSize = 0;
            this.TurnLeft.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.TurnLeft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TurnLeft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TurnLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TurnLeft.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.TurnLeft.ForeColor = System.Drawing.Color.White;
            this.TurnLeft.Location = new System.Drawing.Point(581, 828);
            this.TurnLeft.Name = "TurnLeft";
            this.TurnLeft.Size = new System.Drawing.Size(100, 28);
            this.TurnLeft.TabIndex = 30;
            this.TurnLeft.TabStop = false;
            this.TurnLeft.UseVisualStyleBackColor = false;
            this.TurnLeft.Visible = false;
            this.TurnLeft.Click += new System.EventHandler(this.TurnLeft_Click);
            // 
            // TextOk
            // 
            this.TextOk.BackColor = System.Drawing.Color.Transparent;
            this.TextOk.FlatAppearance.BorderSize = 0;
            this.TextOk.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.TextOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TextOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TextOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TextOk.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.TextOk.ForeColor = System.Drawing.Color.White;
            this.TextOk.Location = new System.Drawing.Point(584, 850);
            this.TextOk.Name = "TextOk";
            this.TextOk.Size = new System.Drawing.Size(100, 28);
            this.TextOk.TabIndex = 30;
            this.TextOk.TabStop = false;
            this.TextOk.UseVisualStyleBackColor = false;
            this.TextOk.Visible = false;
            this.TextOk.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.TextOk.Click += new System.EventHandler(this.TextOk_Click);
            // 
            // PrintPageLbl
            // 
            this.PrintPageLbl.AutoSize = true;
            this.PrintPageLbl.BackColor = System.Drawing.Color.Transparent;
            this.PrintPageLbl.Font = new System.Drawing.Font("PF Centro Slab Pro Medium", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintPageLbl.ForeColor = System.Drawing.Color.Red;
            this.PrintPageLbl.Location = new System.Drawing.Point(683, 252);
            this.PrintPageLbl.Name = "PrintPageLbl";
            this.PrintPageLbl.Size = new System.Drawing.Size(0, 60);
            this.PrintPageLbl.TabIndex = 33;
            this.PrintPageLbl.Visible = false;
            // 
            // printTimer
            // 
            this.printTimer.Interval = 1000;
            this.printTimer.Tick += new System.EventHandler(this.printTimer_Tick);
            // 
            // searchTimer
            // 
            this.searchTimer.Tick += new System.EventHandler(this.searchTimer_Tick);
            // 
            // Lang1
            // 
            this.Lang1.BackColor = System.Drawing.Color.Transparent;
            this.Lang1.FlatAppearance.BorderSize = 0;
            this.Lang1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Lang1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Lang1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Lang1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lang1.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.Lang1.ForeColor = System.Drawing.Color.White;
            this.Lang1.Location = new System.Drawing.Point(786, 641);
            this.Lang1.Name = "Lang1";
            this.Lang1.Size = new System.Drawing.Size(100, 28);
            this.Lang1.TabIndex = 34;
            this.Lang1.TabStop = false;
            this.Lang1.UseVisualStyleBackColor = false;
            this.Lang1.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.Lang1.Click += new System.EventHandler(this.ChangeLang_Click);
            // 
            // Lang2
            // 
            this.Lang2.BackColor = System.Drawing.Color.Transparent;
            this.Lang2.FlatAppearance.BorderSize = 0;
            this.Lang2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Lang2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Lang2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Lang2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lang2.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.Lang2.ForeColor = System.Drawing.Color.White;
            this.Lang2.Location = new System.Drawing.Point(649, 648);
            this.Lang2.Name = "Lang2";
            this.Lang2.Size = new System.Drawing.Size(100, 28);
            this.Lang2.TabIndex = 34;
            this.Lang2.TabStop = false;
            this.Lang2.UseVisualStyleBackColor = false;
            this.Lang2.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.Lang2.Click += new System.EventHandler(this.ChangeLang_Click);
            // 
            // PrintEven
            // 
            this.PrintEven.AutoSize = true;
            this.PrintEven.BackColor = System.Drawing.Color.Transparent;
            this.PrintEven.Font = new System.Drawing.Font("PF Centro Slab Pro", 42F, System.Drawing.FontStyle.Bold);
            this.PrintEven.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.PrintEven.Location = new System.Drawing.Point(493, 805);
            this.PrintEven.Name = "PrintEven";
            this.PrintEven.Size = new System.Drawing.Size(773, 71);
            this.PrintEven.TabIndex = 36;
            this.PrintEven.Text = "Только чётное количество";
            this.PrintEven.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PrintEven.Visible = false;
            // 
            // VideoPlay
            // 
            this.VideoPlay.Location = new System.Drawing.Point(12, 862);
            this.VideoPlay.Name = "VideoPlay";
            this.VideoPlay.Size = new System.Drawing.Size(1080, 608);
            this.VideoPlay.TabIndex = 38;
            this.VideoPlay.Visible = false;
            // 
            // InactivityTest
            // 
            this.InactivityTest.BackColor = System.Drawing.Color.Transparent;
            this.InactivityTest.FlatAppearance.BorderSize = 0;
            this.InactivityTest.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.InactivityTest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InactivityTest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InactivityTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InactivityTest.Font = new System.Drawing.Font("PF Centro Slab Pro", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InactivityTest.ForeColor = System.Drawing.Color.White;
            this.InactivityTest.Location = new System.Drawing.Point(522, 514);
            this.InactivityTest.Name = "InactivityTest";
            this.InactivityTest.Size = new System.Drawing.Size(100, 28);
            this.InactivityTest.TabIndex = 30;
            this.InactivityTest.TabStop = false;
            this.InactivityTest.Text = "Режим ожидания";
            this.InactivityTest.UseVisualStyleBackColor = false;
            this.InactivityTest.Visible = false;
            this.InactivityTest.EnabledChanged += new System.EventHandler(this.Button_EnabledChanged);
            this.InactivityTest.Click += new System.EventHandler(this.InactivityTest_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // loadingPic
            // 
            this.loadingPic.Location = new System.Drawing.Point(874, 217);
            this.loadingPic.Name = "loadingPic";
            this.loadingPic.Size = new System.Drawing.Size(8, 8);
            this.loadingPic.TabIndex = 42;
            this.loadingPic.TabStop = false;
            // 
            // WaitModeTmr
            // 
            this.WaitModeTmr.Tick += new System.EventHandler(this.WaitModeTmr_Tick);
            // 
            // EditCancel
            // 
            this.EditCancel.BackColor = System.Drawing.Color.Transparent;
            this.EditCancel.FlatAppearance.BorderSize = 0;
            this.EditCancel.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.EditCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.EditCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.EditCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EditCancel.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.EditCancel.ForeColor = System.Drawing.Color.White;
            this.EditCancel.Location = new System.Drawing.Point(480, 794);
            this.EditCancel.Name = "EditCancel";
            this.EditCancel.Size = new System.Drawing.Size(100, 28);
            this.EditCancel.TabIndex = 30;
            this.EditCancel.TabStop = false;
            this.EditCancel.UseVisualStyleBackColor = false;
            this.EditCancel.Visible = false;
            this.EditCancel.Click += new System.EventHandler(this.EditCancel_Click);
            // 
            // EditOk
            // 
            this.EditOk.BackColor = System.Drawing.Color.Transparent;
            this.EditOk.FlatAppearance.BorderSize = 0;
            this.EditOk.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.EditOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.EditOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.EditOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EditOk.Font = new System.Drawing.Font("PF Centro Slab Pro", 32.25F);
            this.EditOk.ForeColor = System.Drawing.Color.White;
            this.EditOk.Location = new System.Drawing.Point(479, 805);
            this.EditOk.Name = "EditOk";
            this.EditOk.Size = new System.Drawing.Size(100, 28);
            this.EditOk.TabIndex = 30;
            this.EditOk.TabStop = false;
            this.EditOk.UseVisualStyleBackColor = false;
            this.EditOk.Visible = false;
            this.EditOk.Click += new System.EventHandler(this.EditOk_Click);
            // 
            // rtnMnPnl
            // 
            this.rtnMnPnl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(135)))), ((int)(((byte)(135)))));
            this.rtnMnPnl.Location = new System.Drawing.Point(903, 353);
            this.rtnMnPnl.Name = "rtnMnPnl";
            this.rtnMnPnl.Size = new System.Drawing.Size(1080, 120);
            this.rtnMnPnl.TabIndex = 48;
            this.rtnMnPnl.Visible = false;
            // 
            // MainScreenLbl
            // 
            this.MainScreenLbl.AutoSize = true;
            this.MainScreenLbl.BackColor = System.Drawing.Color.Transparent;
            this.MainScreenLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 80.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainScreenLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.MainScreenLbl.Location = new System.Drawing.Point(660, 225);
            this.MainScreenLbl.Name = "MainScreenLbl";
            this.MainScreenLbl.Size = new System.Drawing.Size(0, 136);
            this.MainScreenLbl.TabIndex = 50;
            this.MainScreenLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PrintPhoneLbl
            // 
            this.PrintPhoneLbl.AutoSize = true;
            this.PrintPhoneLbl.BackColor = System.Drawing.Color.Transparent;
            this.PrintPhoneLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintPhoneLbl.ForeColor = System.Drawing.Color.White;
            this.PrintPhoneLbl.Location = new System.Drawing.Point(668, 233);
            this.PrintPhoneLbl.Name = "PrintPhoneLbl";
            this.PrintPhoneLbl.Size = new System.Drawing.Size(0, 40);
            this.PrintPhoneLbl.TabIndex = 52;
            this.PrintPhoneLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MkSelfieLbl
            // 
            this.MkSelfieLbl.AutoSize = true;
            this.MkSelfieLbl.BackColor = System.Drawing.Color.Transparent;
            this.MkSelfieLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MkSelfieLbl.ForeColor = System.Drawing.Color.White;
            this.MkSelfieLbl.Location = new System.Drawing.Point(671, 272);
            this.MkSelfieLbl.Name = "MkSelfieLbl";
            this.MkSelfieLbl.Size = new System.Drawing.Size(0, 40);
            this.MkSelfieLbl.TabIndex = 52;
            this.MkSelfieLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VideoFeedbackLbl
            // 
            this.VideoFeedbackLbl.AutoSize = true;
            this.VideoFeedbackLbl.BackColor = System.Drawing.Color.Transparent;
            this.VideoFeedbackLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VideoFeedbackLbl.ForeColor = System.Drawing.Color.White;
            this.VideoFeedbackLbl.Location = new System.Drawing.Point(727, 233);
            this.VideoFeedbackLbl.Name = "VideoFeedbackLbl";
            this.VideoFeedbackLbl.Size = new System.Drawing.Size(0, 40);
            this.VideoFeedbackLbl.TabIndex = 52;
            this.VideoFeedbackLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CartLbl
            // 
            this.CartLbl.AutoSize = true;
            this.CartLbl.BackColor = System.Drawing.Color.Transparent;
            this.CartLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CartLbl.Location = new System.Drawing.Point(886, 164);
            this.CartLbl.Name = "CartLbl";
            this.CartLbl.Size = new System.Drawing.Size(0, 82);
            this.CartLbl.TabIndex = 54;
            this.CartLbl.Visible = false;
            // 
            // OrderedLbl
            // 
            this.OrderedLbl.AutoSize = true;
            this.OrderedLbl.BackColor = System.Drawing.Color.Transparent;
            this.OrderedLbl.Font = new System.Drawing.Font("PF Centro Slab Pro Medium", 30F, System.Drawing.FontStyle.Bold);
            this.OrderedLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.OrderedLbl.Location = new System.Drawing.Point(46, 38);
            this.OrderedLbl.Name = "OrderedLbl";
            this.OrderedLbl.Size = new System.Drawing.Size(132, 50);
            this.OrderedLbl.TabIndex = 56;
            this.OrderedLbl.Text = "Order";
            this.OrderedLbl.Visible = false;
            // 
            // NeedCashLbl
            // 
            this.NeedCashLbl.AutoSize = true;
            this.NeedCashLbl.BackColor = System.Drawing.Color.Transparent;
            this.NeedCashLbl.Font = new System.Drawing.Font("PF Centro Slab Pro Medium", 26F);
            this.NeedCashLbl.Location = new System.Drawing.Point(47, 367);
            this.NeedCashLbl.Name = "NeedCashLbl";
            this.NeedCashLbl.Size = new System.Drawing.Size(111, 44);
            this.NeedCashLbl.TabIndex = 56;
            this.NeedCashLbl.Text = "Need:";
            this.NeedCashLbl.Visible = false;
            // 
            // PaidCashLbl
            // 
            this.PaidCashLbl.AutoSize = true;
            this.PaidCashLbl.BackColor = System.Drawing.Color.Transparent;
            this.PaidCashLbl.Font = new System.Drawing.Font("PF Centro Slab Pro Medium", 26F);
            this.PaidCashLbl.Location = new System.Drawing.Point(47, 323);
            this.PaidCashLbl.Name = "PaidCashLbl";
            this.PaidCashLbl.Size = new System.Drawing.Size(98, 44);
            this.PaidCashLbl.TabIndex = 56;
            this.PaidCashLbl.Text = "Paid:";
            this.PaidCashLbl.Visible = false;
            // 
            // PrintingLbl
            // 
            this.PrintingLbl.AutoSize = true;
            this.PrintingLbl.BackColor = System.Drawing.Color.Transparent;
            this.PrintingLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 60F, System.Drawing.FontStyle.Bold);
            this.PrintingLbl.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.PrintingLbl.Location = new System.Drawing.Point(902, 191);
            this.PrintingLbl.Name = "PrintingLbl";
            this.PrintingLbl.Size = new System.Drawing.Size(0, 101);
            this.PrintingLbl.TabIndex = 9;
            this.PrintingLbl.Visible = false;
            // 
            // AcceptedCashLbl
            // 
            this.AcceptedCashLbl.AutoSize = true;
            this.AcceptedCashLbl.BackColor = System.Drawing.Color.Transparent;
            this.AcceptedCashLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 16F);
            this.AcceptedCashLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.AcceptedCashLbl.Location = new System.Drawing.Point(432, 38);
            this.AcceptedCashLbl.Name = "AcceptedCashLbl";
            this.AcceptedCashLbl.Size = new System.Drawing.Size(94, 27);
            this.AcceptedCashLbl.TabIndex = 9;
            this.AcceptedCashLbl.Text = "CashAcc";
            this.AcceptedCashLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AcceptedCashLbl.Visible = false;
            // 
            // NoCoinsLbl
            // 
            this.NoCoinsLbl.AutoSize = true;
            this.NoCoinsLbl.BackColor = System.Drawing.Color.Transparent;
            this.NoCoinsLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 16F);
            this.NoCoinsLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.NoCoinsLbl.Location = new System.Drawing.Point(432, 99);
            this.NoCoinsLbl.Name = "NoCoinsLbl";
            this.NoCoinsLbl.Size = new System.Drawing.Size(92, 27);
            this.NoCoinsLbl.TabIndex = 9;
            this.NoCoinsLbl.Text = "NoCoins";
            this.NoCoinsLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.NoCoinsLbl.Visible = false;
            // 
            // SupportLbl
            // 
            this.SupportLbl.AutoSize = true;
            this.SupportLbl.BackColor = System.Drawing.Color.Transparent;
            this.SupportLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 16F);
            this.SupportLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.SupportLbl.Location = new System.Drawing.Point(432, 155);
            this.SupportLbl.Name = "SupportLbl";
            this.SupportLbl.Size = new System.Drawing.Size(88, 27);
            this.SupportLbl.TabIndex = 9;
            this.SupportLbl.Text = "Support";
            this.SupportLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SupportLbl.Visible = false;
            // 
            // ThanksLbl
            // 
            this.ThanksLbl.AutoSize = true;
            this.ThanksLbl.BackColor = System.Drawing.Color.Transparent;
            this.ThanksLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ThanksLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.ThanksLbl.Location = new System.Drawing.Point(643, 30);
            this.ThanksLbl.Name = "ThanksLbl";
            this.ThanksLbl.Size = new System.Drawing.Size(199, 61);
            this.ThanksLbl.TabIndex = 9;
            this.ThanksLbl.Text = "Thanks";
            this.ThanksLbl.Visible = false;
            // 
            // AutoMainLbl
            // 
            this.AutoMainLbl.AutoSize = true;
            this.AutoMainLbl.BackColor = System.Drawing.Color.Transparent;
            this.AutoMainLbl.Font = new System.Drawing.Font("PF Centro Slab Pro Medium", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoMainLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.AutoMainLbl.Location = new System.Drawing.Point(639, 97);
            this.AutoMainLbl.Name = "AutoMainLbl";
            this.AutoMainLbl.Size = new System.Drawing.Size(178, 53);
            this.AutoMainLbl.TabIndex = 9;
            this.AutoMainLbl.Text = "ToMain";
            this.AutoMainLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AutoMainLbl.Visible = false;
            // 
            // DoNotForgetLbl
            // 
            this.DoNotForgetLbl.AutoSize = true;
            this.DoNotForgetLbl.BackColor = System.Drawing.Color.Transparent;
            this.DoNotForgetLbl.Font = new System.Drawing.Font("PF Centro Slab Pro", 42F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.DoNotForgetLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.DoNotForgetLbl.Location = new System.Drawing.Point(639, 153);
            this.DoNotForgetLbl.Name = "DoNotForgetLbl";
            this.DoNotForgetLbl.Size = new System.Drawing.Size(370, 71);
            this.DoNotForgetLbl.TabIndex = 9;
            this.DoNotForgetLbl.Text = "DoNotForget";
            this.DoNotForgetLbl.Visible = false;
            // 
            // buttonTimer
            // 
            this.buttonTimer.Tick += new System.EventHandler(this.buttonTimer_Tick);
            // 
            // locTimer
            // 
            this.locTimer.Interval = 10000;
            this.locTimer.Tick += new System.EventHandler(this.locTimer_Tick);
            // 
            // ToEditLabel
            // 
            this.ToEditLabel.AutoSize = true;
            this.ToEditLabel.BackColor = System.Drawing.Color.Transparent;
            this.ToEditLabel.Font = new System.Drawing.Font("PF Centro Slab Pro", 36F, System.Drawing.FontStyle.Italic);
            this.ToEditLabel.ForeColor = System.Drawing.Color.Black;
            this.ToEditLabel.Location = new System.Drawing.Point(596, 791);
            this.ToEditLabel.Name = "ToEditLabel";
            this.ToEditLabel.Size = new System.Drawing.Size(162, 60);
            this.ToEditLabel.TabIndex = 58;
            this.ToEditLabel.Text = "ToEdit";
            this.ToEditLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToEditLabel.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 20000;
            this.timer1.Tick += new System.EventHandler(this.Waiting_Tick);
            // 
            // filterPic5
            // 
            this.filterPic5.BackColor = System.Drawing.Color.Transparent;
            this.filterPic5.Location = new System.Drawing.Point(55, 641);
            this.filterPic5.Name = "filterPic5";
            this.filterPic5.Size = new System.Drawing.Size(200, 200);
            this.filterPic5.TabIndex = 60;
            this.filterPic5.TabStop = false;
            this.filterPic5.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic6
            // 
            this.filterPic6.BackColor = System.Drawing.Color.Transparent;
            this.filterPic6.Location = new System.Drawing.Point(55, 641);
            this.filterPic6.Name = "filterPic6";
            this.filterPic6.Size = new System.Drawing.Size(200, 200);
            this.filterPic6.TabIndex = 61;
            this.filterPic6.TabStop = false;
            this.filterPic6.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic7
            // 
            this.filterPic7.BackColor = System.Drawing.Color.Transparent;
            this.filterPic7.Location = new System.Drawing.Point(55, 641);
            this.filterPic7.Name = "filterPic7";
            this.filterPic7.Size = new System.Drawing.Size(200, 200);
            this.filterPic7.TabIndex = 62;
            this.filterPic7.TabStop = false;
            this.filterPic7.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic8
            // 
            this.filterPic8.BackColor = System.Drawing.Color.Transparent;
            this.filterPic8.Location = new System.Drawing.Point(55, 641);
            this.filterPic8.Name = "filterPic8";
            this.filterPic8.Size = new System.Drawing.Size(200, 200);
            this.filterPic8.TabIndex = 63;
            this.filterPic8.TabStop = false;
            this.filterPic8.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic9
            // 
            this.filterPic9.BackColor = System.Drawing.Color.Transparent;
            this.filterPic9.Location = new System.Drawing.Point(55, 641);
            this.filterPic9.Name = "filterPic9";
            this.filterPic9.Size = new System.Drawing.Size(200, 200);
            this.filterPic9.TabIndex = 64;
            this.filterPic9.TabStop = false;
            this.filterPic9.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic10
            // 
            this.filterPic10.BackColor = System.Drawing.Color.Transparent;
            this.filterPic10.Location = new System.Drawing.Point(55, 641);
            this.filterPic10.Name = "filterPic10";
            this.filterPic10.Size = new System.Drawing.Size(200, 200);
            this.filterPic10.TabIndex = 65;
            this.filterPic10.TabStop = false;
            this.filterPic10.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic11
            // 
            this.filterPic11.BackColor = System.Drawing.Color.Transparent;
            this.filterPic11.Location = new System.Drawing.Point(55, 641);
            this.filterPic11.Name = "filterPic11";
            this.filterPic11.Size = new System.Drawing.Size(200, 200);
            this.filterPic11.TabIndex = 66;
            this.filterPic11.TabStop = false;
            this.filterPic11.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // filterPic12
            // 
            this.filterPic12.BackColor = System.Drawing.Color.Transparent;
            this.filterPic12.Location = new System.Drawing.Point(55, 641);
            this.filterPic12.Name = "filterPic12";
            this.filterPic12.Size = new System.Drawing.Size(200, 200);
            this.filterPic12.TabIndex = 67;
            this.filterPic12.TabStop = false;
            this.filterPic12.Click += new System.EventHandler(this.filterPic_Click);
            // 
            // FullName1
            // 
            this.FullName1.AutoSize = true;
            this.FullName1.BackColor = System.Drawing.Color.Transparent;
            this.FullName1.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName1.Location = new System.Drawing.Point(98, 19);
            this.FullName1.Name = "FullName1";
            this.FullName1.Size = new System.Drawing.Size(0, 26);
            this.FullName1.TabIndex = 24;
            this.FullName1.Click += new System.EventHandler(this.User_Click);
            // 
            // User1
            // 
            this.User1.AutoSize = true;
            this.User1.BackColor = System.Drawing.Color.Transparent;
            this.User1.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User1.Location = new System.Drawing.Point(139, 14);
            this.User1.Name = "User1";
            this.User1.Size = new System.Drawing.Size(0, 36);
            this.User1.TabIndex = 24;
            this.User1.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic1
            // 
            this.UserPic1.Location = new System.Drawing.Point(36, 7);
            this.UserPic1.Name = "UserPic1";
            this.UserPic1.Size = new System.Drawing.Size(50, 50);
            this.UserPic1.TabIndex = 26;
            this.UserPic1.TabStop = false;
            this.UserPic1.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser1
            // 
            this.GetUser1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser1.Controls.Add(this.UserPic1);
            this.GetUser1.Controls.Add(this.User1);
            this.GetUser1.Controls.Add(this.FullName1);
            this.GetUser1.Location = new System.Drawing.Point(311, 651);
            this.GetUser1.Name = "GetUser1";
            this.GetUser1.Size = new System.Drawing.Size(200, 71);
            this.GetUser1.TabIndex = 27;
            this.GetUser1.Visible = false;
            this.GetUser1.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName6
            // 
            this.FullName6.AutoSize = true;
            this.FullName6.BackColor = System.Drawing.Color.Transparent;
            this.FullName6.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName6.Location = new System.Drawing.Point(98, 19);
            this.FullName6.Name = "FullName6";
            this.FullName6.Size = new System.Drawing.Size(0, 26);
            this.FullName6.TabIndex = 24;
            this.FullName6.Click += new System.EventHandler(this.User_Click);
            // 
            // User6
            // 
            this.User6.AutoSize = true;
            this.User6.BackColor = System.Drawing.Color.Transparent;
            this.User6.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User6.Location = new System.Drawing.Point(139, 14);
            this.User6.Name = "User6";
            this.User6.Size = new System.Drawing.Size(0, 36);
            this.User6.TabIndex = 24;
            this.User6.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic6
            // 
            this.UserPic6.Location = new System.Drawing.Point(36, 7);
            this.UserPic6.Name = "UserPic6";
            this.UserPic6.Size = new System.Drawing.Size(50, 50);
            this.UserPic6.TabIndex = 26;
            this.UserPic6.TabStop = false;
            this.UserPic6.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser6
            // 
            this.GetUser6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser6.Controls.Add(this.UserPic6);
            this.GetUser6.Controls.Add(this.User6);
            this.GetUser6.Controls.Add(this.FullName6);
            this.GetUser6.Location = new System.Drawing.Point(261, 628);
            this.GetUser6.Name = "GetUser6";
            this.GetUser6.Size = new System.Drawing.Size(200, 71);
            this.GetUser6.TabIndex = 27;
            this.GetUser6.Visible = false;
            this.GetUser6.Click += new System.EventHandler(this.User_Click);
            // 
            // User3
            // 
            this.User3.AutoSize = true;
            this.User3.BackColor = System.Drawing.Color.Transparent;
            this.User3.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User3.Location = new System.Drawing.Point(140, 24);
            this.User3.Name = "User3";
            this.User3.Size = new System.Drawing.Size(0, 36);
            this.User3.TabIndex = 24;
            this.User3.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName3
            // 
            this.FullName3.AutoSize = true;
            this.FullName3.BackColor = System.Drawing.Color.Transparent;
            this.FullName3.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName3.Location = new System.Drawing.Point(119, 16);
            this.FullName3.Name = "FullName3";
            this.FullName3.Size = new System.Drawing.Size(0, 26);
            this.FullName3.TabIndex = 24;
            this.FullName3.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic3
            // 
            this.UserPic3.Location = new System.Drawing.Point(49, 10);
            this.UserPic3.Name = "UserPic3";
            this.UserPic3.Size = new System.Drawing.Size(50, 50);
            this.UserPic3.TabIndex = 26;
            this.UserPic3.TabStop = false;
            this.UserPic3.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser3
            // 
            this.GetUser3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser3.Controls.Add(this.UserPic3);
            this.GetUser3.Controls.Add(this.FullName3);
            this.GetUser3.Controls.Add(this.User3);
            this.GetUser3.Location = new System.Drawing.Point(317, 661);
            this.GetUser3.Name = "GetUser3";
            this.GetUser3.Size = new System.Drawing.Size(200, 71);
            this.GetUser3.TabIndex = 27;
            this.GetUser3.Visible = false;
            this.GetUser3.Click += new System.EventHandler(this.User_Click);
            // 
            // User7
            // 
            this.User7.AutoSize = true;
            this.User7.BackColor = System.Drawing.Color.Transparent;
            this.User7.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User7.Location = new System.Drawing.Point(140, 24);
            this.User7.Name = "User7";
            this.User7.Size = new System.Drawing.Size(0, 36);
            this.User7.TabIndex = 24;
            this.User7.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName7
            // 
            this.FullName7.AutoSize = true;
            this.FullName7.BackColor = System.Drawing.Color.Transparent;
            this.FullName7.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName7.Location = new System.Drawing.Point(119, 16);
            this.FullName7.Name = "FullName7";
            this.FullName7.Size = new System.Drawing.Size(0, 26);
            this.FullName7.TabIndex = 24;
            this.FullName7.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic7
            // 
            this.UserPic7.Location = new System.Drawing.Point(49, 10);
            this.UserPic7.Name = "UserPic7";
            this.UserPic7.Size = new System.Drawing.Size(50, 50);
            this.UserPic7.TabIndex = 26;
            this.UserPic7.TabStop = false;
            this.UserPic7.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser7
            // 
            this.GetUser7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser7.Controls.Add(this.UserPic7);
            this.GetUser7.Controls.Add(this.FullName7);
            this.GetUser7.Controls.Add(this.User7);
            this.GetUser7.Location = new System.Drawing.Point(264, 631);
            this.GetUser7.Name = "GetUser7";
            this.GetUser7.Size = new System.Drawing.Size(200, 71);
            this.GetUser7.TabIndex = 27;
            this.GetUser7.Visible = false;
            this.GetUser7.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic5
            // 
            this.UserPic5.Location = new System.Drawing.Point(35, 10);
            this.UserPic5.Name = "UserPic5";
            this.UserPic5.Size = new System.Drawing.Size(50, 50);
            this.UserPic5.TabIndex = 26;
            this.UserPic5.TabStop = false;
            this.UserPic5.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName5
            // 
            this.FullName5.AutoSize = true;
            this.FullName5.BackColor = System.Drawing.Color.Transparent;
            this.FullName5.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName5.Location = new System.Drawing.Point(110, 16);
            this.FullName5.Name = "FullName5";
            this.FullName5.Size = new System.Drawing.Size(0, 26);
            this.FullName5.TabIndex = 24;
            this.FullName5.Click += new System.EventHandler(this.User_Click);
            // 
            // User5
            // 
            this.User5.AutoSize = true;
            this.User5.BackColor = System.Drawing.Color.Transparent;
            this.User5.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User5.Location = new System.Drawing.Point(3, 0);
            this.User5.Name = "User5";
            this.User5.Size = new System.Drawing.Size(0, 36);
            this.User5.TabIndex = 24;
            this.User5.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser5
            // 
            this.GetUser5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser5.Controls.Add(this.User5);
            this.GetUser5.Controls.Add(this.FullName5);
            this.GetUser5.Controls.Add(this.UserPic5);
            this.GetUser5.Location = new System.Drawing.Point(324, 672);
            this.GetUser5.Name = "GetUser5";
            this.GetUser5.Size = new System.Drawing.Size(200, 71);
            this.GetUser5.TabIndex = 27;
            this.GetUser5.Visible = false;
            this.GetUser5.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic8
            // 
            this.UserPic8.Location = new System.Drawing.Point(35, 10);
            this.UserPic8.Name = "UserPic8";
            this.UserPic8.Size = new System.Drawing.Size(50, 50);
            this.UserPic8.TabIndex = 26;
            this.UserPic8.TabStop = false;
            this.UserPic8.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName8
            // 
            this.FullName8.AutoSize = true;
            this.FullName8.BackColor = System.Drawing.Color.Transparent;
            this.FullName8.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName8.Location = new System.Drawing.Point(110, 16);
            this.FullName8.Name = "FullName8";
            this.FullName8.Size = new System.Drawing.Size(0, 26);
            this.FullName8.TabIndex = 24;
            this.FullName8.Click += new System.EventHandler(this.User_Click);
            // 
            // User8
            // 
            this.User8.AutoSize = true;
            this.User8.BackColor = System.Drawing.Color.Transparent;
            this.User8.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User8.Location = new System.Drawing.Point(3, 0);
            this.User8.Name = "User8";
            this.User8.Size = new System.Drawing.Size(0, 36);
            this.User8.TabIndex = 24;
            this.User8.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser8
            // 
            this.GetUser8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser8.Controls.Add(this.User8);
            this.GetUser8.Controls.Add(this.FullName8);
            this.GetUser8.Controls.Add(this.UserPic8);
            this.GetUser8.Location = new System.Drawing.Point(274, 634);
            this.GetUser8.Name = "GetUser8";
            this.GetUser8.Size = new System.Drawing.Size(200, 71);
            this.GetUser8.TabIndex = 27;
            this.GetUser8.Visible = false;
            this.GetUser8.Click += new System.EventHandler(this.User_Click);
            // 
            // User2
            // 
            this.User2.AutoSize = true;
            this.User2.BackColor = System.Drawing.Color.Transparent;
            this.User2.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User2.Location = new System.Drawing.Point(18, 21);
            this.User2.Name = "User2";
            this.User2.Size = new System.Drawing.Size(0, 36);
            this.User2.TabIndex = 24;
            this.User2.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName2
            // 
            this.FullName2.AutoSize = true;
            this.FullName2.BackColor = System.Drawing.Color.Transparent;
            this.FullName2.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName2.Location = new System.Drawing.Point(169, 26);
            this.FullName2.Name = "FullName2";
            this.FullName2.Size = new System.Drawing.Size(0, 26);
            this.FullName2.TabIndex = 24;
            this.FullName2.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic2
            // 
            this.UserPic2.Location = new System.Drawing.Point(40, 14);
            this.UserPic2.Name = "UserPic2";
            this.UserPic2.Size = new System.Drawing.Size(50, 50);
            this.UserPic2.TabIndex = 26;
            this.UserPic2.TabStop = false;
            this.UserPic2.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser2
            // 
            this.GetUser2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser2.Controls.Add(this.UserPic2);
            this.GetUser2.Controls.Add(this.FullName2);
            this.GetUser2.Controls.Add(this.User2);
            this.GetUser2.Location = new System.Drawing.Point(314, 655);
            this.GetUser2.Name = "GetUser2";
            this.GetUser2.Size = new System.Drawing.Size(200, 71);
            this.GetUser2.TabIndex = 27;
            this.GetUser2.Visible = false;
            this.GetUser2.Click += new System.EventHandler(this.User_Click);
            // 
            // User9
            // 
            this.User9.AutoSize = true;
            this.User9.BackColor = System.Drawing.Color.Transparent;
            this.User9.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User9.Location = new System.Drawing.Point(18, 21);
            this.User9.Name = "User9";
            this.User9.Size = new System.Drawing.Size(0, 36);
            this.User9.TabIndex = 24;
            this.User9.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName9
            // 
            this.FullName9.AutoSize = true;
            this.FullName9.BackColor = System.Drawing.Color.Transparent;
            this.FullName9.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName9.Location = new System.Drawing.Point(169, 26);
            this.FullName9.Name = "FullName9";
            this.FullName9.Size = new System.Drawing.Size(0, 26);
            this.FullName9.TabIndex = 24;
            this.FullName9.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic9
            // 
            this.UserPic9.Location = new System.Drawing.Point(40, 14);
            this.UserPic9.Name = "UserPic9";
            this.UserPic9.Size = new System.Drawing.Size(50, 50);
            this.UserPic9.TabIndex = 26;
            this.UserPic9.TabStop = false;
            this.UserPic9.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser9
            // 
            this.GetUser9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser9.Controls.Add(this.UserPic9);
            this.GetUser9.Controls.Add(this.FullName9);
            this.GetUser9.Controls.Add(this.User9);
            this.GetUser9.Location = new System.Drawing.Point(258, 654);
            this.GetUser9.Name = "GetUser9";
            this.GetUser9.Size = new System.Drawing.Size(200, 71);
            this.GetUser9.TabIndex = 27;
            this.GetUser9.Visible = false;
            this.GetUser9.Click += new System.EventHandler(this.User_Click);
            // 
            // User4
            // 
            this.User4.AutoSize = true;
            this.User4.BackColor = System.Drawing.Color.Transparent;
            this.User4.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User4.Location = new System.Drawing.Point(105, 24);
            this.User4.Name = "User4";
            this.User4.Size = new System.Drawing.Size(0, 36);
            this.User4.TabIndex = 24;
            this.User4.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName4
            // 
            this.FullName4.AutoSize = true;
            this.FullName4.BackColor = System.Drawing.Color.Transparent;
            this.FullName4.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName4.Location = new System.Drawing.Point(99, 19);
            this.FullName4.Name = "FullName4";
            this.FullName4.Size = new System.Drawing.Size(0, 26);
            this.FullName4.TabIndex = 24;
            this.FullName4.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic4
            // 
            this.UserPic4.Location = new System.Drawing.Point(24, 10);
            this.UserPic4.Name = "UserPic4";
            this.UserPic4.Size = new System.Drawing.Size(50, 50);
            this.UserPic4.TabIndex = 26;
            this.UserPic4.TabStop = false;
            this.UserPic4.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser4
            // 
            this.GetUser4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser4.Controls.Add(this.UserPic4);
            this.GetUser4.Controls.Add(this.FullName4);
            this.GetUser4.Controls.Add(this.User4);
            this.GetUser4.Location = new System.Drawing.Point(320, 666);
            this.GetUser4.Name = "GetUser4";
            this.GetUser4.Size = new System.Drawing.Size(200, 71);
            this.GetUser4.TabIndex = 27;
            this.GetUser4.Visible = false;
            this.GetUser4.Click += new System.EventHandler(this.User_Click);
            // 
            // User10
            // 
            this.User10.AutoSize = true;
            this.User10.BackColor = System.Drawing.Color.Transparent;
            this.User10.Font = new System.Drawing.Font("PF Centro Slab Pro", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User10.Location = new System.Drawing.Point(105, 24);
            this.User10.Name = "User10";
            this.User10.Size = new System.Drawing.Size(0, 36);
            this.User10.TabIndex = 24;
            this.User10.Click += new System.EventHandler(this.User_Click);
            // 
            // FullName10
            // 
            this.FullName10.AutoSize = true;
            this.FullName10.BackColor = System.Drawing.Color.Transparent;
            this.FullName10.Font = new System.Drawing.Font("PF Centro Slab Pro", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FullName10.Location = new System.Drawing.Point(99, 19);
            this.FullName10.Name = "FullName10";
            this.FullName10.Size = new System.Drawing.Size(0, 26);
            this.FullName10.TabIndex = 24;
            this.FullName10.Click += new System.EventHandler(this.User_Click);
            // 
            // UserPic10
            // 
            this.UserPic10.Location = new System.Drawing.Point(24, 10);
            this.UserPic10.Name = "UserPic10";
            this.UserPic10.Size = new System.Drawing.Size(50, 50);
            this.UserPic10.TabIndex = 26;
            this.UserPic10.TabStop = false;
            this.UserPic10.Click += new System.EventHandler(this.User_Click);
            // 
            // GetUser10
            // 
            this.GetUser10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.GetUser10.Controls.Add(this.UserPic10);
            this.GetUser10.Controls.Add(this.FullName10);
            this.GetUser10.Controls.Add(this.User10);
            this.GetUser10.Location = new System.Drawing.Point(261, 641);
            this.GetUser10.Name = "GetUser10";
            this.GetUser10.Size = new System.Drawing.Size(200, 71);
            this.GetUser10.TabIndex = 27;
            this.GetUser10.Visible = false;
            this.GetUser10.Click += new System.EventHandler(this.User_Click);
            // 
            // cartIm_11
            // 
            this.cartIm_11.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_11.Location = new System.Drawing.Point(658, 10);
            this.cartIm_11.Name = "cartIm_11";
            this.cartIm_11.Size = new System.Drawing.Size(200, 200);
            this.cartIm_11.TabIndex = 7;
            this.cartIm_11.TabStop = false;
            this.cartIm_11.Visible = false;
            this.cartIm_11.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_12
            // 
            this.cartIm_12.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_12.Location = new System.Drawing.Point(658, 10);
            this.cartIm_12.Name = "cartIm_12";
            this.cartIm_12.Size = new System.Drawing.Size(200, 200);
            this.cartIm_12.TabIndex = 7;
            this.cartIm_12.TabStop = false;
            this.cartIm_12.Visible = false;
            this.cartIm_12.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_13
            // 
            this.cartIm_13.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_13.Location = new System.Drawing.Point(658, 10);
            this.cartIm_13.Name = "cartIm_13";
            this.cartIm_13.Size = new System.Drawing.Size(200, 200);
            this.cartIm_13.TabIndex = 7;
            this.cartIm_13.TabStop = false;
            this.cartIm_13.Visible = false;
            this.cartIm_13.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_14
            // 
            this.cartIm_14.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_14.Location = new System.Drawing.Point(658, 10);
            this.cartIm_14.Name = "cartIm_14";
            this.cartIm_14.Size = new System.Drawing.Size(200, 200);
            this.cartIm_14.TabIndex = 7;
            this.cartIm_14.TabStop = false;
            this.cartIm_14.Visible = false;
            this.cartIm_14.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_15
            // 
            this.cartIm_15.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_15.Location = new System.Drawing.Point(658, 10);
            this.cartIm_15.Name = "cartIm_15";
            this.cartIm_15.Size = new System.Drawing.Size(200, 200);
            this.cartIm_15.TabIndex = 7;
            this.cartIm_15.TabStop = false;
            this.cartIm_15.Visible = false;
            this.cartIm_15.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_16
            // 
            this.cartIm_16.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_16.Location = new System.Drawing.Point(658, 10);
            this.cartIm_16.Name = "cartIm_16";
            this.cartIm_16.Size = new System.Drawing.Size(200, 200);
            this.cartIm_16.TabIndex = 7;
            this.cartIm_16.TabStop = false;
            this.cartIm_16.Visible = false;
            this.cartIm_16.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_17
            // 
            this.cartIm_17.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_17.Location = new System.Drawing.Point(658, 9);
            this.cartIm_17.Name = "cartIm_17";
            this.cartIm_17.Size = new System.Drawing.Size(200, 200);
            this.cartIm_17.TabIndex = 7;
            this.cartIm_17.TabStop = false;
            this.cartIm_17.Visible = false;
            this.cartIm_17.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_18
            // 
            this.cartIm_18.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_18.Location = new System.Drawing.Point(658, 10);
            this.cartIm_18.Name = "cartIm_18";
            this.cartIm_18.Size = new System.Drawing.Size(200, 200);
            this.cartIm_18.TabIndex = 7;
            this.cartIm_18.TabStop = false;
            this.cartIm_18.Visible = false;
            this.cartIm_18.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_19
            // 
            this.cartIm_19.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_19.Location = new System.Drawing.Point(658, 11);
            this.cartIm_19.Name = "cartIm_19";
            this.cartIm_19.Size = new System.Drawing.Size(200, 200);
            this.cartIm_19.TabIndex = 7;
            this.cartIm_19.TabStop = false;
            this.cartIm_19.Visible = false;
            this.cartIm_19.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // cartIm_20
            // 
            this.cartIm_20.BackColor = System.Drawing.Color.Transparent;
            this.cartIm_20.Location = new System.Drawing.Point(658, 11);
            this.cartIm_20.Name = "cartIm_20";
            this.cartIm_20.Size = new System.Drawing.Size(200, 200);
            this.cartIm_20.TabIndex = 7;
            this.cartIm_20.TabStop = false;
            this.cartIm_20.Visible = false;
            this.cartIm_20.Click += new System.EventHandler(this.cartIm_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1500, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 69;
            this.label1.Text = "inpText";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(658, 479);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 71;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // picsPanel
            // 
            this.picsPanel.AutoScroll = true;
            this.picsPanel.BackColor = System.Drawing.Color.Transparent;
            this.picsPanel.DoubleBuffered = true;
            this.picsPanel.Location = new System.Drawing.Point(317, 528);
            this.picsPanel.Name = "picsPanel";
            this.picsPanel.Size = new System.Drawing.Size(200, 100);
            this.picsPanel.TabIndex = 40;
            this.picsPanel.Scroll += new System.Windows.Forms.ScrollEventHandler(this.picsPanel_Scroll);
            this.picsPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picsPanel_MouseDown);
            this.picsPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picsPanel_MouseMove);
            this.picsPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picsPanel_MouseUp);
            // 
            // PrintProgressBar
            // 
            this.PrintProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.PrintProgressBar.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(216)))));
            this.PrintProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintProgressBar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.PrintProgressBar.GradiantPosition = Lkbox.ProgressBarEx.GradiantArea.None;
            this.PrintProgressBar.Image = null;
            this.PrintProgressBar.Location = new System.Drawing.Point(12, 12);
            this.PrintProgressBar.Name = "PrintProgressBar";
            this.PrintProgressBar.ProgressColor = System.Drawing.Color.Red;
            this.PrintProgressBar.Size = new System.Drawing.Size(429, 23);
            this.PrintProgressBar.Text = "PrintProgressBar";
            this.PrintProgressBar.Value = 1;
            this.PrintProgressBar.Visible = false;
            // 
            // Keyboard
            // 
            this.Keyboard.BackColor = System.Drawing.Color.Transparent;
            this.Keyboard.Controls.Add(this.HdKey);
            this.Keyboard.Controls.Add(this.rSymb);
            this.Keyboard.Controls.Add(this.Space);
            this.Keyboard.Controls.Add(this.langChg);
            this.Keyboard.Controls.Add(this.lSymb);
            this.Keyboard.Controls.Add(this.rShift);
            this.Keyboard.Controls.Add(this.l9);
            this.Keyboard.Controls.Add(this.l8);
            this.Keyboard.Controls.Add(this.l7);
            this.Keyboard.Controls.Add(this.l6);
            this.Keyboard.Controls.Add(this.l5);
            this.Keyboard.Controls.Add(this.l4);
            this.Keyboard.Controls.Add(this.l3);
            this.Keyboard.Controls.Add(this.l2);
            this.Keyboard.Controls.Add(this.l1);
            this.Keyboard.Controls.Add(this.lShift);
            this.Keyboard.Controls.Add(this.Search);
            this.Keyboard.Controls.Add(this.m9);
            this.Keyboard.Controls.Add(this.m8);
            this.Keyboard.Controls.Add(this.m7);
            this.Keyboard.Controls.Add(this.m6);
            this.Keyboard.Controls.Add(this.m5);
            this.Keyboard.Controls.Add(this.m4);
            this.Keyboard.Controls.Add(this.m3);
            this.Keyboard.Controls.Add(this.m2);
            this.Keyboard.Controls.Add(this.m1);
            this.Keyboard.Controls.Add(this.bcsp);
            this.Keyboard.Controls.Add(this.u10);
            this.Keyboard.Controls.Add(this.u9);
            this.Keyboard.Controls.Add(this.l11);
            this.Keyboard.Controls.Add(this.l10);
            this.Keyboard.Controls.Add(this.m11);
            this.Keyboard.Controls.Add(this.m10);
            this.Keyboard.Controls.Add(this.u12);
            this.Keyboard.Controls.Add(this.u11);
            this.Keyboard.Controls.Add(this.u8);
            this.Keyboard.Controls.Add(this.u7);
            this.Keyboard.Controls.Add(this.u6);
            this.Keyboard.Controls.Add(this.u5);
            this.Keyboard.Controls.Add(this.u4);
            this.Keyboard.Controls.Add(this.u3);
            this.Keyboard.Controls.Add(this.u2);
            this.Keyboard.Controls.Add(this.u1);
            this.Keyboard.DoubleBuffered = true;
            this.Keyboard.Location = new System.Drawing.Point(261, 749);
            this.Keyboard.Name = "Keyboard";
            this.Keyboard.Size = new System.Drawing.Size(213, 117);
            this.Keyboard.TabIndex = 20;
            this.Keyboard.Visible = false;
            // 
            // HdKey
            // 
            this.HdKey.FlatAppearance.BorderSize = 0;
            this.HdKey.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.HdKey.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.HdKey.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.HdKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HdKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdKey.ForeColor = System.Drawing.Color.White;
            this.HdKey.Location = new System.Drawing.Point(43, 59);
            this.HdKey.Name = "HdKey";
            this.HdKey.Size = new System.Drawing.Size(13, 8);
            this.HdKey.TabIndex = 36;
            this.HdKey.TabStop = false;
            this.HdKey.UseVisualStyleBackColor = true;
            this.HdKey.Click += new System.EventHandler(this.Key_Click);
            this.HdKey.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.HdKey.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // rSymb
            // 
            this.rSymb.FlatAppearance.BorderSize = 0;
            this.rSymb.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.rSymb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.rSymb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.rSymb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rSymb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rSymb.ForeColor = System.Drawing.Color.White;
            this.rSymb.Location = new System.Drawing.Point(155, 73);
            this.rSymb.Name = "rSymb";
            this.rSymb.Size = new System.Drawing.Size(13, 8);
            this.rSymb.TabIndex = 35;
            this.rSymb.TabStop = false;
            this.rSymb.UseVisualStyleBackColor = true;
            this.rSymb.Click += new System.EventHandler(this.Key_Click);
            this.rSymb.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.rSymb.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // Space
            // 
            this.Space.FlatAppearance.BorderSize = 0;
            this.Space.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.Space.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.Space.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Space.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Space.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Space.ForeColor = System.Drawing.Color.White;
            this.Space.Location = new System.Drawing.Point(24, 59);
            this.Space.Name = "Space";
            this.Space.Size = new System.Drawing.Size(13, 8);
            this.Space.TabIndex = 34;
            this.Space.TabStop = false;
            this.Space.UseVisualStyleBackColor = true;
            this.Space.Click += new System.EventHandler(this.Key_Click);
            this.Space.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.Space.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // langChg
            // 
            this.langChg.FlatAppearance.BorderSize = 0;
            this.langChg.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.langChg.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.langChg.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.langChg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.langChg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.langChg.ForeColor = System.Drawing.Color.White;
            this.langChg.Location = new System.Drawing.Point(13, 59);
            this.langChg.Name = "langChg";
            this.langChg.Size = new System.Drawing.Size(13, 8);
            this.langChg.TabIndex = 33;
            this.langChg.TabStop = false;
            this.langChg.UseVisualStyleBackColor = true;
            this.langChg.Click += new System.EventHandler(this.Key_Click);
            this.langChg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.langChg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // lSymb
            // 
            this.lSymb.FlatAppearance.BorderSize = 0;
            this.lSymb.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.lSymb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.lSymb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.lSymb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lSymb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lSymb.ForeColor = System.Drawing.Color.White;
            this.lSymb.Location = new System.Drawing.Point(3, 59);
            this.lSymb.Name = "lSymb";
            this.lSymb.Size = new System.Drawing.Size(13, 8);
            this.lSymb.TabIndex = 32;
            this.lSymb.TabStop = false;
            this.lSymb.UseVisualStyleBackColor = true;
            this.lSymb.Click += new System.EventHandler(this.Key_Click);
            this.lSymb.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.lSymb.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // rShift
            // 
            this.rShift.FlatAppearance.BorderSize = 0;
            this.rShift.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.rShift.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.rShift.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.rShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rShift.ForeColor = System.Drawing.Color.White;
            this.rShift.Location = new System.Drawing.Point(72, 45);
            this.rShift.Name = "rShift";
            this.rShift.Size = new System.Drawing.Size(13, 8);
            this.rShift.TabIndex = 31;
            this.rShift.TabStop = false;
            this.rShift.UseVisualStyleBackColor = true;
            this.rShift.Click += new System.EventHandler(this.Key_Click);
            this.rShift.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.rShift.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l9
            // 
            this.l9.FlatAppearance.BorderSize = 0;
            this.l9.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l9.ForeColor = System.Drawing.Color.White;
            this.l9.Location = new System.Drawing.Point(62, 45);
            this.l9.Name = "l9";
            this.l9.Size = new System.Drawing.Size(13, 8);
            this.l9.TabIndex = 30;
            this.l9.TabStop = false;
            this.l9.UseVisualStyleBackColor = true;
            this.l9.Click += new System.EventHandler(this.Key_Click);
            this.l9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l8
            // 
            this.l8.FlatAppearance.BorderSize = 0;
            this.l8.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l8.ForeColor = System.Drawing.Color.White;
            this.l8.Location = new System.Drawing.Point(53, 45);
            this.l8.Name = "l8";
            this.l8.Size = new System.Drawing.Size(13, 8);
            this.l8.TabIndex = 29;
            this.l8.TabStop = false;
            this.l8.UseVisualStyleBackColor = true;
            this.l8.Click += new System.EventHandler(this.Key_Click);
            this.l8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l7
            // 
            this.l7.FlatAppearance.BorderSize = 0;
            this.l7.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l7.ForeColor = System.Drawing.Color.White;
            this.l7.Location = new System.Drawing.Point(43, 45);
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(13, 8);
            this.l7.TabIndex = 28;
            this.l7.TabStop = false;
            this.l7.UseVisualStyleBackColor = true;
            this.l7.Click += new System.EventHandler(this.Key_Click);
            this.l7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l6
            // 
            this.l6.FlatAppearance.BorderSize = 0;
            this.l6.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l6.ForeColor = System.Drawing.Color.White;
            this.l6.Location = new System.Drawing.Point(32, 45);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(13, 8);
            this.l6.TabIndex = 27;
            this.l6.TabStop = false;
            this.l6.UseVisualStyleBackColor = true;
            this.l6.Click += new System.EventHandler(this.Key_Click);
            this.l6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l5
            // 
            this.l5.FlatAppearance.BorderSize = 0;
            this.l5.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l5.ForeColor = System.Drawing.Color.White;
            this.l5.Location = new System.Drawing.Point(22, 45);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(13, 8);
            this.l5.TabIndex = 26;
            this.l5.TabStop = false;
            this.l5.UseVisualStyleBackColor = true;
            this.l5.Click += new System.EventHandler(this.Key_Click);
            this.l5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l4
            // 
            this.l4.FlatAppearance.BorderSize = 0;
            this.l4.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l4.ForeColor = System.Drawing.Color.White;
            this.l4.Location = new System.Drawing.Point(13, 45);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(13, 8);
            this.l4.TabIndex = 25;
            this.l4.TabStop = false;
            this.l4.UseVisualStyleBackColor = true;
            this.l4.Click += new System.EventHandler(this.Key_Click);
            this.l4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l3
            // 
            this.l3.FlatAppearance.BorderSize = 0;
            this.l3.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3.ForeColor = System.Drawing.Color.White;
            this.l3.Location = new System.Drawing.Point(3, 45);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(13, 8);
            this.l3.TabIndex = 24;
            this.l3.TabStop = false;
            this.l3.UseVisualStyleBackColor = true;
            this.l3.Click += new System.EventHandler(this.Key_Click);
            this.l3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l2
            // 
            this.l2.FlatAppearance.BorderSize = 0;
            this.l2.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2.ForeColor = System.Drawing.Color.White;
            this.l2.Location = new System.Drawing.Point(72, 31);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(13, 8);
            this.l2.TabIndex = 23;
            this.l2.TabStop = false;
            this.l2.UseVisualStyleBackColor = true;
            this.l2.Click += new System.EventHandler(this.Key_Click);
            this.l2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l1
            // 
            this.l1.FlatAppearance.BorderSize = 0;
            this.l1.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.ForeColor = System.Drawing.Color.White;
            this.l1.Location = new System.Drawing.Point(62, 31);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(13, 8);
            this.l1.TabIndex = 22;
            this.l1.TabStop = false;
            this.l1.UseVisualStyleBackColor = true;
            this.l1.Click += new System.EventHandler(this.Key_Click);
            this.l1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // lShift
            // 
            this.lShift.FlatAppearance.BorderSize = 0;
            this.lShift.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.lShift.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.lShift.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.lShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lShift.ForeColor = System.Drawing.Color.White;
            this.lShift.Location = new System.Drawing.Point(53, 31);
            this.lShift.Name = "lShift";
            this.lShift.Size = new System.Drawing.Size(13, 8);
            this.lShift.TabIndex = 21;
            this.lShift.TabStop = false;
            this.lShift.UseVisualStyleBackColor = true;
            this.lShift.Click += new System.EventHandler(this.Key_Click);
            this.lShift.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.lShift.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // Search
            // 
            this.Search.FlatAppearance.BorderSize = 0;
            this.Search.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.Search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.Search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Search.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Search.ForeColor = System.Drawing.Color.White;
            this.Search.Location = new System.Drawing.Point(43, 31);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(13, 8);
            this.Search.TabIndex = 20;
            this.Search.TabStop = false;
            this.Search.UseVisualStyleBackColor = true;
            this.Search.Click += new System.EventHandler(this.Key_Click);
            this.Search.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.Search.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m9
            // 
            this.m9.FlatAppearance.BorderSize = 0;
            this.m9.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m9.ForeColor = System.Drawing.Color.White;
            this.m9.Location = new System.Drawing.Point(32, 31);
            this.m9.Name = "m9";
            this.m9.Size = new System.Drawing.Size(13, 8);
            this.m9.TabIndex = 19;
            this.m9.TabStop = false;
            this.m9.UseVisualStyleBackColor = true;
            this.m9.Click += new System.EventHandler(this.Key_Click);
            this.m9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m8
            // 
            this.m8.FlatAppearance.BorderSize = 0;
            this.m8.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m8.ForeColor = System.Drawing.Color.White;
            this.m8.Location = new System.Drawing.Point(22, 31);
            this.m8.Name = "m8";
            this.m8.Size = new System.Drawing.Size(13, 8);
            this.m8.TabIndex = 18;
            this.m8.TabStop = false;
            this.m8.UseVisualStyleBackColor = true;
            this.m8.Click += new System.EventHandler(this.Key_Click);
            this.m8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m7
            // 
            this.m7.FlatAppearance.BorderSize = 0;
            this.m7.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m7.ForeColor = System.Drawing.Color.White;
            this.m7.Location = new System.Drawing.Point(13, 31);
            this.m7.Name = "m7";
            this.m7.Size = new System.Drawing.Size(13, 8);
            this.m7.TabIndex = 17;
            this.m7.TabStop = false;
            this.m7.UseVisualStyleBackColor = true;
            this.m7.Click += new System.EventHandler(this.Key_Click);
            this.m7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m6
            // 
            this.m6.FlatAppearance.BorderSize = 0;
            this.m6.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m6.ForeColor = System.Drawing.Color.White;
            this.m6.Location = new System.Drawing.Point(3, 31);
            this.m6.Name = "m6";
            this.m6.Size = new System.Drawing.Size(13, 8);
            this.m6.TabIndex = 16;
            this.m6.TabStop = false;
            this.m6.UseVisualStyleBackColor = true;
            this.m6.Click += new System.EventHandler(this.Key_Click);
            this.m6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m5
            // 
            this.m5.FlatAppearance.BorderSize = 0;
            this.m5.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m5.ForeColor = System.Drawing.Color.White;
            this.m5.Location = new System.Drawing.Point(72, 17);
            this.m5.Name = "m5";
            this.m5.Size = new System.Drawing.Size(13, 8);
            this.m5.TabIndex = 15;
            this.m5.TabStop = false;
            this.m5.UseVisualStyleBackColor = true;
            this.m5.Click += new System.EventHandler(this.Key_Click);
            this.m5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m4
            // 
            this.m4.FlatAppearance.BorderSize = 0;
            this.m4.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m4.ForeColor = System.Drawing.Color.White;
            this.m4.Location = new System.Drawing.Point(62, 17);
            this.m4.Name = "m4";
            this.m4.Size = new System.Drawing.Size(13, 8);
            this.m4.TabIndex = 14;
            this.m4.TabStop = false;
            this.m4.UseVisualStyleBackColor = true;
            this.m4.Click += new System.EventHandler(this.Key_Click);
            this.m4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m3
            // 
            this.m3.FlatAppearance.BorderSize = 0;
            this.m3.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m3.ForeColor = System.Drawing.Color.White;
            this.m3.Location = new System.Drawing.Point(53, 17);
            this.m3.Name = "m3";
            this.m3.Size = new System.Drawing.Size(13, 8);
            this.m3.TabIndex = 13;
            this.m3.TabStop = false;
            this.m3.UseVisualStyleBackColor = true;
            this.m3.Click += new System.EventHandler(this.Key_Click);
            this.m3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m2
            // 
            this.m2.FlatAppearance.BorderSize = 0;
            this.m2.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m2.ForeColor = System.Drawing.Color.White;
            this.m2.Location = new System.Drawing.Point(43, 17);
            this.m2.Name = "m2";
            this.m2.Size = new System.Drawing.Size(13, 8);
            this.m2.TabIndex = 12;
            this.m2.TabStop = false;
            this.m2.UseVisualStyleBackColor = true;
            this.m2.Click += new System.EventHandler(this.Key_Click);
            this.m2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m1
            // 
            this.m1.FlatAppearance.BorderSize = 0;
            this.m1.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m1.ForeColor = System.Drawing.Color.White;
            this.m1.Location = new System.Drawing.Point(32, 17);
            this.m1.Name = "m1";
            this.m1.Size = new System.Drawing.Size(13, 8);
            this.m1.TabIndex = 11;
            this.m1.TabStop = false;
            this.m1.UseVisualStyleBackColor = true;
            this.m1.Click += new System.EventHandler(this.Key_Click);
            this.m1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // bcsp
            // 
            this.bcsp.FlatAppearance.BorderSize = 0;
            this.bcsp.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.bcsp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bcsp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bcsp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bcsp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bcsp.ForeColor = System.Drawing.Color.White;
            this.bcsp.Location = new System.Drawing.Point(22, 17);
            this.bcsp.Name = "bcsp";
            this.bcsp.Size = new System.Drawing.Size(13, 8);
            this.bcsp.TabIndex = 10;
            this.bcsp.TabStop = false;
            this.bcsp.UseVisualStyleBackColor = true;
            this.bcsp.Click += new System.EventHandler(this.Key_Click);
            this.bcsp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.bcsp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u10
            // 
            this.u10.FlatAppearance.BorderSize = 0;
            this.u10.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u10.ForeColor = System.Drawing.Color.White;
            this.u10.Location = new System.Drawing.Point(13, 17);
            this.u10.Name = "u10";
            this.u10.Size = new System.Drawing.Size(13, 8);
            this.u10.TabIndex = 9;
            this.u10.TabStop = false;
            this.u10.UseVisualStyleBackColor = true;
            this.u10.Click += new System.EventHandler(this.Key_Click);
            this.u10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u9
            // 
            this.u9.FlatAppearance.BorderSize = 0;
            this.u9.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u9.ForeColor = System.Drawing.Color.White;
            this.u9.Location = new System.Drawing.Point(3, 17);
            this.u9.Name = "u9";
            this.u9.Size = new System.Drawing.Size(13, 8);
            this.u9.TabIndex = 8;
            this.u9.TabStop = false;
            this.u9.UseVisualStyleBackColor = true;
            this.u9.Click += new System.EventHandler(this.Key_Click);
            this.u9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l11
            // 
            this.l11.FlatAppearance.BorderSize = 0;
            this.l11.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l11.ForeColor = System.Drawing.Color.White;
            this.l11.Location = new System.Drawing.Point(24, 35);
            this.l11.Name = "l11";
            this.l11.Size = new System.Drawing.Size(13, 8);
            this.l11.TabIndex = 7;
            this.l11.TabStop = false;
            this.l11.UseVisualStyleBackColor = true;
            this.l11.Click += new System.EventHandler(this.Key_Click);
            this.l11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // l10
            // 
            this.l10.FlatAppearance.BorderSize = 0;
            this.l10.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.l10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.l10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.l10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l10.ForeColor = System.Drawing.Color.White;
            this.l10.Location = new System.Drawing.Point(5, 35);
            this.l10.Name = "l10";
            this.l10.Size = new System.Drawing.Size(13, 8);
            this.l10.TabIndex = 7;
            this.l10.TabStop = false;
            this.l10.UseVisualStyleBackColor = true;
            this.l10.Click += new System.EventHandler(this.Key_Click);
            this.l10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.l10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m11
            // 
            this.m11.FlatAppearance.BorderSize = 0;
            this.m11.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m11.ForeColor = System.Drawing.Color.White;
            this.m11.Location = new System.Drawing.Point(22, 21);
            this.m11.Name = "m11";
            this.m11.Size = new System.Drawing.Size(13, 8);
            this.m11.TabIndex = 7;
            this.m11.TabStop = false;
            this.m11.UseVisualStyleBackColor = true;
            this.m11.Click += new System.EventHandler(this.Key_Click);
            this.m11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // m10
            // 
            this.m10.FlatAppearance.BorderSize = 0;
            this.m10.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.m10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.m10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m10.ForeColor = System.Drawing.Color.White;
            this.m10.Location = new System.Drawing.Point(5, 21);
            this.m10.Name = "m10";
            this.m10.Size = new System.Drawing.Size(13, 8);
            this.m10.TabIndex = 7;
            this.m10.TabStop = false;
            this.m10.UseVisualStyleBackColor = true;
            this.m10.Click += new System.EventHandler(this.Key_Click);
            this.m10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.m10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u12
            // 
            this.u12.FlatAppearance.BorderSize = 0;
            this.u12.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u12.ForeColor = System.Drawing.Color.White;
            this.u12.Location = new System.Drawing.Point(22, 5);
            this.u12.Name = "u12";
            this.u12.Size = new System.Drawing.Size(13, 8);
            this.u12.TabIndex = 7;
            this.u12.TabStop = false;
            this.u12.UseVisualStyleBackColor = true;
            this.u12.Click += new System.EventHandler(this.Key_Click);
            this.u12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u11
            // 
            this.u11.FlatAppearance.BorderSize = 0;
            this.u11.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u11.ForeColor = System.Drawing.Color.White;
            this.u11.Location = new System.Drawing.Point(3, 5);
            this.u11.Name = "u11";
            this.u11.Size = new System.Drawing.Size(13, 8);
            this.u11.TabIndex = 7;
            this.u11.TabStop = false;
            this.u11.UseVisualStyleBackColor = true;
            this.u11.Click += new System.EventHandler(this.Key_Click);
            this.u11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u8
            // 
            this.u8.FlatAppearance.BorderSize = 0;
            this.u8.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u8.ForeColor = System.Drawing.Color.White;
            this.u8.Location = new System.Drawing.Point(72, 3);
            this.u8.Name = "u8";
            this.u8.Size = new System.Drawing.Size(13, 8);
            this.u8.TabIndex = 7;
            this.u8.TabStop = false;
            this.u8.UseVisualStyleBackColor = true;
            this.u8.Click += new System.EventHandler(this.Key_Click);
            this.u8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u7
            // 
            this.u7.FlatAppearance.BorderSize = 0;
            this.u7.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u7.ForeColor = System.Drawing.Color.White;
            this.u7.Location = new System.Drawing.Point(62, 3);
            this.u7.Name = "u7";
            this.u7.Size = new System.Drawing.Size(13, 8);
            this.u7.TabIndex = 6;
            this.u7.TabStop = false;
            this.u7.UseVisualStyleBackColor = true;
            this.u7.Click += new System.EventHandler(this.Key_Click);
            this.u7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u6
            // 
            this.u6.FlatAppearance.BorderSize = 0;
            this.u6.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u6.ForeColor = System.Drawing.Color.White;
            this.u6.Location = new System.Drawing.Point(53, 3);
            this.u6.Name = "u6";
            this.u6.Size = new System.Drawing.Size(13, 8);
            this.u6.TabIndex = 5;
            this.u6.TabStop = false;
            this.u6.UseVisualStyleBackColor = true;
            this.u6.Click += new System.EventHandler(this.Key_Click);
            this.u6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u5
            // 
            this.u5.FlatAppearance.BorderSize = 0;
            this.u5.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u5.ForeColor = System.Drawing.Color.White;
            this.u5.Location = new System.Drawing.Point(43, 3);
            this.u5.Name = "u5";
            this.u5.Size = new System.Drawing.Size(13, 8);
            this.u5.TabIndex = 4;
            this.u5.TabStop = false;
            this.u5.UseVisualStyleBackColor = true;
            this.u5.Click += new System.EventHandler(this.Key_Click);
            this.u5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u4
            // 
            this.u4.FlatAppearance.BorderSize = 0;
            this.u4.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u4.ForeColor = System.Drawing.Color.White;
            this.u4.Location = new System.Drawing.Point(32, 3);
            this.u4.Name = "u4";
            this.u4.Size = new System.Drawing.Size(13, 8);
            this.u4.TabIndex = 3;
            this.u4.TabStop = false;
            this.u4.UseVisualStyleBackColor = true;
            this.u4.Click += new System.EventHandler(this.Key_Click);
            this.u4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u3
            // 
            this.u3.FlatAppearance.BorderSize = 0;
            this.u3.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u3.ForeColor = System.Drawing.Color.White;
            this.u3.Location = new System.Drawing.Point(22, 3);
            this.u3.Name = "u3";
            this.u3.Size = new System.Drawing.Size(13, 8);
            this.u3.TabIndex = 2;
            this.u3.TabStop = false;
            this.u3.UseVisualStyleBackColor = true;
            this.u3.Click += new System.EventHandler(this.Key_Click);
            this.u3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u2
            // 
            this.u2.FlatAppearance.BorderSize = 0;
            this.u2.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u2.ForeColor = System.Drawing.Color.White;
            this.u2.Location = new System.Drawing.Point(13, 3);
            this.u2.Name = "u2";
            this.u2.Size = new System.Drawing.Size(13, 8);
            this.u2.TabIndex = 1;
            this.u2.TabStop = false;
            this.u2.UseVisualStyleBackColor = true;
            this.u2.Click += new System.EventHandler(this.Key_Click);
            this.u2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // u1
            // 
            this.u1.FlatAppearance.BorderSize = 0;
            this.u1.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.u1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.u1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.u1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.u1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.u1.ForeColor = System.Drawing.Color.White;
            this.u1.Location = new System.Drawing.Point(3, 3);
            this.u1.Name = "u1";
            this.u1.Size = new System.Drawing.Size(13, 8);
            this.u1.TabIndex = 0;
            this.u1.TabStop = false;
            this.u1.UseVisualStyleBackColor = true;
            this.u1.Click += new System.EventHandler(this.Key_Click);
            this.u1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Key_MouseDown);
            this.u1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Key_MouseUp);
            // 
            // textChecker
            // 
            this.textChecker.Tick += new System.EventHandler(this.textChecker_Tick);
            // 
            // Form1
            // 
            this.AcceptButton = this.Next;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(942, 878);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filterPic9);
            this.Controls.Add(this.filterPic10);
            this.Controls.Add(this.filterPic11);
            this.Controls.Add(this.filterPic12);
            this.Controls.Add(this.filterPic5);
            this.Controls.Add(this.filterPic6);
            this.Controls.Add(this.filterPic7);
            this.Controls.Add(this.filterPic8);
            this.Controls.Add(this.ToEditLabel);
            this.Controls.Add(this.PaidCashLbl);
            this.Controls.Add(this.NeedCashLbl);
            this.Controls.Add(this.OrderedLbl);
            this.Controls.Add(this.CartLbl);
            this.Controls.Add(this.MkSelfieLbl);
            this.Controls.Add(this.VideoFeedbackLbl);
            this.Controls.Add(this.PrintPhoneLbl);
            this.Controls.Add(this.MainScreenLbl);
            this.Controls.Add(this.rtnMnPnl);
            this.Controls.Add(this.loadingPic);
            this.Controls.Add(this.picsPanel);
            this.Controls.Add(this.InactivityTest);
            this.Controls.Add(this.VideoPlay);
            this.Controls.Add(this.PrintEven);
            this.Controls.Add(this.Lang2);
            this.Controls.Add(this.Lang1);
            this.Controls.Add(this.PrintProgressBar);
            this.Controls.Add(this.PrintPageLbl);
            this.Controls.Add(this.InputBox);
            this.Controls.Add(this.OrderDel);
            this.Controls.Add(this.TurnLeft);
            this.Controls.Add(this.EditOk);
            this.Controls.Add(this.EditCancel);
            this.Controls.Add(this.TurnRight);
            this.Controls.Add(this.TextOk);
            this.Controls.Add(this.AddText);
            this.Controls.Add(this.CashPaid);
            this.Controls.Add(this.BalLbl);
            this.Controls.Add(this.GetUser10);
            this.Controls.Add(this.GetUser4);
            this.Controls.Add(this.GetUser9);
            this.Controls.Add(this.GetUser2);
            this.Controls.Add(this.GetUser8);
            this.Controls.Add(this.GetUser5);
            this.Controls.Add(this.GetUser7);
            this.Controls.Add(this.GetUser3);
            this.Controls.Add(this.GetUser6);
            this.Controls.Add(this.GetUser1);
            this.Controls.Add(this.AccSearch);
            this.Controls.Add(this.TimerTime);
            this.Controls.Add(this.TimerChecker);
            this.Controls.Add(this.Keyboard);
            this.Controls.Add(this.InfoClose);
            this.Controls.Add(this.InfoBttn);
            this.Controls.Add(this.filterPic4);
            this.Controls.Add(this.filterPic3);
            this.Controls.Add(this.filterPic2);
            this.Controls.Add(this.filterPic1);
            this.Controls.Add(this.FiltersPrev);
            this.Controls.Add(this.FiltersNext);
            this.Controls.Add(this.FiltersOk);
            this.Controls.Add(this.FiltersBack);
            this.Controls.Add(this.SearchBack);
            this.Controls.Add(this.Prev);
            this.Controls.Add(this.Next);
            this.Controls.Add(this.RstBttn);
            this.Controls.Add(this.PrintBack);
            this.Controls.Add(this.CashNeeded);
            this.Controls.Add(this.PrintingLbl);
            this.Controls.Add(this.CashAmmLbl);
            this.Controls.Add(this.DoNotForgetLbl);
            this.Controls.Add(this.SupportLbl);
            this.Controls.Add(this.AutoMainLbl);
            this.Controls.Add(this.NoCoinsLbl);
            this.Controls.Add(this.ThanksLbl);
            this.Controls.Add(this.AcceptedCashLbl);
            this.Controls.Add(this.PhotoAmmLbl);
            this.Controls.Add(this.Print);
            this.Controls.Add(this.cartIm_20);
            this.Controls.Add(this.cartIm_19);
            this.Controls.Add(this.cartIm_10);
            this.Controls.Add(this.cartIm_18);
            this.Controls.Add(this.cartIm_9);
            this.Controls.Add(this.cartIm_17);
            this.Controls.Add(this.cartIm_8);
            this.Controls.Add(this.cartIm_16);
            this.Controls.Add(this.cartIm_7);
            this.Controls.Add(this.cartIm_15);
            this.Controls.Add(this.cartIm_6);
            this.Controls.Add(this.cartIm_14);
            this.Controls.Add(this.cartIm_5);
            this.Controls.Add(this.cartIm_13);
            this.Controls.Add(this.cartIm_4);
            this.Controls.Add(this.cartIm_12);
            this.Controls.Add(this.cartIm_3);
            this.Controls.Add(this.cartIm_11);
            this.Controls.Add(this.cartIm_2);
            this.Controls.Add(this.cartIm_1);
            this.Controls.Add(this.CartOk);
            this.Controls.Add(this.BttnSearchBack);
            this.Controls.Add(this.RetakeSelf);
            this.Controls.Add(this.PhotoOk);
            this.Controls.Add(this.SelfieTake);
            this.Controls.Add(this.RtnMain);
            this.Controls.Add(this.SelfView);
            this.Controls.Add(this.SelfPreview);
            this.Controls.Add(this.InstaSearch);
            this.Controls.Add(this.MkSelfie);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "App";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.SelfPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerChecker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadingPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterPic12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic1)).EndInit();
            this.GetUser1.ResumeLayout(false);
            this.GetUser1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic6)).EndInit();
            this.GetUser6.ResumeLayout(false);
            this.GetUser6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic3)).EndInit();
            this.GetUser3.ResumeLayout(false);
            this.GetUser3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic7)).EndInit();
            this.GetUser7.ResumeLayout(false);
            this.GetUser7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic5)).EndInit();
            this.GetUser5.ResumeLayout(false);
            this.GetUser5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic8)).EndInit();
            this.GetUser8.ResumeLayout(false);
            this.GetUser8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic2)).EndInit();
            this.GetUser2.ResumeLayout(false);
            this.GetUser2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic9)).EndInit();
            this.GetUser9.ResumeLayout(false);
            this.GetUser9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic4)).EndInit();
            this.GetUser4.ResumeLayout(false);
            this.GetUser4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserPic10)).EndInit();
            this.GetUser10.ResumeLayout(false);
            this.GetUser10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartIm_20)).EndInit();
            this.Keyboard.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button MkSelfie;
        private System.Windows.Forms.Button InstaSearch;
        private System.IO.Ports.SerialPort sp;
        private System.Windows.Forms.Timer tmr;
        private System.Windows.Forms.PictureBox SelfPreview;
        private System.Windows.Forms.PictureBox SelfView;
        private System.Windows.Forms.Button RtnMain;
        private System.Windows.Forms.Button SelfieTake;
        private System.Windows.Forms.Button PhotoOk;
        private System.Windows.Forms.Button RetakeSelf;
        private System.Windows.Forms.Button BttnSearchBack;
        private System.Windows.Forms.Button CartOk;
        private System.Drawing.Printing.PrintDocument pd;
        private System.Windows.Forms.Timer tmer;
        private System.Windows.Forms.PictureBox cartIm_1;
        private System.Windows.Forms.PictureBox cartIm_2;
        private System.Windows.Forms.PictureBox cartIm_3;
        private System.Windows.Forms.PictureBox cartIm_4;
        private System.Windows.Forms.PictureBox cartIm_5;
        private System.Windows.Forms.PictureBox cartIm_6;
        private System.Windows.Forms.PictureBox cartIm_7;
        private System.Windows.Forms.PictureBox cartIm_8;
        private System.Windows.Forms.PictureBox cartIm_9;
        private System.Windows.Forms.PictureBox cartIm_10;
        private System.Windows.Forms.PictureBox cartIm_11;
        private System.Windows.Forms.PictureBox cartIm_12;
        private System.Windows.Forms.PictureBox cartIm_13;
        private System.Windows.Forms.PictureBox cartIm_14;
        private System.Windows.Forms.PictureBox cartIm_15;
        private System.Windows.Forms.PictureBox cartIm_16;
        private System.Windows.Forms.PictureBox cartIm_17;
        private System.Windows.Forms.PictureBox cartIm_18;
        private System.Windows.Forms.PictureBox cartIm_19;
        private System.Windows.Forms.PictureBox cartIm_20;
        private System.Windows.Forms.Button Print;
        private System.Windows.Forms.Label PhotoAmmLbl;
        private System.Windows.Forms.Label CashAmmLbl;
        private System.Windows.Forms.Label CashNeeded;
        private System.Windows.Forms.Button PrintBack;
        private System.Windows.Forms.Button RstBttn;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Prev;
        private System.Windows.Forms.Button SearchBack;
        private System.Windows.Forms.Button FiltersBack;
        private System.Windows.Forms.Button FiltersOk;
        private System.Windows.Forms.Button FiltersNext;
        private System.Windows.Forms.Button FiltersPrev;
        private System.Windows.Forms.PictureBox filterPic1;
        private System.Windows.Forms.PictureBox filterPic2;
        private System.Windows.Forms.PictureBox filterPic3;
        private System.Windows.Forms.Button InfoBttn;
        private System.Windows.Forms.Button InfoClose;
        private Lkbox.DoubleBufferedPanel Keyboard;
        private System.Windows.Forms.Button HdKey;
        private System.Windows.Forms.Button rSymb;
        private System.Windows.Forms.Button Space;
        private System.Windows.Forms.Button langChg;
        private System.Windows.Forms.Button lSymb;
        private System.Windows.Forms.Button rShift;
        private System.Windows.Forms.Button l9;
        private System.Windows.Forms.Button l8;
        private System.Windows.Forms.Button l7;
        private System.Windows.Forms.Button l6;
        private System.Windows.Forms.Button l5;
        private System.Windows.Forms.Button l4;
        private System.Windows.Forms.Button l3;
        private System.Windows.Forms.Button l2;
        private System.Windows.Forms.Button l1;
        private System.Windows.Forms.Button lShift;
        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.Button m9;
        private System.Windows.Forms.Button m8;
        private System.Windows.Forms.Button m7;
        private System.Windows.Forms.Button m6;
        private System.Windows.Forms.Button m5;
        private System.Windows.Forms.Button m4;
        private System.Windows.Forms.Button m3;
        private System.Windows.Forms.Button m2;
        private System.Windows.Forms.Button m1;
        private System.Windows.Forms.Button bcsp;
        private System.Windows.Forms.Button u10;
        private System.Windows.Forms.Button u9;
        private System.Windows.Forms.Button u8;
        private System.Windows.Forms.Button u7;
        private System.Windows.Forms.Button u6;
        private System.Windows.Forms.Button u5;
        private System.Windows.Forms.Button u4;
        private System.Windows.Forms.Button u3;
        private System.Windows.Forms.Button u2;
        private System.Windows.Forms.Button u1;
        private System.Windows.Forms.PictureBox filterPic4;
        private System.Windows.Forms.Timer slfyTimer;
        private System.Windows.Forms.PictureBox TimerChecker;
        private System.Windows.Forms.Label TimerTime;
        private System.Windows.Forms.TextBox AccSearch;
        private System.Windows.Forms.Label BalLbl;
        private System.Windows.Forms.Label CashPaid;
        private System.Windows.Forms.Button OrderDel;
        private System.Windows.Forms.RichTextBox InputBox;
        private System.Windows.Forms.Button l11;
        private System.Windows.Forms.Button l10;
        private System.Windows.Forms.Button m11;
        private System.Windows.Forms.Button m10;
        private System.Windows.Forms.Button u12;
        private System.Windows.Forms.Button u11;
        private System.Windows.Forms.Button AddText;
        private System.Windows.Forms.Button TurnRight;
        private System.Windows.Forms.Button TurnLeft;
        private System.Windows.Forms.Button TextOk;
        private System.Windows.Forms.Label PrintPageLbl;
        private System.Windows.Forms.Timer printTimer;
        private ProgressBarEx PrintProgressBar;
        private System.Windows.Forms.Timer searchTimer;
        private System.Windows.Forms.Button Lang1;
        private System.Windows.Forms.Button Lang2;
        private System.Windows.Forms.Label PrintEven;
        private System.Windows.Forms.Panel VideoPlay;
        private System.Windows.Forms.Button InactivityTest;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DoubleBufferedPanel picsPanel;
        private System.Windows.Forms.PictureBox loadingPic;
        private System.Windows.Forms.Timer WaitModeTmr;
        private System.Windows.Forms.Button EditCancel;
        private System.Windows.Forms.Button EditOk;
        private System.Windows.Forms.Panel rtnMnPnl;
        private System.Windows.Forms.Label MainScreenLbl;
        private System.Windows.Forms.Label PrintPhoneLbl;
        private System.Windows.Forms.Label MkSelfieLbl;
        private System.Windows.Forms.Label VideoFeedbackLbl;
        private System.Windows.Forms.Label CartLbl;
        private System.Windows.Forms.Label OrderedLbl;
        private System.Windows.Forms.Label NeedCashLbl;
        private System.Windows.Forms.Label PaidCashLbl;
        private System.Windows.Forms.Label PrintingLbl;
        private System.Windows.Forms.Label AcceptedCashLbl;
        private System.Windows.Forms.Label NoCoinsLbl;
        private System.Windows.Forms.Label SupportLbl;
        private System.Windows.Forms.Label ThanksLbl;
        private System.Windows.Forms.Label AutoMainLbl;
        private System.Windows.Forms.Label DoNotForgetLbl;
        private System.Windows.Forms.Timer buttonTimer;
        private System.Windows.Forms.Timer locTimer;
        private System.Windows.Forms.Label ToEditLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox filterPic5;
        private System.Windows.Forms.PictureBox filterPic6;
        private System.Windows.Forms.PictureBox filterPic7;
        private System.Windows.Forms.PictureBox filterPic8;
        private System.Windows.Forms.PictureBox filterPic9;
        private System.Windows.Forms.PictureBox filterPic10;
        private System.Windows.Forms.PictureBox filterPic11;
        private System.Windows.Forms.PictureBox filterPic12;
        private System.Windows.Forms.Label FullName1;
        private System.Windows.Forms.Label User1;
        private System.Windows.Forms.PictureBox UserPic1;
        private System.Windows.Forms.Panel GetUser1;
        private System.Windows.Forms.Label FullName6;
        private System.Windows.Forms.Label User6;
        private System.Windows.Forms.PictureBox UserPic6;
        private System.Windows.Forms.Panel GetUser6;
        private System.Windows.Forms.Label User3;
        private System.Windows.Forms.Label FullName3;
        private System.Windows.Forms.PictureBox UserPic3;
        private System.Windows.Forms.Panel GetUser3;
        private System.Windows.Forms.Label User7;
        private System.Windows.Forms.Label FullName7;
        private System.Windows.Forms.PictureBox UserPic7;
        private System.Windows.Forms.Panel GetUser7;
        private System.Windows.Forms.PictureBox UserPic5;
        private System.Windows.Forms.Label FullName5;
        private System.Windows.Forms.Label User5;
        private System.Windows.Forms.Panel GetUser5;
        private System.Windows.Forms.PictureBox UserPic8;
        private System.Windows.Forms.Label FullName8;
        private System.Windows.Forms.Label User8;
        private System.Windows.Forms.Panel GetUser8;
        private System.Windows.Forms.Label User2;
        private System.Windows.Forms.Label FullName2;
        private System.Windows.Forms.PictureBox UserPic2;
        private System.Windows.Forms.Panel GetUser2;
        private System.Windows.Forms.Label User9;
        private System.Windows.Forms.Label FullName9;
        private System.Windows.Forms.PictureBox UserPic9;
        private System.Windows.Forms.Panel GetUser9;
        private System.Windows.Forms.Label User4;
        private System.Windows.Forms.Label FullName4;
        private System.Windows.Forms.PictureBox UserPic4;
        private System.Windows.Forms.Panel GetUser4;
        private System.Windows.Forms.Label User10;
        private System.Windows.Forms.Label FullName10;
        private System.Windows.Forms.PictureBox UserPic10;
        private System.Windows.Forms.Panel GetUser10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer textChecker;
    }
}