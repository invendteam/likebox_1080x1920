﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lkbox
{
    public partial class MessageForm : Form
    {
        int TimerTime = 10000;

        public MessageForm(string MessageTxt, string OkButtonTxt)
        {
            InitializeComponent();
            Label MessageText = new Label();
            MessageText.Text = MessageTxt;
            MessageText.MaximumSize = new Size(500, 0); 
            MessageText.AutoSize = true;
            MessageText.TextAlign = ContentAlignment.MiddleCenter;
            MessageText.Font = new Font("PF Centro Slab Pro", 16);
            Width = MessageText.Width + 20;
            Controls.Add(MessageText);
            MessageText.Location = new Point(Width / 2 - MessageText.Width / 2, (Height - 100) / 2 - MessageText.Height / 2);
            Button OkButton = new Button();
            OkButton.Text = OkButtonTxt;
            OkButton.ForeColor = Color.White;
            OkButton.Click += OkButton_Click;
            OkButton.FlatStyle = FlatStyle.Flat;
            OkButton.FlatAppearance.BorderSize = 0;
            OkButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            OkButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            OkButton.BackColor = Color.Transparent;
            OkButton.Size = new Size(Width /3, 40);
            OkButton.Font = new Font("PF Centro Slab Pro", 16);
            OkButton.BackgroundImage = new Bitmap(Image.FromFile("bttnEn.jpg"), OkButton.Size);
            Controls.Add(OkButton);
            OkButton.Location = new Point(20, Height - OkButton.Height - 20);
        }

        public MessageForm(string MessageTxt, string OkButtonTxt, string CancelButtonTxt) : this(MessageTxt, OkButtonTxt)
        {
            Button CancelButton = new Button();
            CancelButton.Text = CancelButtonTxt;
            CancelButton.Click += CancelButton_Click;
            CancelButton.ForeColor = Color.White;
            CancelButton.FlatStyle = FlatStyle.Flat;
            CancelButton.FlatAppearance.BorderSize = 0;
            CancelButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            CancelButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            CancelButton.BackColor = Color.Transparent;
            CancelButton.Size = new Size(Width/3, 40);
            CancelButton.Font = new Font("PF Centro Slab Pro", 16);
            CancelButton.BackgroundImage = new Bitmap(Image.FromFile("bttnEn.jpg"), CancelButton.Size);
            Controls.Add(CancelButton);
            CancelButton.Location = new Point(Width - CancelButton.Width - 20, Height - CancelButton.Height - 20);
        }

        public MessageForm(string MessageTxt, string OkButtonTxt, int TimerLength)
            : this(MessageTxt, OkButtonTxt)
        {
            TimerTime = TimerLength;
            CloseTimer.Start();
        }

        public MessageForm(string MessageTxt, string OkButtonTxt, string CancelButtonTxt, int TimerLength)
            : this(MessageTxt, OkButtonTxt, TimerLength)
        {
            Button CancelButton = new Button();
            CancelButton.Text = CancelButtonTxt;
            CancelButton.Click += CancelButton_Click;
            CancelButton.ForeColor = Color.White; 
            CancelButton.FlatStyle = FlatStyle.Flat;
            CancelButton.FlatAppearance.BorderSize = 0;
            CancelButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            CancelButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            CancelButton.BackColor = Color.Transparent;
            CancelButton.Size = new Size(Width / 3, 40);
            CancelButton.Font = new Font("PF Centro Slab Pro", 16);
            CancelButton.BackgroundImage = new Bitmap(Image.FromFile("bttnEn.jpg"), CancelButton.Size);
            Controls.Add(CancelButton);
            CancelButton.Location = new Point(Width - CancelButton.Width - 20, Height - CancelButton.Height - 20);
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void CloseTimer_Tick(object sender, EventArgs e)
        {
            TimerTime--;
            if (TimerTime == 0)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }
    }
}
