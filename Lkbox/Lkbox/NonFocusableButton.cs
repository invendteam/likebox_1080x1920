﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lkbox
{
    class NonFocusableButton : Button
    {
        public NonFocusableButton()
        {
            SetStyle(ControlStyles.Selectable, false);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= User32Constants.WS_EX_TOPMOST;
                cp.ExStyle |= User32Constants.WS_EX_NOACTIVATE;
                return cp;
            }
        }
    }
}
