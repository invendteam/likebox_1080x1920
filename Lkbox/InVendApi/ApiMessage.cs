﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace InVendApi
{
    public class ApiMessage
    {
        private readonly string _uri;
        private readonly FormUrlEncodedContent _content;
        private Dictionary<string, string> _values;

        #region Private Fields

        #endregion

        #region Constructors

        public ApiMessage(Dictionary<string, string> values, string uri)
        {
            _uri = uri;
            _values = values;
            _content = new FormUrlEncodedContent(values);
        }

        #endregion

        #region Properties

        public string Uri {get{return _uri;}}

        public FormUrlEncodedContent Content { get { return new FormUrlEncodedContent(_values); } }

        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        #endregion

        #region Static Methods

        #endregion

        #region Event Handlers

        #endregion


    }
}
