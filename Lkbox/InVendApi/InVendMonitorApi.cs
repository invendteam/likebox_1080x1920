﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OnlineMonitorEnums;

namespace InVendApi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Net;
    using System.Collections.Specialized;


    namespace InVendMonitorApi
    {
        public class InVendMonitorApi
        {
            #region Private Fields

            #region Constants

            private const string ApiRegisterUrl = "/api/Register";
            private const string ApiInstamatReportUrl = "/api/InstamatReport";
            private const string ApiInstamatLogUrl = "/api/InstamatLog";
            private const string ApiInstamatCollectionUrl = "/api/InstamatCollection";
            private const string ApiInstamatPurchaseUrl = "/api/InstamatPurchase";
            private const string ApiInstamatConfigUrl = "/api/InstamatConfig";
            private const string ApiInstamatSocketConfig = "/api/InstamatSocketConfig";

            #endregion

            private readonly string _hash;
            private readonly TerminalType _type;
            private readonly ApiMessageSender _apiMessageSender;
            private WebCamController _webCamControler;
            private string _ipAddres;
            private string _baseHttpUrl;
            private WebSocketController _webSocketController;
            private string _baseWSUrl;

            #endregion

            #region Constructors

            /// <summary>
            /// 
            /// </summary>
            /// <param name="baseHttpUrl">Базовый адрес сайта(без порта)</param>
            /// <param name="hash">HASH-код терминала</param>
            /// <param name="type">Тип терминала</param>
            /// <param name="serverTimeout">Время ожидания, после которого сервер считается недоступным.
            ///  Возможно, имеет смысл выставлять равным времени обновления.</param>
            /// <param name="port">Порт сервера(по умолчанию = 80)</param>
            /// <param name="enableAutoLoging">Включение/выключение автолога(по умолчанию = false)</param>
            public InVendMonitorApi(string baseUrl,
                string hash, 
                TerminalType type,
                TimeSpan serverTimeout, 
                int port = 80,
                bool enableAutoLoging = false)
            {
                _webCamControler = new WebCamController(baseUrl);


                _ipAddres = baseUrl;
                if (port != 80)
                {
                    baseUrl += ":" + port;
                }
                _baseHttpUrl = "http://" + baseUrl;
                
                _baseWSUrl = "ws://" + baseUrl;
                _hash = hash;
                _type = type;
                EnableAutoLoging = enableAutoLoging;
                _apiMessageSender = new ApiMessageSender(_baseHttpUrl,serverTimeout);
                

            }
            #endregion

            #region Properties

            /// <summary>
            /// Включение/откючение автоматической записи в лог о действиях
            /// </summary>
            public bool EnableAutoLoging { get; set; }

            #endregion

            #region Private Methods

            private async Task AutoLog(string response, InstamatLogType type)
            {
                if (EnableAutoLoging)
                {
                    var logMessage = new LogMessage(type, response, DateTime.Now);
                    await SendlogAsync(logMessage);
                }
            }

            #endregion        

            #region Public Methods

            //public async Task SendVideo()
            //{
            //    var request = WebRequest.Create(_baseHttpUrl+"/Terminal/SendVideo/?Hash="+_hash);
            //    request.Method = WebRequestMethods.Http.Post;
            //    var stream = request.GetRequestStream();
            //    stream.wri
            //}




            public WebSocketController GetWebSocketController()
            {
                var uriString = _baseWSUrl + "/CamStream.ashx?hash=" + _hash;
                var uri = new Uri(uriString);
                
                    _webSocketController = new WebSocketController(uri);
                
                return _webSocketController;

            }

            public WebCamController GetCamController()
            {
                
                if (_webCamControler.IsDisposed)
                {
                    _webCamControler = new WebCamController(_baseHttpUrl + "/CamStream.ashx?hash=" + _hash);
                    return _webCamControler;
                }
                return _webCamControler;
            }

            /// <summary>
            /// Регистрация терминала в админке. После регистрации используется как своеобразный "ping".
            /// </summary>
            /// <returns></returns>
            public async Task<RegisterState> TryRegisterTerminalAsync()
            {
                var values = new Dictionary<string, string>
                {
                    {"Hash", _hash},
                    {"Type", _type.ToString()}
                };

                var message = new ApiMessage(values,ApiRegisterUrl);
                var response = await _apiMessageSender.SendMessage(message);

                return ParseRegisterResponse(response);

            }

            /// <summary>
            /// Доклад терминала о состоянии. Для теста используется только один параметр 
            /// </summary>
            /// <param name="paperCapacity">Количество оставшейся бумаги</param>
            /// <returns></returns>
            public async Task<ReportState> SendReportAsync(int paperCapacity)
            {
                var values = new Dictionary<string, string>
                {
                    {"Hash", _hash},
                    {"PaperCapacity", paperCapacity.ToString()}
                };
                var message = new ApiMessage(values, ApiInstamatReportUrl);
                var response = await _apiMessageSender.SendMessage(message);
                var parsedResponse = ParseResponse(response);
                await AutoLog(parsedResponse.ToString(), InstamatLogType.SendReport);
                return parsedResponse;
            }


           

            /// <summary>
            /// Отправка данных о совершенной покупке
            /// </summary>
            /// <param name="total">Сколько всего внесено денег</param>
            /// <param name="countFoto">Сколько фото было распечатано</param>
            /// <param name="phoneNumber">На какой номер был положен остаток(это string, 
            /// если номер не был указан, то можно передать либо "NO", либо null)</param>
            /// <param name="phoneCash">Сумма, которая была переведена на счет телефона</param>
            /// <param name="time">Время покупки</param>
            /// <returns>Энам с состоянием отправки</returns>
            public async Task<ReportState> SendPurchaseAsync(
                double total, 
                int countFoto, 
                string phoneNumber, 
                double phoneCash, 
                DateTime time)
            {
                var values = new Dictionary<string, string>
                {
                    {"Hash", _hash},
                    {"Total", total.ToString(CultureInfo.InvariantCulture)},
                    {"CountFoto", countFoto.ToString(CultureInfo.InvariantCulture)},
                    {"PhoneNumber", phoneNumber},
                    {"PhoneCash", phoneCash.ToString(CultureInfo.InvariantCulture)},
                    {"Time", time.ToString(CultureInfo.InvariantCulture)}
                };

                var message = new ApiMessage(values, ApiInstamatPurchaseUrl);
                var response = await _apiMessageSender.SendMessage(message);
                var parsedResponse = ParseResponse(response);
                #region String Builder 
                var stringBuilder = new StringBuilder();
                stringBuilder.Append("Total:");
                stringBuilder.Append(total);
                stringBuilder.Append("Foto: ");
                stringBuilder.Append(countFoto);
                stringBuilder.Append(" Phone: ");
                stringBuilder.Append(phoneNumber);
                stringBuilder.Append(" PhoneCash: ");
                stringBuilder.Append(phoneCash);
                stringBuilder.Append(". [Server response: ");
                stringBuilder.Append(parsedResponse);
                stringBuilder.Append(']');
                #endregion

                await AutoLog(stringBuilder.ToString(), InstamatLogType.SendPurchase);
                return parsedResponse;
            }

            /// <summary>
            /// Отправка записи в лог
            /// </summary>
            /// <param name="message">Сообщение, которое нужно записать в лог</param>
            /// <returns></returns>
            public async Task<ReportState> SendlogAsync(LogMessage message)
            {
                var values = new Dictionary<string, string>
                {
                    {"Hash", _hash},
                    {"Type", message.Type.ToString()},
                    {"MessageText", message.Message},
                    {"Time", message.Time.ToString(CultureInfo.InvariantCulture)},
                };

                var apiMessage = new ApiMessage(values, ApiInstamatLogUrl);
                var response = await _apiMessageSender.SendMessage(apiMessage);
                var parsedResponse = ParseResponse(response);
                return parsedResponse;


            }

            /// <summary>
            /// Отправка информации об инкассации
            /// </summary>
            /// <param name="amount">Сумма</param>
            /// <returns></returns>
            public async Task<ReportState> SendCollectionAsync(double amount)
            {
                var values = new Dictionary<string, string>
                {
                    {"Hash", _hash},
                    {"Amount", amount.ToString(CultureInfo.InvariantCulture)}
                };
                var apiMessage = new ApiMessage(values, ApiInstamatCollectionUrl);
                var response = await _apiMessageSender.SendMessage(apiMessage);
                var parsedResponse = ParseResponse(response);

                await AutoLog("Amount:"+amount + " Server response: " + parsedResponse, InstamatLogType.SendCollection);
                return parsedResponse;
            }

            /// <summary>
            /// Запрос конфигурации с сервера
            /// </summary>
            /// <returns>Цена за фоторгафию и секунды</returns>
            public async Task<InstamatConfiguration> GetConfigAsync()
            {
                var values = new Dictionary<string, string>
                {
                    {"Hash", _hash}
                };
                var apiMessage =  new ApiMessage(values, ApiInstamatConfigUrl);
                var response = await _apiMessageSender.SendMessage(apiMessage, false);

                #region SHIT
                // {\"Price\":100.0,\"Seconds\":10}

                InstamatConfiguration configuration;
                try
                {
                    var priceIndex = response.IndexOf("Price", StringComparison.Ordinal) + 7;
                    var secondsIndex = response.IndexOf("Seconds", StringComparison.Ordinal);
                    var priceString = response.Substring(priceIndex, secondsIndex - priceIndex - 2);
                    var secondString = response.Substring(secondsIndex + 9, response.Length - (secondsIndex + 9) - 1);
                    var price = double.Parse(priceString.Replace('.', ','));
                    var seconts = Convert.ToInt32(secondString);
                    configuration = new InstamatConfiguration(price, seconts, true);
                }
                catch (Exception)
                {
                    configuration = new InstamatConfiguration();
                }
        
                #endregion

                return configuration;
            }

            public async Task<InstamatSocketConfig> GetSocketConfigAsync()
            {
                var values = new Dictionary<string, string>
                {
                    {"Hash", _hash}
                };
                var apiMessage = new ApiMessage(values, ApiInstamatSocketConfig);
                var response = await _apiMessageSender.SendMessage(apiMessage, false);
                var result = Convert.ToInt32(response);
                if (result == 1 || result== 0 || result == 6)
                {
                    return new InstamatSocketConfig(0, false);
                }
                else
                {
                    return new InstamatSocketConfig(result, true);
                }
            }

            #endregion

            #region Static Methods

            // TODO: to static
            private ReportState ParseResponse(string value)
            {
                ReportState state;
                var result = Enum.TryParse(value, out state);
                if (result)
                {

                    //TODO: SHIT это нужно делать в другом месте

                    #region SHIT
                    if (state == ReportState.ServerDown)
                    {
                        var values = new Dictionary<string, string>
                        {
                            {"Hash", _hash},
                            {"Type", InstamatLogType.LostConnection.ToString()},
                            {"MessageText", "Server not response"},
                            {"Time", DateTime.Now.ToString(CultureInfo.InvariantCulture)},
                        };

                        var apiMessage = new ApiMessage(values, ApiInstamatLogUrl);
                     
                        _apiMessageSender.AddToSendQueue(apiMessage);
                    }
                    #endregion

                    return state;

                }
                else
                    return ReportState.UnknownError;
            }

            private static RegisterState ParseRegisterResponse(string value)
            {
                RegisterState state;
                var result = Enum.TryParse(value, out state);

                if (result)
                    return state;
                else
                    return RegisterState.UnknownError;
            }

            #endregion
        }
    }

}
