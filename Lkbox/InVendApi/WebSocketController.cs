﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InVendApi
{
    public class WebSocketController:IDisposable
    {
        private readonly Uri _uri;
        private readonly ClientWebSocket _webSocket;
        private bool _socketReady;

        #region Private Fields

        #endregion

        #region Constructors

        public WebSocketController(Uri uri)
        {
            _uri = uri;
            _webSocket = new ClientWebSocket();
            _socketReady = true;
        }

        #endregion

        #region Properties

        public WebSocketState State {get{return _webSocket.State;}}
        public WebSocketCloseStatus? CloseStatus { get { return _webSocket.CloseStatus; } }
        public bool IsDisposed { get; private set; }
        #endregion

        #region Public Methods

        public async Task Connect()
        {
            
            await _webSocket.ConnectAsync(_uri, CancellationToken.None);
            _socketReady = true;
        }

        public async Task<bool> SendFrame(Bitmap bitmap)
        {
            if (State == WebSocketState.Open)
            {
                if (_socketReady)
                {
                    //_socketReady = false;
                    await Send(bitmap);
                    _socketReady = true;
                }
                return true;
            }
            return false;
        }

        #endregion

        #region Private Methods

        private async Task Send(Bitmap bitmap)
        {
            byte[] byteBitmap;
            using (var ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Jpeg);
                byteBitmap = ms.ToArray();
            }
            bool b = false;
            string mes = "";
            var data = new ArraySegment<byte>(byteBitmap);
            try
            {
                await _webSocket.SendAsync(data, WebSocketMessageType.Binary, true, CancellationToken.None);
            }
            catch (Exception ex)
            {
                mes = "Неудалось отправить изображение: " + ex.Message + " innerExeption: " +
                        ex.InnerException;
                b = true;
            }
            if(b)
            {
                if (_webSocket.State == WebSocketState.Open)
                    await _webSocket.CloseAsync(WebSocketCloseStatus.InternalServerError,
                        mes, CancellationToken.None);

                Dispose();
            }
        }



        #endregion

        public void Dispose()
        {
            if (!IsDisposed)
            {
                 IsDisposed = true;
                 _webSocket.Dispose();
            }
        }


    }
}
