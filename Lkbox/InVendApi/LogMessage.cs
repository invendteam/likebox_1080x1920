﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineMonitorEnums;

namespace InVendApi
{
    public struct LogMessage
    {
        #region Private Fields

        private readonly InstamatLogType _type;
        private readonly string _message;
        private readonly DateTime _time;

        #endregion

        #region Constructors

        /// <summary>
        /// Сообщение для лога
        /// </summary>
        /// <param name="type">Тип лог-сообщения</param>
        /// <param name="message">Текст</param>
        /// <param name="time">Время записи</param>
        public LogMessage(InstamatLogType type, string message, DateTime time)
        {
            _type = type;
            _message = message;
            _time = time;
        }

        #endregion

        #region Properties

        public InstamatLogType Type
        {
            get { return _type; }
        }

        public string Message
        {
            get { return _message; }
        }

        public DateTime Time
        {
            get { return _time; }
        }

        #endregion
    }
}
