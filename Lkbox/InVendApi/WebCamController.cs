﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace InVendApi
{
    public class WebCamController : IDisposable
    {
        private readonly Socket _socket;
        private readonly string _ipAddress;

        #region Private Fields

        #endregion

        #region Constructors

        public WebCamController(string ipAddress)
        {
            //_ipAddress = IPAddress.Parse(ipAddress == "localhost" ? "127.0.0.1" : ipAddress);
            //var adress =  Dns.GetHostAddresses(ipAddress);
            //_ipAddress = adress.First();
            IsDisposed = false;
            _ipAddress = ipAddress;
            _socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

        }

        #endregion

        #region Properties
        public bool IsDisposed { get; private set; }
        #endregion

        #region Public Methods

        public void Connect(InstamatSocketConfig config)
        {
            
            _socket.Connect(_ipAddress,config.Port);
        }

        public bool SendFrame(Bitmap bitmap)
        {
            if (IsDisposed)
            {
                return false;
            }
            byte[] bufImage;
            using (var ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Jpeg);
                bufImage = ms.ToArray();
            }
            return SendFrame(bufImage);
        }
        public bool SendFrame(byte[] byteBitmap)
        {
            if (IsDisposed)
            {
                return false;
            }
            try
            {
                var bytesSent = _socket.Send(byteBitmap, 0, byteBitmap.Length, SocketFlags.None);
                return true;
            }
            catch (Exception ex)
            {
                _socket.Close();
                return false;
            }
        }
        #endregion

        #region Private Methods



        #endregion

        public void Dispose()
        {
            if (IsDisposed) return;
            _socket.Dispose();
            IsDisposed = true;
        }

        
    }
}
