﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InVendApi
{
    public struct InstamatContentUrl
    {


        #region Private Fields
        private readonly string _imageUrl;
        private readonly string _videoUrl;
        #endregion

        #region Constructors

        public InstamatContentUrl(string imageUrl, string videoUrl)
        {
            _imageUrl = imageUrl;
            _videoUrl = videoUrl;
        }

        #endregion

        #region Properties

        public string ImageUrl { get {return _imageUrl;}}

        public string VideoUrl {get{return _videoUrl;}}

        #endregion

    }
}
