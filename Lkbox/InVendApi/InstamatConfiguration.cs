﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InVendApi
{
    public struct InstamatConfiguration
    {
        #region Private Fields

        private readonly double _price;
        private readonly int _seconds;
        private readonly bool _succesfull;

        #endregion

        #region Constructors

        public InstamatConfiguration(double price, int seconds, bool succesfull)
        {
            _price = price;
            _seconds = seconds;
            _succesfull = succesfull;
        }

        #endregion

        #region Properties

        public double Price
        {
            get { return _price; }
        }

        public int Seconds
        {
            get { return _seconds; }
        }

        public bool Succesfull
        {
            get { return _succesfull; }
        }

        #endregion
    }
}
