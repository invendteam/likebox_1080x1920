﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using OnlineMonitorEnums;

namespace InVendApi
{
    public class ApiMessageSender
    {
        #region Private Fields

        private readonly Queue<ApiMessage> _messages;
        private readonly HttpClient _httpClient;

        #endregion

        #region Constructors

        public ApiMessageSender(string baseUrl, TimeSpan serverTimeout)
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(baseUrl),
                Timeout = serverTimeout
            };
            _messages = new Queue<ApiMessage>();
        }

        #endregion

        #region Properties

        public int CountUnsendedMessages {get{return  _messages.Count;}}

        #endregion

        #region Public Methods

        public async Task<string> SendMessage(ApiMessage message, bool guaranteedDelivery = true)
        {
            if (_messages.Count > 0)
            {
                await SendAllMessage();
            }
            var result = await PostRequest(message);
            //TODO: Double typecast - trash
            if (guaranteedDelivery && result == ((int)ReportState.ServerDown).ToString())
            {
                AddToSendQueue(message);
            }
            return result;
        }

        public void AddToSendQueue(ApiMessage message)
        {
            _messages.Enqueue(message);
        }

        public async Task<int> SendAllMessage()
        {
            var countMessageSend = 0;

            while (_messages.Count > 0)
            {
                var mesage = _messages.Peek();
                var result = await PostRequest(mesage);
                if (result == ((int)ReportState.ServerDown).ToString())
                {
                    return countMessageSend;
                }
                else
                {
                    countMessageSend++;
                    try { _messages.Dequeue(); }
                    catch { }
                }
            }
            return countMessageSend;
        }

        #endregion

        #region Private Methods

        private async Task<string> PostRequest(ApiMessage message)
        {
            HttpResponseMessage response;
            try
            {
                
                response = await _httpClient.PostAsync(message.Uri, message.Content);
            }
            //TODO: Exeption <---------------
            catch (Exception exception)
            {
          
                return ((int)ReportState.ServerDown).ToString();
            }

            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }
        #endregion

        #region Static Methods

        #endregion

        #region Event Handlers

        #endregion
    }
}
