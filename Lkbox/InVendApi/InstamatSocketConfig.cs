﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InVendApi
{
    public struct InstamatSocketConfig
    {
        #region Private Fields

        private readonly int _port;
        private readonly bool _isValid;
        
        #endregion

        #region Constructors

        public InstamatSocketConfig(int port, bool isValid)
        {
            _port = port;
            _isValid = isValid;
        }

        #endregion

        #region Properties

        public int Port 
        {
          get {return _port;}
        }

        public bool IsValid { get { return _isValid; } }

        #endregion
    }
}
